<?php

function logob()
{
	return '<svg version="1.1" id="Primo_Piano" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
	 y="0px" viewBox="0 0 182 36" style="enable-background:new 0 0 182 36;margin-top: 20px;width: 160px;" xml:space="preserve">
<style type="text/css">
	.st0{fill-rule:evenodd;clip-rule:evenodd;}
</style>
<g>
	<g>
		<g>
			<g>
				<path class="st0" d="M66.3,19.3c0,5.9,5.9,9.1,11.7,9.1c4.7,0,11.4-2.1,11.4-8.7c0-4.7-3.1-6.4-7.6-7.5c-3.9-1-8.6-0.8-8.6-3.3
					c0-2,2.2-2.8,4.4-2.8c2.5,0,5,1,5,3.7h6.1c-0.5-6.2-5.5-8.5-11-8.5C72.4,1.3,67,3.6,67,9.9c0,4.4,3.8,6.3,7.8,7
					c6,1,7.9,1.3,7.9,3.6c0,2.4-3.4,3.1-4.6,3.1c-2.7,0-5.5-1.2-5.6-4.2H66.3z"/>
			</g>
			<path class="st0" d="M181.3,1.3h-6.6v2.8c0,0-1.6-3.4-7.8-3.4c-6.7,0-10.9,5.9-10.9,12.3c0,6.8,3.6,12.3,11.1,12.3
				c5.6,0,7.4-3,7.4-3l0.1,2.7c-0.2,3.5-2.1,5.4-5.6,5.4c-2.6,0-4.3-0.8-4.6-2.9l-7.3,0.4c0.3,5,6,8,11.2,8
				c11.7,0,13.1-6.2,13.1-11.2V1.3z M168.2,19.8c-3.6,0-5.9-3.5-5.7-6.9c0.2-3.2,1.9-6.5,5.9-6.5c3.9,0,6.2,3.3,5.9,7.1
				C174.2,16.6,172.5,19.8,168.2,19.8z"/>
			<g>
				<path class="st0" d="M90.5,27.1h6.9V12.7c0-4.3,3-5.3,4.3-5.3c3.8,0,4,3.1,4,5.8v13.9h6.9V12.7c0-4.3,2.5-5.4,4.3-5.4
					c4,0,4.1,3.1,4.1,5.9v13.9h6.9V10.5c0-6.5-3.7-9-8.9-9c-3.4,0-5.9,2-7.3,4.2c-1.3-3-4-4.2-7.1-4.2c-3.2,0-5.7,1.5-7.4,4.1h-0.1
					V2.2h-6.5V27.1z"/>
			</g>
			<path class="st0" d="M154.4,16.2c0.5-7.5-3.7-15.3-12.8-15.3c-7.9,0-12.7,6.4-12.7,13.5c0,7.2,4.9,13.3,12.9,13.3
				c5.8,0,10.2-2.4,12.2-8.3L147,19c-0.2,0.9-2,3.1-5,3.1c-3.9,0-6-3-6-5.9H154.4z M136.2,11.3c0-0.9,1.2-5,5.5-5
				c4.1,0,5.5,3.9,5.5,5H136.2z"/>
		</g>
		<g>
			<g>
				<path d="M32.8,27.1c0-4.5,3.7-8.2,8.2-8.2c4.5,0,8.2,3.7,8.2,8.2c0,4.5-3.7,8.2-8.2,8.2C36.5,35.3,32.8,31.6,32.8,27.1z"/>
			</g>
			<g>
				<path d="M16.8,27.1c0-4.5,3.7-8.2,8.2-8.2c4.5,0,8.2,3.7,8.2,8.2c0,4.5-3.7,8.2-8.2,8.2C20.5,35.3,16.8,31.6,16.8,27.1z"/>
			</g>
			<g>
				<path d="M0.8,27.1c0-4.5,3.7-8.2,8.2-8.2c4.5,0,8.2,3.7,8.2,8.2c0,4.5-3.7,8.2-8.2,8.2C4.4,35.3,0.8,31.6,0.8,27.1z"/>
			</g>
			<g>
				<circle cx="41" cy="11.1" r="8.2"/>
			</g>
			<g>
				<circle cx="25" cy="11.1" r="8.2"/>
			</g>
			<g>
				<circle cx="57.1" cy="11.1" r="8.2"/>
			</g>
		</g>
	</g>
</g>
</svg>';
}

function logow()
{
	return '<svg version="1.1" id="Primo_Piano" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
	 y="0px" viewBox="0 0 182 36" style="enable-background:new 0 0 182 36;margin-top: 20px;width: 160px;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFFFFF;}
</style>
<g>
	<g>
		<g>
			<g>
				<path class="st0" d="M66.3,19.3c0,5.9,5.9,9.1,11.7,9.1c4.7,0,11.4-2.1,11.4-8.7c0-4.7-3.1-6.4-7.6-7.5c-3.9-1-8.6-0.8-8.6-3.3
					c0-2,2.2-2.8,4.4-2.8c2.5,0,5,1,5,3.7h6.1c-0.5-6.2-5.5-8.5-11-8.5C72.4,1.3,67,3.6,67,9.9c0,4.4,3.8,6.3,7.8,7
					c6,1,7.9,1.3,7.9,3.6c0,2.4-3.4,3.1-4.6,3.1c-2.7,0-5.5-1.2-5.6-4.2h-6.2V19.3z"/>
			</g>
			<path class="st0" d="M181.3,1.3h-6.6v2.8c0,0-1.6-3.4-7.8-3.4C160.2,0.7,156,6.6,156,13c0,6.8,3.6,12.3,11.1,12.3
				c5.6,0,7.4-3,7.4-3l0.1,2.7c-0.2,3.5-2.1,5.4-5.6,5.4c-2.6,0-4.3-0.8-4.6-2.9l-7.3,0.4c0.3,5,6,8,11.2,8
				c11.7,0,13.1-6.2,13.1-11.2L181.3,1.3L181.3,1.3z M168.2,19.8c-3.6,0-5.9-3.5-5.7-6.9c0.2-3.2,1.9-6.5,5.9-6.5
				c3.9,0,6.2,3.3,5.9,7.1C174.2,16.6,172.5,19.8,168.2,19.8z"/>
			<g>
				<path class="st0" d="M90.5,27.1h6.9V12.7c0-4.3,3-5.3,4.3-5.3c3.8,0,4,3.1,4,5.8v13.9h6.9V12.7c0-4.3,2.5-5.4,4.3-5.4
					c4,0,4.1,3.1,4.1,5.9v13.9h6.9V10.5c0-6.5-3.7-9-8.9-9c-3.4,0-5.9,2-7.3,4.2c-1.3-3-4-4.2-7.1-4.2c-3.2,0-5.7,1.5-7.4,4.1h-0.1
					V2.2h-6.5v24.9H90.5z"/>
			</g>
			<path class="st0" d="M154.4,16.2c0.5-7.5-3.7-15.3-12.8-15.3c-7.9,0-12.7,6.4-12.7,13.5c0,7.2,4.9,13.3,12.9,13.3
				c5.8,0,10.2-2.4,12.2-8.3l-7-0.4c-0.2,0.9-2,3.1-5,3.1c-3.9,0-6-3-6-5.9H154.4z M136.2,11.3c0-0.9,1.2-5,5.5-5
				c4.1,0,5.5,3.9,5.5,5H136.2z"/>
		</g>
		<g>
			<g>
				<path class="st0" d="M32.8,27.1c0-4.5,3.7-8.2,8.2-8.2s8.2,3.7,8.2,8.2s-3.7,8.2-8.2,8.2C36.5,35.3,32.8,31.6,32.8,27.1z"/>
			</g>
			<g>
				<path class="st0" d="M16.8,27.1c0-4.5,3.7-8.2,8.2-8.2s8.2,3.7,8.2,8.2s-3.7,8.2-8.2,8.2C20.5,35.3,16.8,31.6,16.8,27.1z"/>
			</g>
			<g>
				<path class="st0" d="M0.8,27.1c0-4.5,3.7-8.2,8.2-8.2s8.2,3.7,8.2,8.2s-3.7,8.2-8.2,8.2C4.4,35.3,0.8,31.6,0.8,27.1z"/>
			</g>
			<g>
				<circle class="st0" cx="41" cy="11.1" r="8.2"/>
			</g>
			<g>
				<circle class="st0" cx="25" cy="11.1" r="8.2"/>
			</g>
			<g>
				<circle class="st0" cx="57.1" cy="11.1" r="8.2"/>
			</g>
		</g>
	</g>
</g>
</svg>';
}