


		<!-- Main content -->
		<div class="content-wrapper">
			

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">MANAGE NEWS & EVENT</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<!-- <a href="#" class="breadcrumb-item">Link</a> -->
							<span class="breadcrumb-item active">NEWS & EVENT ADD/EDIT</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">
                
				
				<!-- Basic table -->
				<div class="card">
                    <div id="alert"></div>
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Add/Edit NEWS & EVENT</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a> -->
		                		<!-- <a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					

                    <form action="<?php echo base_url(); ?>adminnews/addnews" method='post'  enctype="multipart/form-data">
                    <div class="container">
                        <div class="row">
                       

                         <div class="col-lg-10 offset-lg-1">

                             <!-- <label class="col-lg-2 col-form-label font-weight-semibold">Thumbnail:</label>
								<div class="col-lg-12 text-center">
                                <input type="file" name="imageblog" class="file-input form-control-lg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
								</div> -->

                                <div class="form-inline mgt-20">
                                    <div class="col-lg-2">
                                    <label for="" class="float-right">Thumbnail:&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                     <input type="file" name="imageblog" class="file-input form-control-lg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                    <div class="col-lg-3 offset-lg-3">
                                    <label for="" class="float-right">Image Alt&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-3">
                                    <input type="text" name="imagealt" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                    <div class="col-lg-2">
                                    <label for="" class="float-right">Title ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                    <input type="text" name="titlethai" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                    <div class="col-lg-2">
                                    <label for="" class="float-right">Title Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                    <input type="text" name="titleeng" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                <div class="col-lg-2">
                                    <label for="" class="float-right">Type&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                    <select name="blog_type_id" data-placeholder="Select a Group..." class="form-control select" data-fouc required>
                                        <option></option>
                                        <?php foreach($newstypedata as $value){ ?>
											<option value="<?php echo $value['blog_type_id']; ?>"><?php echo $value['blog_type_name']; ?></option>
                                        <?php } ?>
									</select>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                <div class="col-lg-2">
                                    <label for="" class="float-right">Category&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                    <select name="blog_category_id" data-placeholder="Select a Group..." class="form-control select" data-fouc required>
                                        <option></option>
                                        <?php foreach($newscategorydata as $value){ ?>
											<option value="<?php echo $value['blog_category_id']; ?>"><?php echo $value['blog_category_name']; ?></option>
                                        <?php } ?>
									</select>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                <div class="col-lg-2">
                                    <label for="" class="float-right">Detail Thai&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                    <textarea id="summernote" name="detailthai"></textarea>
                                    </div>
                                </div>
                                <div class="form-inline mgt-20">
                                <div class="col-lg-2">
                                    <label for="" class="float-right">Detail Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                    <textarea id="summernote1" name="detaileng"></textarea>
                                    </div>
                                </div>
                                <div class="form-inline mgt-20">
                                    <div class="col-lg-2">
                                    <label for="" class="float-right">Meta Page Title&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-10">
                                    <input type="text" name="blog_meta_title" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-inline mgt-20">
                                    <div class="col-lg-2">
                                    <label for="" class="float-right">Meta Description&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-10">
                                    <input type="text" name="blog_meta_description" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-inline mgt-20 ">
                                    <div class="col-lg-2">
                                    <label for="" class="float-right">Meta Keyword&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-10">
                                    <input type="text" name="blog_meta_keyword" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-inline mgt-20 ">
                                    <div class="col-lg-2">
                                    <label for="" class="float-right">Order&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-2">
                                    <input type="text" name="blog_order" value="" class="form-control" maxlength='2' size='2'>
                                    </div>
                                </div>
                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Status&nbsp;&nbsp;:&nbsp;&nbsp;
                                    <label class="form-check-label">
										<input type="checkbox" name="status" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </label>
                                   
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-12 text-center">
                        <button type="button" value="reset" class="btn btn-danger" name="reset">cancel</button>
                        <input type="submit" value="save" class="btn btn-success" name="save">
                        </div>

                    </div>

                     <?php echo form_close();?>



				
			</div>
            <!-- /content area -->
            
            <script>
		$(document).ready(function() {
  $('#summernote').summernote({
    lang: 'ko-KR' // default: 'en-US'
  });
  $('#summernote1').summernote({
    lang: 'ko-KR' // default: 'en-US'
  });
});
		</script>


		