


		<!-- Main content -->
		<div class="content-wrapper">
			

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">MANAGE NEWS & EVENT CATEGORY</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<!-- <a href="#" class="breadcrumb-item">Link</a> -->
							<span class="breadcrumb-item active">NEWS & EVENT CATEGORY ADD/EDIT</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">
                
				
				<!-- Basic table -->
				<div class="card">
                    <div id="alert"></div>
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Add/Edit NEWS & EVENT CATEGORY</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a> -->
		                		<!-- <a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					

                    <form action="<?php echo base_url(); ?>adminnews/addnewscategory" method='post'  enctype="multipart/form-data">
                    <div class="container">
                        <div class="row">
                       

                         <div class="col-lg-8 offset-lg-2">

                             

                               

                                <div class="form-inline">
                                    <div class="col-lg-4">
                                    <label for="">Name Category ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-8">
                                    <input type="text" name="namethai" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                    <div class="col-lg-4">
                                    <label for="">Name Category Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-8">
                                    <input type="text" name="nameeng" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                    <div class="col-lg-4">
                                    <label for="">Meta Page Title&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-8">
                                    <input type="text" name="blog_category_meta_title" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-4">
                                    <label for="">Meta Description&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-8">
                                    <input type="text" name="blog_category_meta_description" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-4">
                                    <label for="">Meta Keyword&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-8">
                                    <input type="text" name="blog_category_meta_keyword" value="" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Status&nbsp;&nbsp;:&nbsp;&nbsp;
                                    <label class="form-check-label">
										<input type="checkbox" name="blog_category_status" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </label>
                                   
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-12 text-center">
                        <button type="button" value="reset" class="btn btn-danger" name="reset">cancel</button>
                        <input type="submit" value="save" class="btn btn-success" name="save">
                        </div>

                    </div>

                     <?php echo form_close();?>



				
			</div>
            <!-- /content area -->
            
           

		