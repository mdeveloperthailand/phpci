<!-- <?php print_r($newsbyblogiddata); ?> -->
<section class="settopnewsevent">
    <div class="container-fluid">
        <div class="row setmglr80res">
            <div class="col-lg-12">
                <p class="detailpro mg-0"><a href="" onclick="window.history.go(-1); return false;" class="setpointer"><i class="fas fa-long-arrow-alt-left"></i> BACK</a><span class="float-right setbreadcrumb detailpro "> <a href="<?php echo base_url();?>" class="setpointer">Home</a> > <a href="<?php echo base_url();?>news" class="setpointer">News & Event</a> > <span class="setlastbreadcrumb"><?php echo $newsbyblogiddata['blog_category_name'];?></span></span></p>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row mgt-50 setmglr80res">
            <div class="col-lg-12">
                <h1 class="setprodetailtextid"><?php echo $newsbyblogiddata['blog_description_subject'];?></h1>
                <p class="setdatenewsdetail"><?php echo date('d',strtotime($newsbyblogiddata['update_date'])); ?>/<?php echo date('m',strtotime($newsbyblogiddata['update_date'])); ?>/<?php echo date('Y',strtotime($newsbyblogiddata['update_date'])); ?></p>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row mgt-50 setmglr80res">
            <div class="col-lg-12 setnewsbordercolright">
            <?php echo $newsbyblogiddata['blog_description_detail'];?>
            </div>

            <div class="col-lg-12 setbgfooternews">
                
                <div class="row">
                <div class="col-12">
                    <p class="newsheadlinefooter text-white text-center">สามารถจองสินค้า หรือสอบถามรายละเอียดเพิ่มเติมได้ที่..</p>
                </div>
                </div>

                <div class="row text-center">
                    <div class="col-lg-4">
                    <span class="mglr-10"><a href="#" target="_blank" class="setpointer"><i class="fab fa-facebook-f fa-3x text-white"></i></a></span>
                        <p class="newscontactfooter text-white">www.facebook.com/smegthai</p>
                    </div>
                    <div class="col-lg-4">
                    <span class="mglr-10"><a href="#" target="_blank" class="setpointer"><i class="far fa-envelope fa-3x text-white"></i></a></span>
                        <p class="newscontactfooter text-white">lifecenter@penk.co.th</p>
                    </div>
                    <div class="col-lg-4">
                    <span class="mglr-10"><a href="http://line.me/ti/p/%40smegthai" target="_blank" class="setpointer"><i class="fab fa-line fa-3x text-white"></i></a></span>
                        <p class="newscontactfooter text-white">@smegthai (มี @)</p>
                    </div>
                </div>

            </div>

            <div class="col-lg-12 setbgfooterlastsec">
                
                <div class="row">
                <div class="col-12 mgt-50">
                    <p class="newsheadlinelastsec text-white text-center">บริษัท เพ็น เค อินเตอร์เทรดดิ้ง จำกัด
                    <br>PEN K LIFE CENTER<br>
เชิงบันไดขึ้นลงสถานีรถไฟฟ้าพระโขนง (ทางออก 1) ถ.สุขุมวิท กรุงเทพมหานคร<br>
โทร. 02 714 0785 – 6</p>
                </div>
                </div>

                <div class="row text-center">
                <div class="col-12 mgt-50 mgb-50">
                    <a href="" class="setpointer btn btndetailpro">ข้อมูลเกี่ยวกับโชว์รูมสินค้าคลิ๊กที่นี่</a>
                </div>
                </div>

            </div>

            <div class="col-lg-12">
                <p class="setbacknewsdetail mg-0 text-center mgt-20"><a href="<?php echo base_url();?>news/newscategory/<?php echo $newsbyblogiddata['blog_category_id'];?>" class="setpointer"><i class="fas fa-long-arrow-alt-left"></i> BACK</a></p>
            </div>

        </div>
    </div>
</section>