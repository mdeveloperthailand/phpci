<!-- <?php echo $this->lang->line("home"); ?> -->
<section>
    <!--Carousel Wrapper-->
<div id="video-carousel-example" class="carousel slide carousel-fade">
    <!--Indicators-->
    <ol class="carousel-indicators">
    <?php
    $i = 1;
        while ($i <= count($homeslidedata)) {
            if ($i==1) {
           echo "<li data-target='#video-carousel-example' data-slide-to='".($i-1)."' class='active'></li>";
            }else{
           echo "<li data-target='#video-carousel-example' data-slide-to='".($i-1)."'></li>";
            }
            $i++;
        }
    ?>
     <!--    <li data-target="#video-carousel-example" data-slide-to="0" class="active"></li>
        <li data-target="#video-carousel-example" data-slide-to="1"></li>
        <li data-target="#video-carousel-example" data-slide-to="2"></li> -->
    </ol>
    <!--/.Indicators-->
    <!--Slides-->
    <div class="carousel-inner" role="listbox">

        <?php
            foreach ($homeslidedata as $key => $value) {
                $active = "";
                if ($key==0) {
                    $active = "active";
                }
                if(strpos($value['homeslide_image'], 'mp4'))                
                {
                    echo "<div class='carousel-item ".$active."' data-interval='27850'>
                        <video class='video-fluid' autoplay loop muted id='video-background'>
                            <source src='".base_url()."image/homeslide/".$value['homeslide_image']."' type='video/mp4' />
                        </video>
                    </div>";
                }else{
                    echo "<div class='carousel-item ".$active."' data-interval='3000'>
                        <img src='".base_url()."image/homeslide/".$value['homeslide_image']."' alt='".$value['homeslide_imagealt']."' class='img-fluid testss' width='100%'>
                    </div>";
                }
                
            }
        ?>
       

    </div>
    <!--/.Slides-->
    <!--Controls-->
    <a class="carousel-control-prev" href="#video-carousel-example" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#video-carousel-example" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!--/.Controls-->

</div>
<!--Carousel Wrapper-->
</section>

<section>
<img src="<?php echo base_url(); ?>image/scrolldown.png" class="img-fluid btnscrolldown"  alt="">
 <!--Carousel Wrapper-->
 <div id="sectwo" class="carousel slide carousel-fade">
      <!--Indicators-->
      <ol class="carousel-indicators sectwo">
                    <?php
    $i = 1;
        while ($i <= count($featureproductdata)) {
            if ($i==1) {
           echo "<li data-target='#sectwo' data-slide-to='".($i-1)."' class='active'></li>";
            }else{
           echo "<li data-target='#sectwo' data-slide-to='".($i-1)."'></li>";
            }
            $i++;
        }
    ?>
                </ol>
                <!--/.Indicators-->
    <!--Slides-->
    <div class="carousel-inner" role="listbox">

        <?php
                $i = 1;
            foreach ($featureproductdata as $key => $value) {
                if ($i==1) {
                    $active = "active";
                }else{
                    $active = "";
                }
                echo "<div class='carousel-item setimagesectwo ".$active."' data-interval='3000'>
                <div class='row'>
                <div class='col-lg-3 mgt-80 setpadcaptionsectwo'>
                    <div class='setdivborderbottom'><span class='settextsectwo1'>Feature Product<span></div>
                    <br>
                    <br>
                    <br>
                    <h4 class='settextsectwo2'>".$value['feature_h2']."</h4>
                    <h1 class='settextsectwo3'>".wordwrap($value['feature_h1'],6,'<br>',TRUE)."</h1>
                    <span class='settextsectwo4'>
                    ".$value['feature_h3']."
                    </span>
                    <br/>
                    <br/>
                    <a href='".$value['feature_link']."' class='btn setbtnreadmore'>Read more</a>
                </div> 
                <div class='col-lg-9'>            
                    <img src='".base_url()."image/featureproduct/".$value['feature_image']."' class='img-fluid' width='100%'>
                </div>
                </div>
            </div>";
            $i++;
            }
            
        ?>

         <!-- <div class="carousel-item setimagesectwo" data-interval="3000">
            <div class="row">
            <div class="col-lg-3 mgt-80 setpadcaptionsectwo">
                <div class="setdivborderbottom"><span class="settextsectwo1">Feature Product<span></div>
                <br>
                <br>
                <br>
                <h4 class="settextsectwo2">Speed SMEG</h4>
                <h1 class="settextsectwo3">kettle</h1>
                <span class="settextsectwo4">
                    กาต้มน้ำร้อนไฟฟ้า ที่ทำให้เครื่องดื่มร้อนๆ<br>
                    ของคุณ เป็นเครื่องดื่มที่แสนพิเศษ
                </span>
                <br/>
                <br/>
                <a href="#" class="btn setbtnreadmore">Read more</a>
            </div> 
            <div class="col-lg-9">            
                <img src="<?php echo base_url(); ?>image/sectwopic.png" class='img-fluid' width="100%">
            </div>
            </div>
        </div> -->

        
    </div>
    <!--/.Slides-->
    <!--Controls-->
    <a class="carousel-control-prev control1" href="#sectwo" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon1 sectwo" aria-hidden="true"><img src="<?php echo base_url(); ?>image/arrowl.png"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next control1" href="#sectwo" role="button" data-slide="next">
        <span class="carousel-control-next-icon1 sectwo" aria-hidden="true"><img src="<?php echo base_url(); ?>image/arrowr.png"></span>
        <span class="sr-only">Next</span>
    </a>
    <!--/.Controls-->

</div>
<!--Carousel Wrapper-->

</section>




<section>
    
    <div class="container-fluid">
    <div class="row mgt-80">
        <div class="col-lg-4 offset-lg-4">
            <div class="text-center"><h3 class="setheadsection">News<span class="setheadborder"> & </span>Event </h3></div>
        </div>
    </div>
    </div>
    <div class="container-fluid">
    <div class="row">
       
        <?php 
        foreach ($newsdata as $key => $value) {
            ?>
             <a href="<?php echo base_url(); ?>news/newsdetail/<?php echo $value['blog_id'];?>" class="setnewshome">
<div class="col-lg-4 setmarandpadzero setopa">
            <figure>
                <img src="<?php echo base_url(); ?>image/blog/<?php echo $value['blog_image'];?>" class='img-fluid' width="100%">
            </figure>
            <div class="setcaptionsecthree">
            <p class="sethomenews one"><?php echo $value['blog_type_name'];?></p>
            <h4 class="sethomenews"><?php echo $value['blog_description_subject'];?></h2>
            </a>
            </div>
        </div>
            <?php
           
        }
    ?>
        
    </div>
    <div class="row setviewmoresecthree d-block">
      <div class="col-lg-4 offset-lg-4"><a href="<?php echo base_url(); ?>news" class="btn d-block text-center"><span class="setbtnreadmore">View more<span></a></div>
    </div>
    </div>
</section>



<section>
<div class="container-fluid">
    <div class="row mgt-50">
        <div class="col-lg-4 offset-lg-4">
            <div class="text-center"><h3 class="setheadsection">Per<span class="setheadborder">smeg</span>tive </h3></div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 offset-lg-4">
            <div class="text-center"><h1 class="setsubpersmegtive">EAT MEETS ART</h1></div>
        </div>
    </div>
    </div>
<div class="container">
	<div class="row mgt-50">
		<div class="col-lg-3 pdlr-10">
            <figure>
                <img src="<?php echo base_url(); ?>image/persmegtive/<?php echo $persmegtivedata[0]['persmegtive_image'];?>" class="img-fluid imagesecthree setheightbigsecthree ">
                    <div class="middlesecthree">
                        <a href="<?php echo base_url(); ?>persmegtive/persmegtivedetail/<?php echo $persmegtivedata[0]['persmegtive_id'];?>" class="setnewshome">
                        <div class="textsecthree"><?php echo wordwrap($persmegtivedata[0]['persmegtive_name'],6,"<br>\n");?></div>
                        </a>
                    </div>
            </figure>
		</div>
		<div class="col-lg-9 pdlr-10">
			<div class="row">
				<div class="col-lg-4 pdlr-10">
                    <figure>
                        <img src="<?php echo base_url(); ?>image/persmegtive/<?php echo $persmegtivedata[1]['persmegtive_image'];?>" class="img-fluid imagesecthree setheightothersecthree">
                        <div class="middlesecthree">
                        <a href="<?php echo base_url(); ?>persmegtive/persmegtivedetail/<?php echo $persmegtivedata[1]['persmegtive_id'];?>" class="setnewshome">
                            <div class="textsecthree"><?php echo wordwrap($persmegtivedata[1]['persmegtive_name'],6,"<br>\n");?></div>
                        </a>
                        </div>
                    </figure>
				</div>
				<div class="col-lg-4 pdlr-10">
                    <figure>
                        <img src="<?php echo base_url(); ?>image/persmegtive/<?php echo $persmegtivedata[2]['persmegtive_image'];?>" class="img-fluid imagesecthree setheightothersecthree">
                        <div class="middlesecthree">
                        <a href="<?php echo base_url(); ?>persmegtive/persmegtivedetail/<?php echo $persmegtivedata[2]['persmegtive_id'];?>" class="setnewshome">
                            <div class="textsecthree"><?php echo wordwrap($persmegtivedata[2]['persmegtive_name'],6,"<br>\n");?></div>
                        </a>
                        </div>
                    </figure>
				</div>
				<div class="col-lg-4 pdlr-10">
                    <figure>
                        <img src="<?php echo base_url(); ?>image/persmegtive/<?php echo $persmegtivedata[3]['persmegtive_image'];?>" class="img-fluid imagesecthree setheightothersecthree">
                        <div class="middlesecthree">
                        <a href="<?php echo base_url(); ?>persmegtive/persmegtivedetail/<?php echo $persmegtivedata[3]['persmegtive_id'];?>" class="setnewshome">
                            <div class="textsecthree"><?php echo wordwrap($persmegtivedata[3]['persmegtive_name'],6,"<br>\n");?></div>
                        </a>
                        </div>
                    </figure>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 pdlr-10">
                    <figure>
                        <img src="<?php echo base_url(); ?>image/persmegtive/<?php echo $persmegtivedata[4]['persmegtive_image'];?>" class="img-fluid imagesecthree setheightothersecthree">
                        <div class="middlesecthree">
                        <a href="<?php echo base_url(); ?>persmegtive/persmegtivedetail/<?php echo $persmegtivedata[4]['persmegtive_id'];?>" class="setnewshome">
                            <div class="textsecthree"><?php echo wordwrap($persmegtivedata[4]['persmegtive_name'],6,"<br>\n");?></div>
                        </a>
                        </div>
                    </figure>
				</div>
				<div class="col-lg-8 pdlr-10">
                    <figure>
                        <img src="<?php echo base_url(); ?>image/persmegtive/<?php echo $persmegtivedata[5]['persmegtive_image'];?>" class="img-fluid imagesecthree setheightothersecthree">
                        <div class="middlesecthree">
                        <a href="<?php echo base_url(); ?>persmegtive/persmegtivedetail/<?php echo $persmegtivedata[5]['persmegtive_id'];?>" class="setnewshome">
                            <div class="textsecthree"><?php echo wordwrap($persmegtivedata[5]['persmegtive_name'],6,"<br>\n");?></div>
                        </a>
                        </div>
                    </figure>
				</div>
			</div>
		</div>
	</div>
</div>
</section>


<section>
    <div class="container-fluid">
    <div class="row mgt-50">
        <div class="col-lg-4 offset-lg-4">
            <div class="text-center"><h3 class="setheadsection">Ins<span class="setheadborder">tagr</span>am</h3></div>
        </div>
    </div>
    </div>

    <div class="container-fluid">
        <div class="row">
        <div data-is data-is-api="asset/instashow/api/" 
          data-is-source="@muradosmann"
          data-is-width="100%"
          data-is-columns="5" 
          data-is-rows="2" 
          data-is-gutter="0" 
          data-is-effect="slide" 
          data-is-scroll-control="ture" 
          data-is-arrows-control="false" 
          data-is-speed="5000"
          data-is-auto="8000"
          class="hidden-xs">
    </div>
        </div>
    </div>
</section>

<section class="bgsecsix">

    <div class="container-fluid setsecsixcontainer">
        <!--Carousel Wrapper-->
 <div id="secsix" class="carousel slide carousel-fade bgwhite">
      <!--Indicators-->
      <ol class="carousel-indicators secsix">
                   <?php
    $i = 1;
        while ($i <= count($smegreviewdata)) {
            if ($i==1) {
           echo "<li data-target='#secsix' data-slide-to='".($i-1)."' class='active'></li>";
            }else{
           echo "<li data-target='#secsix' data-slide-to='".($i-1)."'></li>";
            }
            $i++;
        }
    ?>
                </ol>
                <!--/.Indicators-->
    <!--Slides-->
    <div class="carousel-inner" role="listbox">

        <?php
         $i = 1;
        foreach ($smegreviewdata as $key => $value) {
            if ($i==1) {
                $active = "active";
            }else{
                $active = "";
            }
            ?>

             <div class="carousel-item <?php echo $active; ?>" data-interval="3000">
            <div class="row">
            <div class="col-lg-6 pdlr-20 setpdb20res">
            <div class="row mgt-50">
                <div class="col-lg-12  text-center center-block">
                    <div class="text-center"><p class="setheadsectionsix forcenowrap">Sme<span class="setheadborder">g&nbsp;Re</span>view</p></div>
                    <br/>
                    <h4 class="settextsecsix2 text-center"><?php echo $value['review_name']; ?></h4>
                    <p class="settextsecsix3 text-center"><?php echo $value['review_short']; ?></p>
                    <a href="<?php echo base_url(); ?>review/reviewdetail/<?php echo $value['review_id']; ?>" class="btn setbtnreadmore mgt-10">Read more</a>
                </div>
            </div>
            </div> 
            <div class="col-lg-6">            
                <img src="<?php echo base_url(); ?>image/review/<?php echo $value['review_image']; ?>" alt="<?php echo $value['review_imagealt']; ?>" class='img-fluid' width="100%">
            </div>
            </div>
        </div>
          
            <?php
              $i++;
        }
        ?>
       
        
       

    </div>
    <!--/.Slides-->
    <!--Controls-->
    <a class="carousel-control-prev control6left" href="#secsix" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon1" aria-hidden="true"><img src="<?php echo base_url(); ?>image/arrowl.png"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next control6right" href="#secsix" role="button" data-slide="next">
        <span class="carousel-control-next-icon1" aria-hidden="true"><img src="<?php echo base_url(); ?>image/arrowr.png"></span>
        <span class="sr-only">Next</span>
    </a>
    <!--/.Controls-->

</div>
<!--Carousel Wrapper-->
    </div>
</section>


<section class="bgsecseven">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 offset-lg-4 mgt-80">
                <div class="text-center"><h3 class="setheadsection text-white">Sme<span class="setheadborder">g&nbsp;S</span>tory</h3></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 mgt-100 text-center">
                <div class="text-center"><h1 class="text-white setthintext"><?php echo $smegstorydata['setpage_h1']; ?></h1></div>
                <a href="home/story" class="btn setbtnreadmore text-white mgt-20">Read more</a>
            </div>
        </div>
    </div>
</section>


<script>
  var t;
   var start = $('#video-carousel-example').find('.carousel-item.active').attr('data-interval');
   t = setTimeout("$('#video-carousel-example').carousel({interval: 1000});", start-1000);
   
   $('#video-carousel-example').on('slid.bs.carousel', function () {  
        clearTimeout(t);  
        var duration = $(this).find('.carousel-item.active').attr('data-interval');
       
        $('#video-carousel-example').carousel('pause');
        t = setTimeout("$('#video-carousel-example').carousel();", duration-1000);
   })
   
   $('.carousel-control-next').on('click', function(){
       clearTimeout(t);   
   });
   
   $('.carousel-control-prev').on('click', function(){
       clearTimeout(t);   
   });

   $(document).ready(function () {
  $('.btnscrolldown').click(function() {
  $('html, body').animate({
    scrollTop: $("div#sectwo").offset().top
  }, 1000)
    });
});
</script>


