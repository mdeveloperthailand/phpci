


		<!-- Main content -->
		<div class="content-wrapper">
			

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">MANAGE review</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<!-- <a href="#" class="breadcrumb-item">Link</a> -->
							<span class="breadcrumb-item active">review ADD/EDIT</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">
                
				
				<!-- Basic table -->
				<div class="card">
                    <div id="alert"></div>
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Add/Edit review</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a> -->
		                		<!-- <a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					

                    <form action="<?php echo base_url(); ?>adminreview/editreview/<?php echo $reviewbyiddata['review_id']; ?>" method='post'  enctype="multipart/form-data">
                    <div class="container">
                        <div class="row">
                       

                         <div class="col-lg-8 offset-lg-2">

                             <label class="col-lg-2 col-form-label font-weight-semibold">Thumbnail:</label>
								<div class="col-lg-12 text-center">
                                <img src="<?php echo base_url(); ?>image/review/<?php echo $reviewbyiddata['review_image']; ?>" width="100px" alt="">
                                <input type="file" name="imagereview" class="file-input form-control-lg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                                <input type="hidden" name="imagereviewname" value="<?php echo $reviewbyiddata['review_image']; ?>">
								</div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">Image Alt&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="review_imagealt" value="<?php echo $reviewbyiddata['review_imagealt']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">review Name ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="namethai" value="<?php echo $reviewbyiddata['review_name_th']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">review Name Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="nameeng" value="<?php echo $reviewbyiddata['review_name_en']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">Short Description ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="shortthai" value="<?php echo $reviewbyiddata['review_short_th']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">Short Description Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="shorteng" value="<?php echo $reviewbyiddata['review_short_en']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group mgt-20">
                                <div class="col-lg-12">
                                    <label for="">review Detail Thai&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <textarea id="summernote" name="detailthai"><?php echo $reviewbyiddata['review_detail_th']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group mgt-20">
                                <div class="col-lg-12">
                                    <label for="">review Detail Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <textarea id="summernote1" name="detaileng"><?php echo $reviewbyiddata['review_detail_en']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group mgt-20">
                                    <div class="col-lg-12">
                                    <label for="">Meta Page Title&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="review_meta_title" value="<?php echo $reviewbyiddata['review_meta_title']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Meta Description&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="review_meta_description" value="<?php echo $reviewbyiddata['review_meta_description']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Meta Keyword&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="review_meta_keyword" value="<?php echo $reviewbyiddata['review_meta_keyword']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Order&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="review_order" value="<?php echo $reviewbyiddata['review_order']; ?>" class="form-control" maxlength='2' size='2'>
                                    </div>
                                </div>
                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Status&nbsp;&nbsp;:&nbsp;&nbsp;
                                    <label class="form-check-label">
                                    <?php
                                            if ($reviewbyiddata['review_status']==1) {
                                               echo "<input type='checkbox' name='review_status' class='form-check-input-switchery' checked data-fouc>";
                                            } else {
                                                echo "<input type='checkbox' name='review_status' class='form-check-input-switchery' data-fouc>";
                                            }
                                            
                                        ?>									
                                    </label>
                                    </label>
                                   
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-12 text-center">
                        <button type="button" value="reset" class="btn btn-danger" name="reset">cancel</button>
                        <input type="submit" value="save" class="btn btn-success" name="save">
                        </div>

                    </div>

                     <?php echo form_close();?>



				
			</div>
            <!-- /content area -->
            
            <script>
		$(document).ready(function() {
  $('#summernote').summernote({
    lang: 'ko-KR' // default: 'en-US'
  });
  $('#summernote1').summernote({
    lang: 'ko-KR' // default: 'en-US'
  });
});
		</script>


		