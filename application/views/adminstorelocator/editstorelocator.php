


		<!-- Main content -->
		<div class="content-wrapper">
			

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">MANAGE storelocator</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<!-- <a href="#" class="breadcrumb-item">Link</a> -->
							<span class="breadcrumb-item active">storelocator ADD/EDIT</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">
                
				
				<!-- Basic table -->
				<div class="card">
                    <div id="alert"></div>
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Add/Edit storelocator</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a> -->
		                		<!-- <a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					

                    <form action="<?php echo base_url(); ?>adminstorelocator/editstorelocator/<?php echo $storelocatorbyiddata['storelocator_id']; ?>" method='post'  enctype="multipart/form-data">
                    <div class="container">
                        <div class="row">
                       

                         <div class="col-lg-8 offset-lg-2">

                             <label class="col-lg-2 col-form-label font-weight-semibold">Thumbnail:</label>
								<div class="col-lg-12 text-center">
                                <img src="<?php echo base_url(); ?>image/storelocator/<?php echo $storelocatorbyiddata['storelocator_image']; ?>" width="100px" alt="">
                                <input type="file" name="imagestorelocator" class="file-input form-control-lg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
                                <input type="hidden" name="imagestorelocatorname" value="<?php echo $storelocatorbyiddata['storelocator_image']; ?>">
								</div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">Image Alt&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="storelocator_imagealt" value="<?php echo $storelocatorbyiddata['storelocator_imagealt']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">Location Name ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="namethai" value="<?php echo $storelocatorbyiddata['storelocator_name_th']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">Location Name Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="nameeng" value="<?php echo $storelocatorbyiddata['storelocator_name_en']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">address ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="addressthai" value="<?php echo $storelocatorbyiddata['storelocator_address_th']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">address Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="addresseng" value="<?php echo $storelocatorbyiddata['storelocator_address_en']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">Tel&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="tel" value="<?php echo $storelocatorbyiddata['storelocator_tel']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-12">
                                    <label for="">Period&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="period" value="<?php echo $storelocatorbyiddata['storelocator_time']; ?>" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-group mgt-20">
                                    <div class="col-lg-12">
                                    <label for="">Meta Page Title&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="storelocator_meta_title" value="<?php echo $storelocatorbyiddata['storelocator_meta_title']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Meta Description&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="storelocator_meta_description" value="<?php echo $storelocatorbyiddata['storelocator_meta_description']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Meta Keyword&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="storelocator_meta_keyword" value="<?php echo $storelocatorbyiddata['storelocator_meta_keyword']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Latitude&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="lat" value="<?php echo $storelocatorbyiddata['storelocator_lat']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Longtitude&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="long" value="<?php echo $storelocatorbyiddata['storelocator_long']; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Order&nbsp;&nbsp;:&nbsp;&nbsp;</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <input type="text" name="storelocator_order" value="<?php echo $storelocatorbyiddata['storelocator_order']; ?>" class="form-control" maxlength='2' size='2'>
                                    </div>
                                </div>
                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-12">
                                    <label for="">Status&nbsp;&nbsp;:&nbsp;&nbsp;
                                    <label class="form-check-label">
                                    <?php
                                            if ($storelocatorbyiddata['storelocator_status']==1) {
                                               echo "<input type='checkbox' name='storelocator_status' class='form-check-input-switchery' checked data-fouc>";
                                            } else {
                                                echo "<input type='checkbox' name='storelocator_status' class='form-check-input-switchery' data-fouc>";
                                            }
                                            
                                        ?>									
                                    </label>
                                    </label>
                                   
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-12 text-center">
                        <button type="button" value="reset" class="btn btn-danger" name="reset">cancel</button>
                        <input type="submit" value="save" class="btn btn-success" name="save">
                        </div>

                    </div>

                     <?php echo form_close();?>



				
			</div>
            <!-- /content area -->
            
            <script>
		$(document).ready(function() {
  $('#summernote').summernote({
    lang: 'ko-KR' // default: 'en-US'
  });
  $('#summernote1').summernote({
    lang: 'ko-KR' // default: 'en-US'
  });
});
		</script>


		