<section class="settopnewsevent">
    <div class="container-fluid">
        <div class="row setmglr80res">
            <div class="col-lg-12">
                <p class="setnewstextbreadcrumbleft mg-0">STORE LOCATION<span class="float-right setbreadcrumb"> <a href="<?php echo base_url();?>" class="setpointer">Home</a> > <span class="setlastbreadcrumb">Store Location</span></span>
</p>
            </div>
        </div>
    </div>
</section>
<!-- <?php print_r($smegstorelocatordata);?> -->





<section>
    <div class="container-fluid">
        <div class="row mgt-50 setmglr80res">
                
        
        <?php
            foreach ($smegstorelocatordata as $key => $value) {
        ?>

        <div class="col-lg-6 mgt-20">
                <div class="col-lg-12">
                <div class="row">
                    <img src="<?php echo base_url(); ?>image/storelocator/<?php echo $value['storelocator_image'];?>" alt="" width="100%" height="400px">
                    <button class="setbtnviewmap" data-toggle="modal" data-target="#myMap<?php echo $value['storelocator_id'];?>"><i class="fas fa-map-marker-alt"></i> view map</button>
                </div>
                </div>
                <div class="col-lg-12 setnewsbordercolright">
                    <h2 class="mgt-20 setnewstextheadline"><?php echo $value['storelocator_name'];?></h2>
                    <p class="setnewstextcatemain"><?php echo $value['storelocator_address'];?></p>
                    <a href="tel:<?php echo $value['storelocator_tel'];?>" class="setpointer"><i class="fas fa-phone-volume"></i> <?php echo $value['storelocator_tel'];?></a>
                </div>
            </div>  

             <!-- Modal -->
<div class="modal fade" id="myMap<?php echo $value['storelocator_id'];?>" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <div style="width: 100%"><iframe width="100%" height="450" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=<?php echo $value['storelocator_lat'];?>,+<?php echo $value['storelocator_long'];?>&key=AIzaSyAUg-HrJST7PlCjnS0Na9ZzJMouvfMF4F4" allowfullscreen></iframe></div><br />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>

        <?php
            }
        ?>
            

           

           
            
            

           

            <div class="col-lg-12 mgt-50"><?php echo $links;?></div>


        </div>      

    </div>
</section>



