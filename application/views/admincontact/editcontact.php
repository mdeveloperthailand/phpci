


<!-- Main content -->
<div class="content-wrapper">


<!-- Page header -->
<div class="page-header page-header-light">
<div class="page-header-content header-elements-md-inline">
<div class="page-title d-flex">
<h4> <span class="font-weight-semibold">MANAGE CONTACT</span></h4>
<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>


</div>

<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
<div class="d-flex">
<div class="breadcrumb">
<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<!-- <a href="#" class="breadcrumb-item">Link</a> -->
<span class="breadcrumb-item active">CONTACT ADD/EDIT</span>
</div>

<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>


</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">


<!-- Basic table -->
<div class="card">
<div id="alert"></div>
<div class="card-header header-elements-inline">
<h5 class="card-title">Add/Edit CONTACT</h5>
<div class="header-elements">
<div class="list-icons">
<a class="list-icons-item" data-action="collapse"></a>
<!-- <a class="list-icons-item" data-action="reload"></a> -->
<!-- <a class="list-icons-item" data-action="remove"></a> -->
</div>
</div>
</div>



<form action="<?php echo base_url(); ?>admincontact/editcontact/<?php echo $contactbyiddata['contact_id'];?>" method='post'  enctype="multipart/form-data">
<div class="container">
<div class="row">


<div class="col-lg-8 offset-lg-2">

<label class="col-lg-2 col-form-label font-weight-semibold">Thumbnail footer:</label>
<div class="col-lg-12 text-center">
<img src="<?php echo base_url(); ?>image/<?php echo $contactbyiddata['contact_image']; ?>" width="100px" alt="">
<input type="file" name="imagecontact" class="file-input form-control-lg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
<input type="hidden" name="imagecontactname" value="<?php echo $contactbyiddata['contact_image']; ?>">
</div>



<div class="form-group">
<div class="col-lg-12">
<label for="">Company name ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="namethai" value="<?php echo $contactbyiddata['contact_name_th'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">Company name Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="nameeng" value="<?php echo $contactbyiddata['contact_name_en'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">Address ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="addressthai" value="<?php echo $contactbyiddata['contact_address_th'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">Address Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="addresseng" value="<?php echo $contactbyiddata['contact_address_en'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">Tel&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="tel" value="<?php echo $contactbyiddata['contact_tel'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">Period&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="contact_time" value="<?php echo $contactbyiddata['contact_time'];?>" class="form-control">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="">Facebook&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="contact_facebook" value="<?php echo $contactbyiddata['contact_facebook'];?>" class="form-control">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="">Instagram&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="contact_ig" value="<?php echo $contactbyiddata['contact_ig'];?>" class="form-control">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="">Line ID&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="contact_line" value="<?php echo $contactbyiddata['contact_line'];?>" class="form-control">
</div>
</div>


</div>
</div>
</div>

<div class="row mgt-20 mglr-10 mgb-50">
<div class="col-lg-12 text-center">
<button type="button" value="reset" class="btn btn-danger" name="reset">cancel</button>
<input type="submit" value="save" class="btn btn-success" name="save">
</div>

</div>

<?php echo form_close();?>




</div>
<!-- /content area -->

<script>
$(document).ready(function() {
    $('#summernote').summernote({
        lang: 'ko-KR' // default: 'en-US'
    });
    $('#summernote1').summernote({
        lang: 'ko-KR' // default: 'en-US'
    });
});
</script>


