


<!-- Main content -->
<div class="content-wrapper">


<!-- Page header -->
<div class="page-header page-header-light">
<div class="page-header-content header-elements-md-inline">
<div class="page-title d-flex">
<h4> <span class="font-weight-semibold">MANAGE setpage Story</span></h4>
<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>


</div>

<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
<div class="d-flex">
<div class="breadcrumb">
<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<!-- <a href="#" class="breadcrumb-item">Link</a> -->
<span class="breadcrumb-item active">setpage ADD/EDIT Story</span>
</div>

<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>


</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">


<!-- Basic table -->
<div class="card">
<div id="alert"></div>
<div class="card-header header-elements-inline">
<h5 class="card-title">Add/Edit Story setpage</h5>
<div class="header-elements">
<div class="list-icons">
<a class="list-icons-item" data-action="collapse"></a>
<!-- <a class="list-icons-item" data-action="reload"></a> -->
<!-- <a class="list-icons-item" data-action="remove"></a> -->
</div>
</div>
</div>



<form action="<?php echo base_url(); ?>admincontact/editpagestory/<?php echo $setpagebyiddata['setpage_id'];?>" method='post'  enctype="multipart/form-data">
<div class="container">
<div class="row">


<div class="col-lg-8 offset-lg-2">

<label class="col-lg-2 col-form-label font-weight-semibold">Thumbnail footer:</label>
<div class="col-lg-12 text-center">
<img src="<?php echo base_url(); ?>image/<?php echo $setpagebyiddata['setpage_image']; ?>" width="100px" alt="">
<input type="file" name="imagesetpage" class="file-input form-control-lg" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
<input type="hidden" name="imagesetpagename" value="<?php echo $setpagebyiddata['setpage_image']; ?>">
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">imagealt&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_imagealt" value="<?php echo $setpagebyiddata['setpage_imagealt'];?>" class="form-control" required>
</div>
</div>



<div class="form-group">
<div class="col-lg-12">
<label for="">h1 ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_h1_th" value="<?php echo $setpagebyiddata['setpage_h1_th'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">h1 Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_h1_en" value="<?php echo $setpagebyiddata['setpage_h1_en'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">h2 ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_h2_th" value="<?php echo $setpagebyiddata['setpage_h2_th'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">h2 Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_h2_en" value="<?php echo $setpagebyiddata['setpage_h2_en'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">short ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_short_th" value="<?php echo $setpagebyiddata['setpage_short_th'];?>" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">short Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_short_en" value="<?php echo $setpagebyiddata['setpage_short_en'];?>" class="form-control" required>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">Detail Thai&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<textarea id="summernote" name="setpage_detail_th"><?php echo $setpagebyiddata['setpage_detail_th'];?></textarea>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">Detail Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<textarea id="summernote1" name="setpage_detail_en"><?php echo $setpagebyiddata['setpage_detail_en'];?></textarea>
</div>
</div>


<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">Meta Page Title&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_meta_title" value="<?php echo $setpagebyiddata['setpage_meta_title']; ?>" class="form-control">
</div>
</div>
<div class="form-group mgt-20 mgb-50">
<div class="col-lg-12">
<label for="">Meta Description&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_meta_description" value="<?php echo $setpagebyiddata['setpage_meta_description']; ?>" class="form-control">
</div>
</div>
<div class="form-group mgt-20 mgb-50">
<div class="col-lg-12">
<label for="">Meta Keyword&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="setpage_meta_keyword" value="<?php echo $setpagebyiddata['setpage_meta_keyword']; ?>" class="form-control">
</div>
</div>

</div>
</div>
</div>

<div class="row mgt-20 mglr-10 mgb-50">
<div class="col-lg-12 text-center">
<button type="button" value="reset" class="btn btn-danger" name="reset">cancel</button>
<input type="submit" value="save" class="btn btn-success" name="save">
</div>

</div>

<?php echo form_close();?>




</div>
<!-- /content area -->

<script>
 $(document).ready(function() {
    $('#summernote').summernote({
    height: 200,
    callbacks: {
        onImageUpload : function(files, editor, welEditable) {

             for(var i = files.length - 1; i >= 0; i--) {
                     sendFile(files[i], this);
            }
        }
    }
});
$('#summernote1').summernote({
    height: 200,
    callbacks: {
        onImageUpload : function(files, editor, welEditable) {

             for(var i = files.length - 1; i >= 0; i--) {
                     sendFile1(files[i], this);
            }
        }
    }
});
        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: "../<?php base_url();?>home/uploadsummernote",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    // editor.insertImage(welEditable, url);
                    $('#summernote').summernote('editor.insertImage', url);
                    alert('UPLOAD SUCCESSFULLY');
                }
            });
        }

        function sendFile1(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            $.ajax({
                data: data,
                type: "POST",
                url: "../<?php base_url();?>home/uploadsummernote",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    // editor.insertImage(welEditable, url);
                    $('#summernote1').summernote('editor.insertImage', url);
                    alert('UPLOAD SUCCESSFULLY');
                }
            });
        }


    });

</script>


