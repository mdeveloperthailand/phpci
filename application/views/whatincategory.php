<!--<pre><?php print_r($productdatabycategoryiddata); ?>-->
<section id="bgwhatinstore" style="background-image: url('<?php echo base_url(); ?>image/whatincategorymain.png');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <p class="text-white text-center setwhatincategorytextsmall">SMEG PRODUCT RANGE</p>
                <h1 class="text-white text-center setwhatincategorytextmain">REFRIDGERATORS</h1>
            </div>
        </div>
    </div>
</section>

<?php if (!empty($productdatabycategoryiddata)) {?>
<section id="sectwo">
<img src="<?php echo base_url(); ?>image/scrolldown.png" class="img-fluid btnscrolldown whatinstorebg"  alt="">
<div class="container-fluid">
	<div class="row mgt-50 mglr-80 whatincategory">


		<div class="col-md-6">

            <div class="row setborderactivewhatincategory">
                <div class="col-lg-6 text-center">
                <a href="<?php echo base_url(); ?>whatinstore/productdetail/<?php echo $productdatabycategoryiddata[0]['product_id']; ?>" class="setpointer">
                    <img src="<?php echo base_url(); ?>image/product/<?php echo $productdatabycategoryiddata[0]['product_image']; ?>" class="img-fluid">
                </a>
                </div>

                <div class="col-lg-6">
                <a href="<?php echo base_url(); ?>whatinstore/productdetail/<?php echo $productdatabycategoryiddata[0]['product_id']; ?>" class="setpointer">
                    <h2 class="setwhatinstoretextmain mg-0 whatincategory centerres"><?php echo $productdatabycategoryiddata[0]['product_model']; ?></h2>
                    <p class="setnewstextcatemain whatincate centerres"><?php echo $productdatabycategoryiddata[0]['product_description_name']; ?></p>
                    <p class="setwhatincatedes centerres"><?php echo $productdatabycategoryiddata[0]['product_description_short_detail']; ?></p>
                    <p class="setwhatincatedes centerres">฿ <?php echo number_format($productdatabycategoryiddata[0]['product_price']); ?></p>
                </a>
                </div>

            </div>

        </div>
        
		<div class="col-md-6">

			<div class="row">
            <?php if (!empty($productdatabycategoryiddata[1])) {?>
				<div class="col-md-6 centerres setborderactivewhatincategory">
                <a href="<?php echo base_url(); ?>whatinstore/productdetail/<?php echo $productdatabycategoryiddata[1]['product_id']; ?>" class="setpointer">
                <div class="col-md-12 text-center">
                    <img src="<?php echo base_url(); ?>image/product/<?php echo $productdatabycategoryiddata[1]['product_image']; ?>" class="img-fluid mgt-30" width="40%" alt="">
                </div>
                    <h2 class="setwhatinstoretextmain mg-0 whatincategory subproduct centerres"><?php echo $productdatabycategoryiddata[1]['product_model']; ?><span class='float-right'>฿ <?php echo number_format($productdatabycategoryiddata[1]['product_price']); ?></span></h2>
                    <p class="setnewstextcatemain whatincate centerres"><?php echo $productdatabycategoryiddata[1]['product_description_name']; ?></p>
                    <img src="<?php echo base_url(); ?>image/ribbon/<?php echo $productdatabycategoryiddata[1]['ribbon_image']; ?>" class="setpositionrb" alt="">
				</a>
                </div>
            <?php } ?>
            <?php if (!empty($productdatabycategoryiddata[2])) {?>
				<div class="col-md-6 centerres setborderactivewhatincategory">
                <a href="<?php echo base_url(); ?>whatinstore/productdetail/<?php echo $productdatabycategoryiddata[2]['product_id']; ?>" class="setpointer">
                <div class="col-md-12 text-center">
                    <img src="<?php echo base_url(); ?>image/product/<?php echo $productdatabycategoryiddata[2]['product_image']; ?>" class="img-fluid mgt-30" width="40%" alt="">
                </div>
                    <h2 class="setwhatinstoretextmain mg-0 whatincategory subproduct centerres"><?php echo $productdatabycategoryiddata[2]['product_model']; ?><span class='float-right'>฿ <?php echo number_format($productdatabycategoryiddata[2]['product_price']); ?></span></h2>
                    <p class="setnewstextcatemain whatincate centerres"><?php echo $productdatabycategoryiddata[2]['product_description_name']; ?></p>
                    <img src="<?php echo base_url(); ?>image/ribbon/<?php echo $productdatabycategoryiddata[2]['ribbon_image']; ?>" class="setpositionrb" alt="">
                </a>
                </div>
            </div>
             <?php } ?>
			<div class="row">
            <?php if (!empty($productdatabycategoryiddata[3])) {?>

                <div class="col-md-6 centerres setborderactivewhatincategory">
                <a href="<?php echo base_url(); ?>whatinstore/productdetail/<?php echo $productdatabycategoryiddata[3]['product_id']; ?>" class="setpointer">
                <div class="col-md-12 text-center">
                    <img src="<?php echo base_url(); ?>image/product/<?php echo $productdatabycategoryiddata[3]['product_image']; ?>" class="img-fluid mgt-30" width="40%" alt="">
                </div>
                    <h2 class="setwhatinstoretextmain mg-0 whatincategory subproduct centerres"><?php echo $productdatabycategoryiddata[3]['product_model']; ?><span class='float-right'>฿ <?php echo number_format($productdatabycategoryiddata[3]['product_price']); ?></span></h2>
                    <p class="setnewstextcatemain whatincate centerres"><?php echo $productdatabycategoryiddata[3]['product_description_name']; ?></p>
                    <img src="<?php echo base_url(); ?>image/ribbon/<?php echo $productdatabycategoryiddata[3]['ribbon_image']; ?>" class="setpositionrb" alt="">
                </a>
                </div>
            
                 <?php } ?>
            <?php if (!empty($productdatabycategoryiddata[4])) {?>
				<div class="col-md-6 centerres setborderactivewhatincategory">
                <a href="<?php echo base_url(); ?>whatinstore/productdetail/<?php echo $productdatabycategoryiddata[4]['product_id']; ?>" class="setpointer">
                <div class="col-md-12 text-center">
                    <img src="<?php echo base_url(); ?>image/product/<?php echo $productdatabycategoryiddata[4]['product_image']; ?>" class="img-fluid mgt-30" width="40%" alt="">
                </div>
                    <h2 class="setwhatinstoretextmain mg-0 whatincategory subproduct centerres"><?php echo $productdatabycategoryiddata[4]['product_model']; ?><span class='float-right'>฿ <?php echo number_format($productdatabycategoryiddata[4]['product_price']); ?></span></h2>
                    <p class="setnewstextcatemain whatincate centerres"><?php echo $productdatabycategoryiddata[4]['product_description_name']; ?></p>
                    <img src="<?php echo base_url(); ?>image/ribbon/<?php echo $productdatabycategoryiddata[4]['ribbon_image']; ?>" class="setpositionrb" alt="">
                </a>
                </div>
            </div>
                 <?php } ?>
            </div>
            

        </div>



        <div class="col-lg-12">
        <div class="row">

                <?php
                    foreach ($productdatabycategoryiddata as $key => $value) {
                       if ($key>=5) {
                        echo "<div class='col-lg-3 col-sm-3 centerres setborderactivewhatincategory '>
                        <a href='".base_url()."whatinstore/productdetail/".$value['product_id']."' class='setpointer'>
                         <div class='col-lg-12 text-center'>
                        <img src='".base_url()."image/product/".$value['product_image']."' class='img-fluid' width='40%' alt=''>
                        </div>
                        <h2 class='setwhatinstoretextmain mg-0 whatincategory subproduct centerres'>".$value['product_model']." <span class='float-right'>฿ ".number_format($value['product_price'])."</span></h2>
                        <p class='setnewstextcatemain whatincate centerres'>".$value['product_description_name']."</p>
                        <img src='".base_url()."image/ribbon/".$value['ribbon_image']."' class='setpositionrb' alt=''>
                    </div>";
                       }
                    }
                ?>
                


        </div>       
        </div>


	</div>
    <div class="col-lg-12 mgt-50"><?php echo $links;?></div>

</div>

</section>
                <?php }else{echo "<h1>NO DATA</h1>";} ?>


<script>
$(document).ready(function () {
  $('.btnscrolldown').click(function() {
  $('html, body').animate({
    scrollTop: $("section#sectwo").offset().top
  }, 1000)
    });
});
</script>

<style type="text/css">
    .setpointer.langer{
        color: #fff;
    }
</style>