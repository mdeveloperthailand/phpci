<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inter Vision - Smeg Back Office</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>asset/admin/css/colors.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/smeg.css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/main/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->
    
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/daterangepicker.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/anytime.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/pickadate/legacy.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/validation/validate.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/inputs/touchspin.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/styling/switch.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    
	<!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>asset/admin/js/app.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/form_validation.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/form_select2.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/datatables_basic.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/picker_date.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/login.js"></script>


    <!-- /theme JS files -->
    

</head>

<body>
<?php
if($this->session->flashdata('message') != ''){
     echo $this->session->flashdata('message');
}else{
     echo $this->session->flashdata('message');
}
?>
 
<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Content area -->
    <div class="content d-flex justify-content-center align-items-center">

        <!-- Login card -->
        <?php echo form_open('adminlogin/login','class=login-form');?>
            <div class="card mb-0">
                <div class="card-body">
                    <div class="text-center mb-3">
                        <img src="<?php echo base_url(); ?>image/logo.png" class="img-fluid mgb-20">
                        <h5 class="mb-0">Login to your account</h5>
                        <span class="d-block text-muted">Your credentials</span>
                    </div>

                    <div class="form-group form-group-feedback form-group-feedback-left">
                        <input type="text" name="username" class="form-control" placeholder="Username">
                        <div class="form-control-feedback">
                            <i class="icon-user text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group form-group-feedback form-group-feedback-left">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <div class="form-control-feedback">
                            <i class="icon-lock2 text-muted"></i>
                        </div>
                    </div>

                   

                    <div class="form-group">
                        <button type="submit" name="submit" value="login" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
                    </div>

                    
                </div>
            </div>
        </form>
        <!-- /login card -->

    </div>
    <!-- /content area -->

</div>
<!-- /main content -->

</div>
<!-- /page content -->