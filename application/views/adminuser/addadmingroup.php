


		<!-- Main content -->
		<div class="content-wrapper">
			

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">MANAGE USER</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<!-- <a href="#" class="breadcrumb-item">Link</a> -->
							<span class="breadcrumb-item active">GROUP PERMISSION ADD/EDIT</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">
                
				
				<!-- Basic table -->
				<div class="card">
                    <div id="alert"></div>
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Add/Edit GROUP PERMISSION</h5>
						<div class="header-elements">
							<div class="list-icons">
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a> -->
		                		<!-- <a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					

                    <?php echo form_open('admin/addadmingroup','class=form-validate-jquery');?>
                    
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-inline">
                                    <div class="col-lg-4 float-right">
                                    <label for="">Group Name&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                    <div class="col-lg-8">
                                    <input type="text" name="groupname" value="" class="form-control" required>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20">
                                    <div class="col-lg-4 float-right">
                                    <label for="">Group Permission&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
                                    </div>
                                </div>
                                
                                <div class="form-inline mgt-20 mgb-50 ">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Home Page Slide&nbsp;&nbsp;:&nbsp;&nbsp; </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="homeslide" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Feature Product&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="featureproduct" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">News & Event&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="news" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Persmegtive&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="persmegtive" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Review&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="review" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Story&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="story" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">What's in Store&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="whatinstore" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Store Locator&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="storelocator" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Page Content Smeg&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="pagecontent" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Contact Us&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="contactus" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                                <div class="form-inline mgt-20 mgb-50">
                                    <div class="col-lg-6">
                                    <label for="" class="float-right">Admin Website&nbsp;&nbsp;:&nbsp;&nbsp;
                                    </label>
                                    </div>
                                    <div class="col-lg-6">
                                    <label class="form-check-label float-left">
										<input type="checkbox" name="adminweb" class="form-check-input-switchery" checked data-fouc>
									</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-12 text-center">
                        <button type="button" value="reset" class="btn btn-danger" name="reset">cancel</button>
                        <input type="submit" value="save" class="btn btn-success" name="save">
                        </div>

                    </div>

                    <?php echo form_close();?>



				
			</div>
			<!-- /content area -->


		