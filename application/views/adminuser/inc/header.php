<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inter Vision - Smeg Back Office</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>asset/admin/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>asset/admin/css/colors.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/smeg.css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/main/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/js/smeg.js"></script>
    <!-- /core JS files -->
    
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/ui/moment/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/daterangepicker.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/anytime.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/pickers/pickadate/legacy.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/validation/validate.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/inputs/touchspin.min.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/styling/switch.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/uploaders/dropzone.min.js"></script>
    
	<!-- Theme JS files -->
    <script src="<?php echo base_url(); ?>asset/admin/js/app.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/form_validation.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/form_select2.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/datatables_basic.js"></script>
    <script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/picker_date.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/login.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
	<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
	<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script src="<?php echo base_url(); ?>asset/admin/global_assets/js/demo_pages/uploader_bootstrap.js"></script>



    <!-- /theme JS files -->
    

</head>

<body>

<?php
if($this->session->flashdata('message') != ''){
     echo $this->session->flashdata('message');
}else{
     echo $this->session->flashdata('message');
}
?>
<?php
$user = $this->session->userdata('userdatasession');
if (!in_array("homeslide",$user['permission'])){$hidhomeslide = "hidden";}else{$hidhomeslide = "";}
if (!in_array("featureproduct",$user['permission'])){$hidfeatureproduct = "hidden";}else{$hidfeatureproduct = "";}
if (!in_array("news",$user['permission'])){$hidnews = "hidden";}else{$hidnews = "";}
if (!in_array("persmegtive",$user['permission'])){$hidpersmegtive = "hidden";}else{$hidpersmegtive = "";}
if (!in_array("review",$user['permission'])){$hidreview = "hidden";}else{$hidreview = "";}
if (!in_array("story",$user['permission'])){$hidstory = "hidden";}else{$hidstory = "";}
if (!in_array("whatinstore",$user['permission'])){$hidwhatinstore = "hidden";}else{$hidwhatinstore = "";}
if (!in_array("storelocator",$user['permission'])){$hidstorelocator = "hidden";}else{$hidstorelocator = "";}
if (!in_array("pagecontent",$user['permission'])){$hidpagecontent = "hidden";}else{$hidpagecontent = "";}
if (!in_array("contactus",$user['permission'])){$hidcontactus = "hidden";}else{$hidcontactus = "";}
if (!in_array("adminweb",$user['permission'])){$hidadminweb = "hidden";}else{$hidadminweb = "";}
?>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark" style="min-height:50px">
		<div class="navbar-brand">
			<a href="<?php echo base_url(); ?>admin" class="text-white setdownloadsubject">
			INTERVISION
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-paragraph-justify3"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>
			</ul>

			<ul class="navbar-nav ml-auto">
				
				

				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url(); ?>asset/admin/global_assets/images/image.png" class="rounded-circle" alt="">
						<span><?php echo $userdatasession['firstname']." ".$userdatasession['lastname']; ?></span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						
						<a href="<?php echo base_url(); ?>adminlogin/logout" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							<div class="mr-3">
								<a href="#"><img src="<?php echo base_url(); ?>asset/admin/global_assets/images/image.png" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>

							<div class="media-body">
								<div class="media-title font-weight-semibold"><?php echo $userdatasession['firstname']." ".$userdatasession['lastname']; ?></div>
								
							</div>

							
						</div>
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
						<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
						<!-- <li class="nav-item">
							<a href="<?php echo base_url(); ?>admin" class="nav-link">
								<i class="icon-home4"></i>
								<span>Dashboard</span>
							</a>
						</li> -->

						<li class="nav-item nav-item-submenu <?php echo $hidadminweb; ?>">
							<a href="#" class="nav-link"><i class="icon-stack"></i> <span>User</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="Starter kit">
								<li class="nav-item"><a href="<?php echo base_url(); ?>admin/adminlist" class="nav-link">User List</a></li>
								<li class="nav-item"><a href="<?php echo base_url(); ?>admin/admingrouplist" class="nav-link">Group Permission</a></li>
			
	
							</ul>
						</li>

						<li class="nav-item nav-item-submenu <?php echo $hidnews; ?>">
							<a href="#" class="nav-link"><i class="icon-stack"></i> <span>News & Event</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="Starter kit">
								<li class="nav-item"><a href="<?php echo base_url(); ?>adminnews/newslist" class="nav-link">News List</a></li>
								<li class="nav-item"><a href="<?php echo base_url(); ?>adminnews/newscategorylist" class="nav-link">News Category</a></li>
			
	
							</ul>
						</li>

						<li class="nav-item nav-item-submenu <?php echo $hidpersmegtive; ?>">
							<a href="#" class="nav-link"><i class="icon-stack"></i> <span>Persmegtive</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Starter kit">
								<li class="nav-item"><a href="<?php echo base_url(); ?>adminpersmegtive/persmegtivelist" class="nav-link">Persmegtive List</a></li>
							</ul>
						</li>

						<li class="nav-item nav-item-submenu <?php echo $hidfeatureproduct; ?>">
							<a href="#" class="nav-link"><i class="icon-stack"></i> <span>Feature Product</span></a>
							<ul class="nav nav-group-sub" data-submenu-title="Starter kit">
								<li class="nav-item"><a href="<?php echo base_url(); ?>adminfeature/featurelist" class="nav-link">Feature List</a></li>
							</ul>
						</li>

						
								
								<li class="nav-item nav-item-submenu <?php echo $hidhomeslide; ?>">
									<a href="#" class="nav-link"><i class="icon-stack"></i><span>HOMESLIDE</span></a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="<?php echo base_url(); ?>adminhomeslide/homeslidelist" class="nav-link">HOMESLIDE List</a></li>
									</ul>
								</li>
								<li class="nav-item nav-item-submenu <?php echo $hidreview; ?>">
									<a href="#" class="nav-link"><i class="icon-stack"></i><span>Review</span></a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="<?php echo base_url(); ?>adminreview/reviewlist" class="nav-link">Review List</a></li>
									</ul>
								</li>
								<li class="nav-item nav-item-submenu <?php echo $hidstorelocator; ?>">
									<a href="#" class="nav-link"><i class="icon-stack"></i><span>Store Locator</span></a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="<?php echo base_url(); ?>adminstorelocator/storelocatorlist" class="nav-link">Store Locator List</a></li>
									</ul>
								</li>


						<li class="nav-item nav-item-submenu <?php echo $hidwhatinstore; ?>">
							<a href="#" class="nav-link"><i class="icon-stack"></i> <span>Product</span></a>

							<ul class="nav nav-group-sub" data-submenu-title="Starter kit">
								<li class="nav-item"><a href="<?php echo base_url(); ?>adminwhatinstore/productlist" class="nav-link">Product List</a></li>
								<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link">Product Manage</a>
									<ul class="nav nav-group-sub">
										<li class="nav-item"><a href="<?php echo base_url(); ?>adminwhatinstore/featurelist" class="nav-link">Feature Product List</a></li>
										<li class="nav-item"><a href="<?php echo base_url(); ?>adminwhatinstore/dimensionlist" class="nav-link">Dimension List</a></li>
										<li class="nav-item"><a href="<?php echo base_url(); ?>adminwhatinstore/colourlist" class="nav-link">Colour List</a></li>
										<li class="nav-item"><a href="<?php echo base_url(); ?>adminwhatinstore/aestheticlist" class="nav-link">Aesthetic List</a></li>
										<li class="nav-item"><a href="<?php echo base_url(); ?>adminwhatinstore/ribbonlist" class="nav-link">Ribbon List</a></li>
									</ul>
								</li>
								<li class="nav-item"><a href="<?php echo base_url(); ?>adminwhatinstore/whatinstorecategorylist" class="nav-link">Category Product</a></li>
								
	
							</ul>
						</li>
						<li class="nav-item nav-item-submenu">
									<a href="#" class="nav-link"><i class="icon-stack"></i><span>Setting Page</span></a>
									<ul class="nav nav-group-sub">
										<li class="nav-item <?php echo $hidpagecontent; ?>"><a href="<?php echo base_url(); ?>admincontact/editpageabout" class="nav-link">About</a></li>

										<li class="nav-item <?php echo $hidpagecontent; ?>"><a href="<?php echo base_url(); ?>admincontact/editpageexclusive" class="nav-link">Exclusive</a></li>

										<li class="nav-item <?php echo $hidstory; ?>"><a href="<?php echo base_url(); ?>admincontact/editpagestory" class="nav-link <?php echo $hidstory; ?>">Story</a></li>
										<li class="nav-item <?php echo $hidpagecontent; ?>"><a href="<?php echo base_url(); ?>adminwhatinstore/setwhatinstore" class="nav-link">What in store</a></li>
										<li class="nav-item <?php echo $hidpagecontent; ?>"><a href="<?php echo base_url(); ?>adminwhatinstore/setwhatinstoreproduct" class="nav-link <?php echo $hidpagecontent; ?>">What in store Product</a></li>
										<li class="nav-item <?php echo $hidcontactus; ?>"><a href="<?php echo base_url(); ?>admincontact/editcontact" class="nav-link <?php echo $hidcontactus; ?>">Contact</a></li>

									</ul>
								</li>

						
						<!-- /main -->

					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->
			
		</div>
		<!-- /main sidebar -->

<style type="text/css">
	body,html{
    font-family:gotham_light !important;
    height: 900px;
}
</style>