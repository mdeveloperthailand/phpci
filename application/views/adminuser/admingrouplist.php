

		<!-- Main content -->
		<div class="content-wrapper">
			

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">MANAGE USER</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<!-- <a href="#" class="breadcrumb-item">Link</a> -->
							<span class="breadcrumb-item active">Group Permission</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">
                <div class="card">  
                    <?php echo form_open('search/searchadmingrouplist');?>

                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-3">
                        <label for="">Name:</label>
                        <input type="text" name="name" class="form-control">
                        </div>
                        <!-- <div class="col-lg-2">
                        <label for="">Status:</label>
                        <select name="status" class="form-control">
                            <option value="0">Inactive</option>
                            <option value="1">Active</option>
                        </select>
                        </div> -->
                        <div class="col-lg-3">
                        <div class="form-group">
									<label>Date Update:</label>
									<div class="input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" class="form-control daterange-single" name="start" value="01/01/2018"> 
									</div>
								</div>
                        </div>
                        <div class="col-lg-1 text-center">
                        <h3 class="mgt-20"> - </h3>
                        </div>
                         <div class="col-lg-3">
                               <div class="form-group">
									<label>&nbsp;</label>
									<div class="input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" class="form-control daterange-single" name="end"  value=""> 
									</div>
							    </div>
                         </div>
                    </div>
                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-12 text-center">
                        <input type="submit" value="search" class="btn btn-primary" name="search">
                        <button type="button" value="reset" class="btn btn-danger" name="reset">Reset Filter</button>
                        </div>

                    </div>
                    <?php echo form_close();?>
                </div>
				
				<!-- Basic table -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Group Permission</h5>
						<div class="header-elements">
							<div class="list-icons">
                                <a href="<?php echo base_url(); ?>admin/addadmingroup" class="btn btn-primary text-white">Add</a>
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a> -->
		                		<!-- <a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					 <?php echo form_open('admin/updatestatus');?>

				

					

					<div class="table-responsive">
						<table class="table datatable-basic">
							<thead>
								<tr>
									
									<th>Group</th>
									<th>Last Update</th>
									<th>Manage</th>
								</tr>
							</thead>
							<tbody>
                                <?php foreach ($admingrouplistdata as $key => $value) {

								
                                   echo "<tr>
                                   <td>".$value['name']."</td>
                                   <td>".$value['update_date']."</td>
                                   <td> <a href='".base_url()."admin/editadmingroup/".$value['user_group_id']."' class='btn btn-success'>Edit</a> <button class='btn btn-danger deleterow' id='".$value['user_group_id']."' >Delete</button> </td>
                               </tr>";
                                } ?>
							</tbody>
							
						</table>
					</div>
				</div>
				<!-- /basic table -->

 				<?php echo form_close();?>
				
			</div>
            <!-- /content area -->
            <!-- Modal -->
<div class="modal fade" id="deletemodal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Do you Confirm Delete ?</p>
        </div>
        <div class="modal-footer" id="insertbuttondel">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

            <script>
            $(document).ready(function(){
                $('#custom_checkbox_stacked_unchecked_all').click(function(){

                  $('input:checkbox.boxstatus').not(this).prop('checked', this.checked);

                });
				
				$('.deleterow').click(function(){
					event.preventDefault();   
					$('#delcf').remove();
					$('#insertbuttondel').append("<a href='deleteadmingroup/"+this.id+"' id='delcf' class='btn btn-danger'>Delete</a>");
					$('#deletemodal').modal('show');
	
				});

				// $('.delall').click(function(){
				// 	event.preventDefault();   
				// 	$('#delcf').remove();
				// 	var ids = $(this).parents('form#delallsubmit').find('input[type=checkbox].boxstatus:checked').map(function() {
				// 		return {
				// 			id: $(this).val()
				// 		}
				// 	}).get();
				// 	console.log(ids);
					
				// 	$('#insertbuttondel').append("<form id='delcf' action='<?php echo base_url(); ?>admin/deleteadminbyselect' method='post'><button type='submit' class='btn btn-danger'>Delete</button></form>");
				// 	$(ids).each(function(index,value) { 
				// 		$('#delcf').append("<input name='arraydel"+value.id+"' type='hidden' value='"+value.id+"'>");
				// 	});
				// 	$('#deletemodal').modal('show');
					
					
				// });

				
				
            });
            </script>


		