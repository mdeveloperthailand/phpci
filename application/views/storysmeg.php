<!-- <?php print_r($storydata);?> -->
<section id="bgwhatinstore" style="background-image: url('<?php echo base_url(); ?>image/<?php echo $storydata['setpage_image']; ?>');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <p class="text-white text-center setstorytexttop">SMEG STORY</p>
                <h1 class="text-white text-center setstorytexttop2"><?php echo $storydata['setpage_h1']; ?></h1>
            </div>
        </div>
    </div>
</section>

<section>
<img src="<?php echo base_url(); ?>image/scrolldown.png" class="img-fluid btnscrolldown whatinstorebg"  alt="">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10 offset-lg-1" id="sectwo">
            <h5 class="text-center setwhatinstoretextsec2main">
            <?php echo $storydata['setpage_h2']; ?>
            </h5>
            <p class="text-center"><?php echo $storydata['setpage_short']; ?></p>
            <p class="text-center mgt-50"><?php echo $storydata['setpage_detail']; ?></p>
        </div>
    </div>
</div>

</section>

<script>
$(document).ready(function () {
  $('.btnscrolldown').click(function() {
  $('html, body').animate({
    scrollTop: $("div#sectwo").offset().top
  }, 1000)
    });
});
</script>