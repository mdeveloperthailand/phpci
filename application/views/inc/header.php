<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="Smeg">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>SMEG</title>

			<!-- CSS ============================================= -->
			<link rel="stylesheet" href="<?php echo base_url(); ?>asset/bootstrap/css/bootstrap.css">
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/smeg.css">
			<script src="<?php echo base_url(); ?>asset/js/jquery.js"></script>
			  <script src="<?php echo base_url(); ?>asset/js/popper.min.js"></script>
			  <script src="<?php echo base_url(); ?>asset/instashow/elfsight-instagram-feed.js"></script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
			<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
			<link href="<?php echo base_url(); ?>asset/lib/zoomer/jquery.spzoom.css" rel="stylesheet" type="text/css">
		</head>
		<body>	

<section>
<div class="container-fluid header navbar-fixed-top text-center">

	<div class="row">

		<div class="col-lg-12 sethighmenu">
				<span class="float-left" id="opennav">
				<div id="nav-icon1">
					<span id='hamone'></span>
					<span id="cen"></span>
					<span id='hamthree'></span>
				</div>
				</span>
		
				<a href="<?php echo base_url(); ?>home" class="setpointer" id='logo'>
				<?php
				 if (isset($logo)) {
				 	echo $logo; 
				 }else{
				 	echo $logow;
				 }

				 ?></a>

	
					<a id="langna" class="float-right mgt-10 setpointer langer" href="<?php echo base_url(); ?>home/changelang">
					<?php
					if ($this->session->userdata('lang')==1) {
						echo "<i class='fas fa-globe-asia sethalfauto mgr-10'></i>&nbsp;&nbsp;TH&nbsp;&nbsp;</i>";
					}else{
						echo "<i class='fas fa-globe-asia sethalfauto mgr-10'></i>&nbsp;&nbsp;EN&nbsp;&nbsp;</i>";
					}
					?>
					</a>

				<!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					<a class="dropdown-item" href="#">TH</a>
				</div> -->
		</div>



	</div>
		
		<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" id="closenav">&times;</a>
		<br>
		<br>
		<br>
		<br>
		<a href="<?php echo base_url(); ?>home" class="forcenowrap text-left mgl-80"><span class="setbordermenu">Ho</span>me</a>
		<a href="<?php echo base_url(); ?>whatinstore" class="forcenowrap text-left mgl-80"><span class="setbordermenu">Wh</span>at's in Store</a>
		<a href="<?php echo base_url(); ?>news" class="forcenowrap text-left mgl-80"><span class="setbordermenu">Ne</span>ws & Event</a>
		<a href="<?php echo base_url(); ?>storelocator" class="forcenowrap text-left mgl-80"><span class="setbordermenu">St</span>ore Locator</a>
		<a href="<?php echo base_url(); ?>home/exclusive" class="forcenowrap text-left mgl-80"><span class="setbordermenu">Sm</span>eg Exclucive</a>
		<a href="<?php echo base_url(); ?>home/about" class="forcenowrap text-left mgl-80"><span class="setbordermenu">Ab</span>out Smeg</a>
		</div>

		

</div>
</section>


<script>
	$(document).ready(function(){

		$('#opennav').click(function(){
			$('#mySidenav').removeClass("setwidthclose");
			$('#mySidenav').addClass("setwidthopen");
		});

		$('#closenav').click(function(){
			$('#mySidenav').removeClass("setwidthopen");
			$('#mySidenav').addClass("setwidthclose");
		});

	});
</script>








