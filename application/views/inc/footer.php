<footer class="mgt-50">
    <div class="container-fluid">
        <div class="row mgt-50 mgb-50">
            <div class="col-lg-2 text-center setborderhomefooter">
            <a href="#" class="setpointer">
            <img src="<?php echo base_url(); ?>image/logo.png" class="setlogofootersize text-center" alt="">
            </a>
            </div>
            <div class="col-lg-2 centerres">
                <p class="settextthaifootersubject">ติดต่อ สอบถาม</p>
                <p class="settelnumber"><a href="tel:<?php echo $contactdata['contact_tel']; ?>" class="setpointer boldtel"> <img src="<?php echo base_url(); ?>image/footertel.png" class="setfootertelpic" alt=""> <?php echo $contactdata['contact_tel']; ?><a></p>
                <p class="settextthaifootersubject">ผู้แทนจำหน่าย</p>
                <p class="settextthaifootergray"><?php echo $contactdata['contact_name']; ?><br/>
                <?php echo $contactdata['contact_address']; ?></p>
            </div>
            <div class="col-lg-4">
                <p class="text-center">
                <img src="<?php echo base_url(); ?>image/footerproud.png" class="setfooterproud" alt="">
                </p>
                <p class="text-center"> 
                <a href="<?php echo $contactdata['contact_facebook']; ?>" target="_blank" class="setpointer mglr-10">
                <img src="<?php echo base_url(); ?>image/facebook.png">
                </a>
                <a href="http://instagram.com/_u/<?php echo $contactdata['contact_ig']; ?>/" target="_blank" class="setpointer mglr-10">
                <img src="<?php echo base_url(); ?>image/instagram.png">
                </a>
                <a href="http://line.me/ti/p/%40<?php echo substr($contactdata['contact_line'],1); ?>" target="_blank" class="setpointer mglr-10">
                <img src="<?php echo base_url(); ?>image/line.png">
                </a>
                </p>
                <p class="text-center setcopyright">Copyright &copy; 2018 SMEGTHAI All right reserved.</p>
            </div>
            <div class="col-lg-4 setcenter-responsive">
                <p class="setfooterheadstay">Stay Connected</p>
                <label class="field a-field a-field_a2 page__field">
                <input class="field__input" placeholder="e.g. Smeg@smeg.co.th" type="email" value="" name="stayemail"  required>
                <span class="field__label-wrap">
                    <span class="field__label"><i class="far fa-envelope mgl-down15"></i> <span class="textstay">Your Email</span></span>
                </span>
                </label>    
                <a class="setpointer setstayhome"><img src="<?php echo base_url(); ?>image/arrowr.png"></a>
                <div id="check"></div>
            </div>
        </div>
    </div>
    <input type="hidden" name="urisegone" value="<?php echo $this->uri->segment(1); ?>">
</footer>


   <script src="<?php echo base_url(); ?>asset/bootstrap/js/bootstrap.js"></script>
   
   <script>
   
   $(function () {
    var urisegone = $('[name=urisegone]').val();
        if (urisegone=="whatinstore") {
             $('#hamone').addClass('hamone');
             $('#cen').addClass('cen');
             $('#hamthree').addClass('hamthree');
          }
     $(document).scroll(function () {
        var ii = 0;
       var $nav = $(".navbar-fixed-top");
       $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      
       var urisegone = $('[name=urisegone]').val();
       if ($(this).scrollTop() > $nav.height()) {
        $('#langna').removeClass('langer');
        $('path,circle').removeClass('st0');
        $('path,circle').addClass('st1');
        if (urisegone=="whatinstore") {
            $('#hamone').removeClass('hamone');
            $('#cen').removeClass('cen');
            $('#hamthree').removeClass('hamthree');
       }
       }else{
         $('#langna').addClass('langer');
         $('path,circle').removeClass('st1');
         $('path,circle').addClass('st0');
         if (urisegone=="whatinstore") {
             $('#hamone').addClass('hamone');
             $('#cen').addClass('cen');
             $('#hamthree').addClass('hamthree');
          }
       }
    

       
     });
   });

   $(document).ready(function(){
	$('#nav-icon1').click(function(){
		$(this).toggleClass('open');
	});
    });

    $('input[name=stayemail]').change(function() 
    {
        $('#check').empty();
        var rs = emailValidate($(this).val());

        if (rs) {
                $.ajax({
            method: "POST",
            url: "<?php echo base_url();?>home/collectdata",
            data: { email: $(this).val()}
            })
            .done(function( val ) {   
                console.log(val);
                if (val==200) {
                    $('input[name=stayemail]').val('');
                    $('#check').html("<p id='valid'>SUCCESSFULLY</p>");
                }else if (val==400) {
                    $('#check').html("<p id='invalid'>ALREADY REGISTER</p>");
                }else {
                    $('#check').html("<p id='invalid'>ERROR</p>");
                }             
            });
        } else {
            $('#check').html("<p id='invalid'>INVALID</p>");
        }
        
        

    });

    function emailValidate(email){
            var check = "" + email;
            if((check.search('@')>=0)&&(check.search(/\./)>=0))
                if(check.search('@')<check.split('@')[1].search(/\./)+check.search('@')) return true;
                else return false;
            else return false;
        }
       
//    function openNav() {
//        document.getElementById("mySidenav").style.width = "450px";
//    }
//    function closeNav() {
//        document.getElementById("mySidenav").style.width = "0";
//    }
   </script>

  
        
   
   </body>
   </html>
   