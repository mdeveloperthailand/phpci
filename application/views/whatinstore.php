<section id="bgwhatinstore" style="background-image: url('<?php echo base_url(); ?>image/whatprobanner.png');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="text-white text-center setwhatinstoretextmain"><?php echo $layoutwhatinstoredata['layout_h1']; ?></h1>
            </div>
        </div>
    </div>
</section>

<section>
<img src="<?php echo base_url(); ?>image/scrolldown.png" class="img-fluid btnscrolldown whatinstorebg"  alt="">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10 offset-lg-1">
            <h5 class="text-center setwhatinstoretextsec2main setwhatinstorebordersec1">
            <?php echo $layoutwhatinstoredata['layout_h2']; ?>
            </h5>
            <p class="text-center mgt-20"><?php echo $layoutwhatinstoredata['layout_texteditor']; ?></p>
        </div>
    </div>
</div>

</section>

<section id="sectwo">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        <div class="row mgt-50 mglr-50">
            
            <?php
                foreach ($categoryproductdata as $key => $value) {
                    echo "<div class='col-lg-4 hoverwhatinstore'>
                    <a href='".base_url()."whatinstore/whatincategory/".$value['category_id']."' class='setpointer'>
                    <figure>
                            <img src='".base_url()."image/category/".$value['category_image']."' class='img-fluid'>
                    </figure>
                    </a>
                </div>";
                }
            ?>

           

            </div>
        </div>
    </div>
</div>
</section>

<script>
$(document).ready(function () {
  $('.btnscrolldown').click(function() {
  $('html, body').animate({
    scrollTop: $("section#sectwo").offset().top
  }, 1000)
    });
});
</script>

<style type="text/css">
    .setpointer.langer{
        color: #fff;
    }
</style>


