<!DOCTYPE html>
<html>
<head>
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
  <script>tinymce.init({
    selector: '#tinyone',
    plugins: 'image code',
    toolbar: 'undo redo | image code',
    images_upload_handler: function (blobInfo, success, failure) {
    var xhr, formData;
    xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open('POST', 'http://smeg.co.th/dev/home/testtiny');
    xhr.onload = function() {
      var json;

      if (xhr.status != 200) {
        failure('HTTP Error: ' + xhr.status);
        return;
      }
      json = JSON.parse(xhr.responseText);
      if (!json || typeof json.location != 'string') {
        failure('Invalid JSON: ' + xhr.responseText);
        return;
      }
      success(json.location);
        };
      
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
        
        
        
        xhr.send(formData);
    },
});
</script>

</head>
<body>
 <form>
  <textarea id="tinyone">Next, use our Get Started docs to setup Tiny!</textarea>
</form>
</body>

</html>