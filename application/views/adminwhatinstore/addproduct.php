


<!-- Main content -->
<div class="content-wrapper">


<!-- Page header -->
<div class="page-header page-header-light">
<div class="page-header-content header-elements-md-inline">
<div class="page-title d-flex">
<h4> <span class="font-weight-semibold">MANAGE ADD PRODUCT</span></h4>
<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>


</div>

<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
<div class="d-flex">
<div class="breadcrumb">
<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
<!-- <a href="#" class="breadcrumb-item">Link</a> -->
<span class="breadcrumb-item active">ADD PRODUCT ADD/EDIT</span>
</div>

<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>


</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">


<!-- Basic table -->
<div class="card">

<div id="alert"></div>
<div class="card-header header-elements-inline">
<h5 class="card-title">Add/Edit ADD PRODUCT</h5>
<div class="header-elements">
<div class="list-icons">
<a class="list-icons-item" data-action="collapse"></a>
<!-- <a class="list-icons-item" data-action="reload"></a> -->
<!-- <a class="list-icons-item" data-action="remove"></a> -->
</div>
</div>
</div>



<form action="<?php echo base_url(); ?>adminwhatinstore/addproduct" method='post'  enctype="multipart/form-data">
<div class="container">

<div class="row mgt-20 mglr-10 mgb-50">
<div class="col-lg-12 text-center">
<button type="button" value="reset" class="btn btn-danger" name="reset">cancel</button>
<input type="submit" value="save" class="btn btn-success" name="save">
</div>
</div>

<div class="row">
<div class="col-lg-8 offset-lg-2">
<ul class="nav nav-tabs nav-tabs-bottom border-bottom-0 nav-justified">
<li class="nav-item"><a href="#bottom-justified-divided-tab1" class="nav-link active" data-toggle="tab">PRODUCT DETAIL</a></li>
<li class="nav-item"><a href="#bottom-justified-divided-tab2" class="nav-link" data-toggle="tab">CATEGORY/OPTION</a></li>
<li class="nav-item"><a href="#bottom-justified-divided-tab3" class="nav-link" data-toggle="tab">UPLOAD/IMAGE</a></li>
</ul>
</div>
</div>
<div class="col-lg-8 offset-lg-2">
<div class="tab-content">
<div class="tab-pane fade show active" id="bottom-justified-divided-tab1">

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">ID PRODUCT&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="product_model" value="" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">NAME PRODUCT ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="product_name_th" value="" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">NAME PRODUCT ENG&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="product_name_en" value="" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">SHORT DESCRIPTION ไทย&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="product_short_description_th" value="" class="form-control" required>
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">SHORT DESCRIPTION ENG&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="text" name="product_short_description_en" value="" class="form-control" required>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">Detail Thai&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<textarea id="summernote" name="product_description_th"></textarea>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">Detail Eng&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<textarea id="summernote1" name="product_description_en"></textarea>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">Detail Thai SECTION 2&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<textarea id="summernote2" name="product_description_th2"></textarea>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">Detail Eng SECTION 2&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<textarea id="summernote3" name="product_description_en2"></textarea>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">META TITLE&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="product_meta_title" value="" class="form-control">
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">META DESCRIPTION&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="product_meta_description" value="" class="form-control">
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">META KEYWORD&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<input type="text" name="product_meta_keyword" value="" class="form-control">
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<label for="">PRICE&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<input type="number" min="0" max="1000000" step="1" name="product_price"  class="form-control" required/>
</div>
</div>


<div class="form-inline mgt-20 mgb-50">
<div class="col-lg-12">
<label for="">Status&nbsp;&nbsp;:&nbsp;&nbsp;
<label class="form-check-label">
<input type="checkbox" name="product_status" class="form-check-input-switchery" checked data-fouc>
</label>
</label>
</div>
</div>

</div><!-- /tab-1 -->

<div class="tab-pane fade" id="bottom-justified-divided-tab2">

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">MAIN Category&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<select name="categorymain" data-placeholder="Select a Group..." class="form-control select" data-fouc required>
<option></option>
<?php foreach($categorymainlistdata as $value){ ?>
<option value="<?php echo $value['category_id']; ?>"><?php echo $value['category_description_name']; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">SUB Category&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<select name="categorysub" data-placeholder="Select a Group..." class="form-control select" data-fouc required id="categorysub">
<option></option>
</select>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">AESTHETIC LINE&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<select name="aesthetic_id" data-placeholder="Select a Group..." class="form-control select" data-fouc required>
<option></option>
<?php foreach($aestheticlistdata as $value){ ?>
<option value="<?php echo $value['aesthetic_id']; ?>"><?php echo $value['aesthetic_name_en']; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">FEATURE&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<select name="feature_id" data-placeholder="Select a Group..." class="form-control select" data-fouc required>
<option></option>
<?php foreach($featurelistdata as $value){ ?>
<option value="<?php echo $value['feature_id']; ?>"><?php echo $value['feature_name_en']; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">DIMENSION CM&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<select name="dimension_id" data-placeholder="Select a Group..." class="form-control select" data-fouc required>
<option></option>
<?php foreach($dimensionlistdata as $value){ ?>
<option value="<?php echo $value['dimension_id']; ?>"><?php echo $value['dimension_name']; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="form-group mgt-20">
<div class="col-lg-12">
<label for="">COLOUR&nbsp;&nbsp;:&nbsp;&nbsp;<span class="text-danger">*&nbsp;&nbsp;&nbsp;</span></label>
</div>
<div class="col-lg-12">
<select name="colour_id" data-placeholder="Select a Group..." class="form-control select" data-fouc required>
<option></option>
<?php foreach($colourlistdata as $value){ ?>
<option value="<?php echo $value['colour_id']; ?>"><?php echo $value['colour_name_en']; ?></option>
<?php } ?>
</select>
</div>
</div>

<div class="form-group mgt-20 mgb-50">
<div class="col-lg-12">
<label for="">RIBBON&nbsp;&nbsp;:&nbsp;&nbsp;</label>
</div>
<div class="col-lg-12">
<select name="ribbon_id" data-placeholder="Select a Group..." class="form-control select" data-fouc>
<option></option>
<?php foreach($ribbonlistdata as $value){ ?>
<option value="<?php echo $value['ribbon_id']; ?>"><?php echo $value['ribbon_name']; ?></option>
<?php } ?>
</select>
</div>
</div>

</div><!-- /tab-2 -->

<div class="tab-pane fade" id="bottom-justified-divided-tab3">

<label class="col-lg-2 col-form-label font-weight-semibold">PRODUCT MAIN IMAGE:</label>
<div class="col-lg-12 text-center">
<input type="file" name="imageproductmain" class="file-input form-control-lg" accept="image/*" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
</div>

<label class="col-lg-2 col-form-label font-weight-semibold">PRODUCT IMAGE OTHER:</label>
<div class="col-lg-12 text-center">
<input type="file" name="imageproduct[]" class="file-input form-control-lg" accept="image/*" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" multiple="multiple" data-fouc>
<p>*Multiple Image</p>
</div>

<label class="col-lg-2 col-form-label font-weight-semibold">SYMBOLIC IMAGE:</label>
<div class="col-lg-12 text-center">
<input type="file" name="imagesymbolic" class="file-input form-control-lg" accept="image/*" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
</div>

<label class="col-lg-2 col-form-label font-weight-semibold">SIMILAR IMAGE:</label>
<div class="col-lg-12 text-center">
<input type="file" name="imagesimilar" class="file-input form-control-lg" data-show-caption="false" accept="image/*" data-show-upload="false" data-browse-class="btn btn-primary btn-lg" data-remove-class="btn btn-light btn-lg" data-fouc>
</div>

<div class="form-group row mgt-50">
<label class="col-lg-4 col-form-label font-weight-semibold">TECHNICAL DRAWING:</label>
<div class="col-lg-8">
<input type="file" class="file-input" name="technicaldrawing" data-show-preview="false" data-show-upload="false" data-fouc>
</div>
</div>

<div class="form-group row mgt-50">
<label class="col-lg-4 col-form-label font-weight-semibold">INSTRUCTION MANUAL:</label>
<div class="col-lg-8">
<input type="file" class="file-input" name="instructionmanual" data-show-preview="false" data-show-upload="false" data-fouc>
</div>
</div>

<div class="form-group row mgt-50">
<label class="col-lg-4 col-form-label font-weight-semibold">PDF BULLET-IN:</label>
<div class="col-lg-8">
<input type="file" class="file-input" name="pdfbulletin" data-show-preview="false" data-show-upload="false" data-fouc>
</div>
</div>

<div class="form-group row mgt-50">
<label class="col-lg-4 col-form-label font-weight-semibold">3D FILTER:</label>
<div class="col-lg-8">
<input type="file" class="file-input" name="3dfilter" data-show-preview="false" data-show-upload="false" data-fouc>
</div>
</div>

</div><!-- /tab-3 -->

</div> <!-- /tab-content -->
</div> <!-- /col-8 -->


</form>




</div>
</div>
<!-- /content area -->

<script>
$(document).ready(function() {
$('#summernote').summernote({
lang: 'ko-KR' // default: 'en-US'
});
$('#summernote1').summernote({
lang: 'ko-KR' // default: 'en-US'
});
$('#summernote2').summernote({
lang: 'ko-KR' // default: 'en-US'
});
$('#summernote3').summernote({
lang: 'ko-KR' // default: 'en-US'
});


$('[name=categorymain]').change(function() {
$.ajax({
method: "POST",
url: "<?php echo base_url();?>adminwhatinstore/getsubcategory",
data: { category_id: 66}
})
.done(function( val ) {
$.map(JSON.parse(val), function(i) {
$("#categorysub").append("<option value='"+i.category_id+"'>"+i.category_description_name+"</option>");
});
});
});




});
</script>


