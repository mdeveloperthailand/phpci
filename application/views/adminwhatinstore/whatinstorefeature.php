<!-- <?php print_r($featurelistdata);?> -->

		<!-- Main content -->
		<div class="content-wrapper">
			

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4> <span class="font-weight-semibold">MANAGE FEATURE PRODUCT</span></h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<a href="<?php echo base_url(); ?>admin" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
							<!-- <a href="#" class="breadcrumb-item">Link</a> -->
							<span class="breadcrumb-item active">FEATURE PRODUCT List</span>
						</div>

						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

					
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">
                <div class="card">  
                    <?php echo form_open('search/searchfeaturelist');?>

                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-3">
                        <label for="">Name:</label>
                        <input type="text" name="feature_name_en" class="form-control">
                        </div>
                       
                        <div class="col-lg-2">
                        <label for="">Status:</label>
                        <select name="feature_status" class="form-control">
                            <option value="0">Inactive</option>
                            <option value="1" selected>Active</option>
                        </select>
                        </div>
                        <div class="col-lg-3">
                        <div class="form-group">
									<label>Date Update:</label>
									<div class="input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="start" class="form-control daterange-single" value="01/01/2018" readonly> 
									</div>
								</div>
                        </div>
                        <div class="col-lg-1 text-center">
                        <h3 class="mgt-20"> - </h3>
                        </div>
                         <div class="col-lg-3">
                               <div class="form-group">
									<label>&nbsp;</label>
									<div class="input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" name="end" class="form-control daterange-single" value="" readonly> 
									</div>
							    </div>
                         </div>
                    </div>
                    <div class="row mgt-20 mglr-10 mgb-50">
                        <div class="col-lg-12 text-center">
                        <input type="submit" value="search" class="btn btn-primary" name="search">
                        <button type="button" value="reset" class="btn btn-danger" name="reset">Reset Filter</button>
                        </div>

                    </div>
                    <?php echo form_close();?>
                </div>
				
				<!-- Basic table -->
				<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">FEATURE PRODUCT LIST</h5>
						<div class="header-elements">
							<div class="list-icons">
                                <a href="<?php echo base_url(); ?>adminwhatinstore/addfeature" class="btn btn-primary text-white">Add feature</a>
		                		<a class="list-icons-item" data-action="collapse"></a>
		                		<!-- <a class="list-icons-item" data-action="reload"></a> -->
		                		<!-- <a class="list-icons-item" data-action="remove"></a> -->
		                	</div>
	                	</div>
					</div>

					 <?php echo form_open('adminwhatinstore/updatestatusfeature','id=delallsubmit');?>
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-inline">
								<label for="">Status : &nbsp;&nbsp;&nbsp;</label>
								<select name="status" class="form-control">
									<option value="0">Inactive</option>
									<option value="1">Active</option>
								</select>&nbsp;&nbsp;&nbsp;
								<input type="submit" value="Update Status" class="btn btn-primary" name="submit">
                                &nbsp;&nbsp;&nbsp;
								<button type="button" value="delete" class="btn btn-danger delall" name="delete">Delete Selected</button>
								</div>
							</div>
						</div>
					</div>

					

					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>
                                        <div class='custom-control custom-checkbox mgt-ng-20'>
										<input type='checkbox' name='checkboxstatusall[]' class='custom-control-input' id='custom_checkbox_stacked_unchecked_all'>
										<label class='custom-control-label' for='custom_checkbox_stacked_unchecked_all'></label>
                                    </div>
                                </th>
									<th>Name</th>
									<th>Last Update</th>
									<th>Status</th>
									<th>Manage</th>
								</tr>
							</thead>
							<tbody>
                                
                                <?php 
                                  
                                
                                foreach ($featurelistdata as $key => $value) {

									if ($value['feature_status']==0) {
										$status = "<span class='badge badge-secondary'>Inactive</span>";
									} else {
										$status = "<span class='badge badge-success'>Active</span>";
									}
									
                                   echo "<tr>
                                   <td>
                                   <div class='custom-control custom-checkbox mgt-ng-20'>
										<input type='checkbox' keep='".$value['feature_id']."' name='checkboxstatus[]' value='".$value['feature_id']."' class='custom-control-input boxstatus' id='custom_checkbox_stacked_unchecked".$value['feature_id']."'>
										<label class='custom-control-label' for='custom_checkbox_stacked_unchecked".$value['feature_id']."'></label>
									</div>
                                    </td>
                                   <td>".$value['feature_name_en']."</td>
                                   <td>".$value['update_date']."</td>
                                   <td>".$status."</td>
                                   <td> <a href='".base_url()."adminwhatinstore/editfeature/".$value['feature_id']."' class='btn btn-success'>Edit</a> <button class='btn btn-danger deleterow' id='".$value['feature_id']."' >Delete</button> </td>
                               </tr>";

                                    if (isset($value['sub'])) {
                                        foreach ($value['sub'] as $key1 => $value1) {
                                            if ($value1['category_status']==0) {
                                                $status1 = "<span class='badge badge-secondary'>Inactive</span>";
                                            } else {
                                                $status1 = "<span class='badge badge-success'>Active</span>";
                                            }
            
                                            echo "<tr>
                                               <td>
                                               <div class='custom-control custom-checkbox mgt-ng-20'>
                                                    <input type='checkbox' keep='".$value1['feature_id']."' name='checkboxstatus[]' value='".$value1['feature_id']."' class='custom-control-input boxstatus' id='custom_checkbox_stacked_unchecked".$value1['feature_id']."'>
                                                    <label class='custom-control-label' for='custom_checkbox_stacked_unchecked".$value1['feature_id']."'></label>
                                                </div>
                                                </td>
                                               <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$value1['category_description_name']."</td>
                                               <td>".$value1['update_date']."</td>
                                               <td>".$status1."</td>
                                               <td> <a href='".base_url()."adminwhatinstore/editwhatinstorecategory/".$value1['feature_id']."' class='btn btn-success'>Edit</a> <button class='btn btn-danger deleterow' id='".$value['feature_id']."' >Delete</button> </td>
                                           </tr>";
                                           }
                                    }
                               
                                } 
                            ?>
							</tbody>
							
						</table>
					</div>
				</div>
				<!-- /basic table -->

 				<?php echo form_close();?>
                 <p><?php echo $links; ?></p>

			</div>
            <!-- /content area -->
            
<!-- Modal -->
<div class="modal fade" id="deletemodal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Do you Confirm Delete ?</p>
        </div>
        <div class="modal-footer" id="insertbuttondel">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <!-- Modal -->
<div class="modal fade" id="addsubmodal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Do you Confirm Delete ?</p>
        </div>
        <div class="modal-footer" id="insertbuttondel">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


            <script>
            $(document).ready(function(){
                $('#custom_checkbox_stacked_unchecked_all').click(function(){

                  $('input:checkbox.boxstatus').not(this).prop('checked', this.checked);

                });
				
				$('.deleterow').click(function(){
					event.preventDefault();   
					$('#delcf').remove();
					$('#insertbuttondel').append("<a href='deletefeature/"+this.id+"' id='delcf' class='btn btn-danger'>Delete</a>");
					$('#deletemodal').modal('show');
	
				});
                

				$('.delall').click(function(){
					event.preventDefault();   
					$('#delcf').remove();
					var ids = $(this).parents('form#delallsubmit').find('input[type=checkbox].boxstatus:checked').map(function() {
						return {
							id: $(this).val()
						}
					}).get();
					console.log(ids);
					
					$('#insertbuttondel').append("<form id='delcf' action='<?php echo base_url(); ?>adminwhatinstore/deletefeaturebyselect' method='post'><button type='submit' class='btn btn-danger'>Delete</button></form>");
					$(ids).each(function(index,value) { 
						$('#delcf').append("<input name='arraydel"+value.id+"' type='hidden' value='"+value.id+"'>");
					});
					$('#deletemodal').modal('show');
					
					
				});


                
				
				
            });
            </script>


		