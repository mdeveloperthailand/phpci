<section class="settopnewsevent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="detailpro mg-0"><a href="" onclick="window.history.go(-1); return false;" class="setpointer"><i class="fas fa-long-arrow-alt-left"></i> BACK</a><span class="float-right setbreadcrumb detailpro "> <a href="<?php echo base_url();?>" class="setpointer">Home</a> > <a href="<?php echo base_url();?>whatinstore" class="setpointer">What In Store</a> > <span class="setlastbreadcrumb">All</span></span>
</p>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row mgt-80 mglr-80 whatincategory">
            <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-1 col-12 setpadinherit">

                    <div class="row">
                    <div class='col-lg-12 col-4'><img src="<?php echo base_url(); ?>image/product/<?php echo $productbyiddata['product_image'];?>" class="img-fluid setpadthumbdetail setpointer thumbnailpic setborderactivewhatincategory" id="<?php echo $productbyiddata['product_image'];?>" alt=""></div>
                    <?php
                        foreach ($productimagebyiddata as $key => $value) {
                            // if ($key==0) {
                            //    $setborder = "setborderactivewhatincategory";
                            // }else{
                            //     $setborder = "";
                            // }
                          echo "<div class='col-lg-12 col-4'>
                                <img id='".$value['image']."' src='".base_url()."image/product/".$value['image']."' class='img-fluid setpadthumbdetail setpointer thumbnailpic setborderactivewhatincategory' alt=''>
                                </div>";
                        }
                    ?>
                    
                    </div>
                    
                </div>

                <div class="col-lg-5 text-center mgt-40res" id="mainpic">
                   
                    <a href="<?php echo base_url(); ?>image/product/<?php echo $productbyiddata['product_image'];?>" title="Smeg" data-spzoom>
                        <img src="<?php echo base_url(); ?>image/product/<?php echo $productbyiddata['product_image'];?>" class="spzoom-image" alt="thumb"/>
                    </a>
                </div>

                 <div class="col-lg-3 mgt-40res">
                    <p class="setnewstextcatemain"><?php echo $productbyiddata['category_description_name'];?></p>
                    <h1 class="setprodetailtextid"><?php echo $productbyiddata['product_model'];?></h1>
                    <p class="setprodetailtextcaption"><?php echo $productbyiddata['product_description_name'];?></p>
                    <p class="setprodetailtextprice">&#3647; <?php echo number_format($productbyiddata['product_price']);?></p>
                    <p class="setprodetailtextheadline"><?php echo $productbyiddata['product_description_short_detail'];?></p>
                    <img src="<?php echo base_url(); ?>image/prodetail2.png" alt="">
                </div>

            </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row mgt-50 mglr-80">
            <div class="col-lg-6" id="descriptiondata">
            <?php echo $productbyiddata['product_description_detail'];?>
            </div>
            <div class="col-lg-6 setborderdownload">
                <p class="setdownloadsubject mgt-20">Download Area</p>
                <p class="setdownloadtextfile mgt-20"><a class="setpointer" href="<?php echo base_url(); ?>image/technicaldrawing/<?php echo $productbyiddata['product_technicaldrawing'];?>" download>Technical drawing</a></p>
                <p class="setdownloadtextfile"><a class="setpointer" href="<?php echo base_url(); ?>image/instructionmanual/<?php echo $productbyiddata['product_instructionmanual'];?>" download>Instruction manaul</a></p>
                <p class="setdownloadtextfile"><a class="setpointer" href="<?php echo base_url(); ?>image/pdfbulletin/<?php echo $productbyiddata['product_pdfbulletin'];?>" download>Pdf Bullet in</a></p>
                <p class="setdownloadtextfile"><a class="setpointer" href="<?php echo base_url(); ?>image/3dfilter/<?php echo $productbyiddata['product_3dfilter'];?>" download>3D Filter</a></p>
            </div>
        </div>

        <div class="row mgt-50 mglr-80">
            <div class="col-lg-12">
            <?php echo $productbyiddata['product_description_detail2'];?>
            </div>
        </div>
    </div>
</section>





<script>
    $(document).ready(function(){

        $('.thumbnailpic').click(function() {
        

        $("#mainpic").empty();
        $("#mainpic").append("<a href='<?php echo base_url(); ?>image/product/"+this.id+"' title='Smeg' data-spzoom><img src='<?php echo base_url(); ?>image/product/"+this.id+"' class='spzoom-image' alt='thumb'/></a>");
        $(function() {
            $('[data-spzoom]').spzoom();
        });
        });


    });
</script>
   
<script src="<?php echo base_url(); ?>asset/lib/zoomer/jquery.spzoom.js"></script>
<script>
$(function() {
            $('[data-spzoom]').spzoom();
        });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
