<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="Smeg">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>SMEG</title>

			<!-- CSS ============================================= -->
			<link rel="stylesheet" href="<?php echo base_url(); ?>asset/bootstrap/css/bootstrap.css">
            <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/smeg.css">
			<script src="<?php echo base_url(); ?>asset/js/jquery.js"></script>
			  <script src="<?php echo base_url(); ?>asset/js/popper.min.js"></script>
			  <script src="<?php echo base_url(); ?>asset/instashow/elfsight-instagram-feed.js"></script>
			<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
			<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
			<link href="<?php echo base_url(); ?>asset/lib/zoomer/jquery.spzoom.css" rel="stylesheet" type="text/css">
		</head>
		<body>	

<div class="container-fluid">
<div class="row">
<div class="col-lg-12 text-center">
<img src="<?php echo base_url();?>image/landing1.jpg"  width="100%" height="100%">
</div>
<div class="col-lg-12 text-center mgb-50" >
    <a href="<?php echo base_url();?>home" class="btn btn-danger btn-lg">Enter Site</a>
</div>
</div>
</div>


   </body>
   </html>
