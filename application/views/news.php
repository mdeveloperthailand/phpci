<section class="settopnewsevent">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="setnewstextbreadcrumbleft mg-0">NEWS & EVENT<span class="float-right setbreadcrumb"> <a href="<?php echo base_url();?>" class="setpointer">Home</a> > <a href="<?php echo base_url();?>news" class="setpointer">News & Event</a> > <span class="setlastbreadcrumb">All</span></span>
</p>
            </div>
        </div>
    </div>
</section>


<section>
    <div class="container">
        <div class="row mgt-50">
            <div class="col-lg-9 offset-lg-1 seven-cols">
            <div class="row seven-cols">
              <?php
              if ($this->uri->segment(3)==""||$this->uri->segment(2)=="index") {
                  echo "<div class='setbordercatenews setwidthseven text-center'> <a class='setnewstextsectwo'>ALL</a> </div>";
                    }else{
                        echo "<div class='setwidthseven text-center'> <a href='".base_url()."news' class='setnewstextsectwo setpointer pd-10new'>ALL</a> </div>";
                    }
                foreach ($categorydata as $key => $value) {
                   if ($this->uri->segment(3)==$value['blog_category_id']) {
                    echo "<div class='setbordercatenews setwidthseven text-center'> <a href='".base_url()."news/newscategory/".$value['blog_category_id']."' class='setnewstextsectwo setpointer pd-10new'>".$value['blog_category_name']."</a> </div>";
                   }else{
                    echo "<div class='setwidthseven text-center'> <a href='".base_url()."news/newscategory/".$value['blog_category_id']."' class='setnewstextsectwo setpointer pd-10new'>".$value['blog_category_name']."</a> </div>";
                   }
                }
              ?>
              <!-- <div class="setwidthseven text-center"> <a class="setnewstextsectwo">Awards</a> </div>
              <div class="setwidthseven text-center"> <a class="setnewstextsectwo">Promotion</a> </div>
              <div class="setwidthseven text-center"> <a class="setnewstextsectwo">Company News</a> </div>
              <div class="setwidthseven text-center"> <a class="setnewstextsectwo">Events</a> </div>
              <div class="setwidthseven text-center"> <a class="setnewstextsectwo">Product News</a> </div>
              <div class="setwidthseven text-center"> <a class="setnewstextsectwo">Retailer</a> </div> -->
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row mgt-50 setmglr80res">
                

            <?php
                foreach ($newsbycategoryiddata as $key => $value) {
                   
                    $num_newlines =  strlen($value['blog_description_subject']); 
                    echo "<div class='col-lg-12 mgt-20'>
                    <div class='row'>
                        <div class='col-lg-6 pdlr-0 setheightnewspixall'>
                            <img src='".base_url()."image/blog/".$value['blog_image']."' width='100%' alt=''>
                        </div>
                        <div class='col-lg-6 setnewsbordercolright setheightnewspixall'>
                            <p class='mgt-50 setnewstextcatemain'>".$value['blog_type_name']."</p>";

                            // if ($num_newlines>15) {
                            //    echo "<h2 class='setnewstextheadline1'>".$value['blog_description_subject']."</h2>";
                            // }elseif ($num_newlines>20) {
                            //      echo "<h2 class='setnewstextheadline2'>".$value['blog_description_subject']."</h2>";
                            // }else{
                                echo "<h2 class='setnewstextheadline'>".$value['blog_description_subject']."</h2>";
                            // }

                           
                            echo "<p class='setnewstextdate'>".date('m',strtotime($value['update_date']))."&nbsp;&nbsp;<span class='setbulletdate'>&#9679;</span>&nbsp;&nbsp;".date('d',strtotime($value['update_date']))."&nbsp;&nbsp;<span class='setbulletdate'>&#9679;</span>&nbsp;&nbsp;".date('Y',strtotime($value['update_date']))."</p>
                            <a href='".base_url()."news/".$value['blog_meta_title']."l/".$value['blog_id']."' class='setpointer setnewsbtnreadmore'>Read more</a>
                        </div>
                    </div>
                    </div>  ";
                }
            ?>
            

            <!-- <div class="col-lg-12 mgt-20">
            <div class="row">
                <div class="col-lg-6 pdlr-0">
                    <img src="<?php echo base_url(); ?>image/news1.png" alt="" width="100%">
                </div>
                <div class="col-lg-6 setnewsbordercolright">
                    <p class="mgt-50 setnewstextcatemain">Company News</p>
                    <h2 class="setnewstextheadline">SMEG SHOWROOM UNDER RENOVATION</h2>
                    <p class="setnewstextdate">07&nbsp;&nbsp;<span class="setbulletdate">&#9679;</span>&nbsp;&nbsp;23&nbsp;&nbsp;<span class="setbulletdate">&#9679;</span>&nbsp;&nbsp;2018</p>
                    <a class="setpointer setnewsbtnreadmore">Read more</a>
                </div>
            </div>
            </div>  

            <div class="col-lg-12 mgt-20">
            <div class="row">
                <div class="col-lg-6 pdlr-0">
                    <img src="<?php echo base_url(); ?>image/news1.png" alt="" width="100%">
                </div>
                <div class="col-lg-6 setnewsbordercolright">
                    <p class="mgt-50 setnewstextcatemain">Company News</p>
                    <h2 class="setnewstextheadline">SMEG SHOWROOM UNDER RENOVATION</h2>
                    <p class="setnewstextdate">07&nbsp;&nbsp;<span class="setbulletdate">&#9679;</span>&nbsp;&nbsp;23&nbsp;&nbsp;<span class="setbulletdate">&#9679;</span>&nbsp;&nbsp;2018</p>
                    <a class="setpointer setnewsbtnreadmore">Read more</a>
                </div>
            </div>
            </div>  

             <div class="col-lg-12 mgt-20">
            <div class="row">
                <div class="col-lg-6 pdlr-0">
                    <img src="<?php echo base_url(); ?>image/news1.png" alt="" width="100%">
                </div>
                <div class="col-lg-6 setnewsbordercolright">
                    <p class="mgt-50 setnewstextcatemain">Company News</p>
                    <h2 class="setnewstextheadline">SMEG SHOWROOM UNDER RENOVATION</h2>
                    <p class="setnewstextdate">07&nbsp;&nbsp;<span class="setbulletdate">&#9679;</span>&nbsp;&nbsp;23&nbsp;&nbsp;<span class="setbulletdate">&#9679;</span>&nbsp;&nbsp;2018</p>
                    <a class="setpointer setnewsbtnreadmore">Read more</a>
                </div>
            </div>
            </div>  

             <div class="col-lg-12 mgt-20">
            <div class="row">
                <div class="col-lg-6 pdlr-0">
                    <img src="<?php echo base_url(); ?>image/news1.png" alt="" width="100%">
                </div>
                <div class="col-lg-6 setnewsbordercolright">
                    <p class="mgt-50 setnewstextcatemain">Company News</p>
                    <h2 class="setnewstextheadline">SMEG SHOWROOM UNDER RENOVATION</h2>
                    <p class="setnewstextdate">07&nbsp;&nbsp;<span class="setbulletdate">&#9679;</span>&nbsp;&nbsp;23&nbsp;&nbsp;<span class="setbulletdate">&#9679;</span>&nbsp;&nbsp;2018</p>
                    <a class="setpointer setnewsbtnreadmore">Read more</a>
                </div>
            </div>
            </div>   -->

            <div class="col-lg-12 mgt-50" style="width: 100%"><?php echo $links;?></div>


        </div>      

    </div>
</section>

<style type="text/css">

    .setheightnewspixall{
        overflow: hidden;
        height: 308px;
    }
    .page-link{
        margin: 10px;
    }

    @media screen and (max-width: 1100px){
        .setheightnewspixall{
        overflow: hidden;
        height: 250px;
         }
         .setnewstextheadline{
            font-size: 20px;
         }
         .setheightnewspixall img{
            position: absolute;
            bottom: 0;
         }
    }
</style>
