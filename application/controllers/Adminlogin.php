<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
            parent::__construct();
            // Your own constructor code
            
    }

    public function login()
	{
        if ($this->input->post('submit')=="login") {

            $rs = $this->admin_model->checkdatalogin($this->input->post('username'),$this->input->post('password'));
            if ($rs!=""){
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            // set session
            $userdatasession = array(
                'user_id' => $rs['user_id'],
                'username' => $rs['username'],
                'firstname' => $rs['firstname'],
                'lastname' => $rs['lastname'],
                'permission' => unserialize($rs['permission']),
                'email' => $rs['email'],
                'tel' => $rs['tel'],
                'lineid' => $rs['lineid'],
                'create_date' => $rs['create_date'],
                'update_date' => $rs['update_date'],
                'logged_in' => TRUE
            );
           
            $this->session->set_userdata('userdatasession',$userdatasession);
            $url = base_url()."admin/";
            header("Location: ".$url."");
            }else{
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>Invalid Username or Password</span>
            </div>");
            }

        }

		$this->load->view('adminuser/loginform');
		$this->load->view('adminuser/inc/footer');
    }
    public function logout()
	{
        $this->clearsession('userdatasession');
        $url = base_url()."adminlogin/login";
            header("Location: ".$url."");
    }
   
    public function clearsession($name)
    {
        $this->session->unset_userdata($name);

    }




}
