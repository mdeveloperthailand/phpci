<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminwhatinstore extends CI_Controller {
    
	/**
    * Index Page for this controller.
    *
    * Maps to the following URL
    * 		http://example.com/index.php/welcome
    *	- or -
    * 		http://example.com/index.php/welcome/index
    *	- or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see https://codeigniter.com/user_guide/general/urls.html
    */
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('userdatasession')=="") {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>Please Login</span>
            </div>");
            $url = base_url()."adminlogin/login";
            header("Location: ".$url."");
        }
        
        $user = $this->session->userdata('userdatasession');
            if (!in_array("whatinstore",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
    }
    
    
    public function whatinstorecategorylist()
    {
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/whatinstorecategorylist/";
        $config['total_rows'] =   $this->db->count_all("category");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["whatinstoremaincategorylistdata"] = $this->whatinstore_model->getwhatinstoremaincategoryall($config["per_page"], $this->uri->segment(3));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/whatinstorecategorylist',$data);
		$this->load->view('adminuser/inc/footer');
    }
    
    
    
    public function deletewhatinstore()
    {
        $rs = $this->whatinstore_model->deletewhatinstore($this->uri->segment(3));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            $url = base_url()."adminwhatinstore/whatinstorelist";
            header("Location: ".$url."");
            
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/whatinstorelist";
            header("Location: ".$url."");
            
        }
        
    }
    
    
    
    public function deletewhatinstorecategory()
    {
        $rs = $this->whatinstore_model->deletewhatinstorecategory($this->uri->segment(3));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            $url = base_url()."adminwhatinstore/whatinstorecategorylist";
            header("Location: ".$url."");
            
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/whatinstorecategorylist";
            header("Location: ".$url."");
            
        }
        
    }
    
    public function deletewhatinstorecategorybyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
            foreach ($this->input->post() as $key => $value) {
                $index[] = $value;
            }
            $rs = $this->whatinstore_model->deletewhatinstorecategorybyselect($index);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/whatinstorecategorylist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
                $url = base_url()."adminwhatinstore/whatinstorecategorylist";
                header("Location: ".$url."");
                
            }
            
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/whatinstorecategorylist";
            header("Location: ".$url."");
        }
        
        
        
    }
    
    
    public function clearsession($name)
    {
        $this->session->unset_userdata($name);
        
    }
    
    
    public function addwhatinstorecategorymain()
	{
        
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            if (!isset($_FILES['imagecategory']['name'])) {
                $_FILES['imagecategory']['name'] = null;
            }else{
                $config['upload_path'] = './image/category';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagecategory')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagecategory = null;
                }else{
                    $imagecategory = $this->upload->data('file_name');
                }
            }
            
            
            $datainsert = array(
                'category_image' => $imagecategory, 
                'category_imagealt' => $this->input->post('imagealt'), 
                'parent_id' => 0, 
                'category_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $category_id = $this->whatinstore_model->addwhatinstorecategorymain($datainsert);
            
            
            $datathai = array(
                'category_id' => $category_id, 
                'language_id' => 1, 
                'category_description_name' => $this->input->post('namethai'),  

            );
            $dataeng = array(
                'category_id' => $category_id, 
                'language_id' => 2, 
                'category_description_name' => $this->input->post('nameeng'),  

            );
            $rs = $this->whatinstore_model->addwhatinstorecategorydescriptionmain($datathai,$dataeng);
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/whatinstorecategorylist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
            
            
        }
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/addwhatinstorecategorymain',$data);
		$this->load->view('adminuser/inc/footer');
    }
    
    
    
    public function addwhatinstoresubcategory()
	{
        
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            if (!isset($_FILES['imagecategory']['name'])) {
                $_FILES['imagecategory']['name'] = null;
            }else{
                $config['upload_path'] = './image/category';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagecategory')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagecategory = null;
                }else{
                    $imagecategory = $this->upload->data('file_name');
                }
            }
            
            
            $datainsert = array(
                'category_image' => $imagecategory, 
                'category_imagealt' => $this->input->post('imagealt'), 
                'parent_id' => $this->input->post('parent_id'), 
                'category_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $category_id = $this->whatinstore_model->addwhatinstorecategorymain($datainsert);
            
            
            $datathai = array(
                'category_id' => $category_id, 
                'language_id' => 1, 
                'category_description_name' => $this->input->post('namethai'),  

            );
            $dataeng = array(
                'category_id' => $category_id, 
                'language_id' => 2, 
                'category_description_name' => $this->input->post('nameeng'),  

            );
            $rs = $this->whatinstore_model->addwhatinstorecategorydescriptionmain($datathai,$dataeng);
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/whatinstorecategorylist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
            
            
        }
        
        $data['maincategorydatabyid'] = $this->whatinstore_model->getmaincategorydatabyid($this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/addwhatinstoresubcategory',$data);
		$this->load->view('adminuser/inc/footer');
    }
    
    
    
    
    
    
    
    
    
    public function updatestatuscategory()
    {
        if ($this->input->post('submit')=="Update Status") {
            
            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                    'category_status' => $this->input->post('status'), 
                    'update_date' => date("Y/m/d H:i:s")
                );
                
                $rs = $this->whatinstore_model->updatestatuscategory($this->input->post("checkboxstatus"),$datainsert);
                
                if ($rs==200) {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>SUCCESSFULLY</span>
                    </div>");
                    
                } else {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>NO SUCCESS</span>
                    </div>");
                }
                
            }
            
            
        }
        
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/whatinstorecategorylist/";
        $config['total_rows'] =   $this->db->count_all("category");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["whatinstoremaincategorylistdata"] = $this->whatinstore_model->getwhatinstoremaincategoryallupdatestatus($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/whatinstorecategorylist',$data);
        $this->load->view('adminuser/inc/footer');
        
        
        
        
        
    }
    
    public function editwhatinstorecategory()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            if (!isset($_FILES['imagecategory']['name'])) {
                $_FILES['imagecategory']['name'] = null;
            }elseif (empty($_FILES['imagecategory']['name'])) {
                $imagecategory = $this->input->post('imagecategoryname');
            }else{
                $config['upload_path'] = './image/category';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagecategory')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagecategory = null;
                }else{
                    $imagecategory = $this->upload->data('file_name');
                }
            }
            
            
            $dataupdate = array(
                'category_image' => $imagecategory, 
                'category_imagealt' => $this->input->post('imagealt'), 
                'parent_id' => $this->input->post('parent_id'), 
                'category_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $category_id = $this->whatinstore_model->updatewhatinstorecategory($dataupdate,$this->uri->segment(3));
            
            
            $datathai = array(
                'category_id' => $this->uri->segment(3), 
                'language_id' => 1, 
                'category_description_name' => $this->input->post('namethai'), 

            );
            $dataeng = array(
                'category_id' => $this->uri->segment(3), 
                'language_id' => 2, 
                'category_description_name' => $this->input->post('nameeng'),  

            );
            $rs = $this->whatinstore_model->updatewhatinstorecategorydescription($datathai,$dataeng,$this->uri->segment(3));
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/whatinstorecategorylist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
            
            
        }
        $data['categorydatabyidthai'] = $this->whatinstore_model->getcategorydatabyidthai($this->uri->segment(3));
        $data['categorydatabyideng'] = $this->whatinstore_model->getcategorydatabyideng($this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/editwhatinstorecategory',$data);
        $this->load->view('adminuser/inc/footer');
        
        
    }
    
    public function setwhatinstore()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            if (!isset($_FILES['imagecategory']['name'])) {
                $_FILES['imagecategory']['name'] = null;
            }elseif (empty($_FILES['imagecategory']['name'])) {
                $imagecategory = $this->input->post('imagecategoryname');
            }else{
                $config['upload_path'] = './image/category';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagecategory')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagecategory = null;
                }else{
                    $imagecategory = $this->upload->data('file_name');
                }
            }
            
            
            $datainsert = array(
                'layout_page' => "whatinstore", 
                'layout_image' => $imagecategory, 
                'layout_h1' => $this->input->post('titleh1eng'), 
                'layout_h2' => $this->input->post('titleh2eng'), 
                'layout_h1_th' => $this->input->post('titleh1thai'), 
                'layout_h2_th' => $this->input->post('titleh2thai'), 
                'layout_texteditor' => $this->input->post('detaileng'), 
                'layout_texteditor_th' => $this->input->post('detailthai'), 
                'layout_meta_page_title' => $this->input->post('metatitle'), 
                'layout_meta_description' => $this->input->post('metadescription'), 
                'layout_meta_keyword' => $this->input->post('metakeyword'), 
                'layout_imagealt' => $this->input->post('imagealt'), 
                'layout_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->updatesetwhatinstore($datainsert);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."setwhatinstore");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        $data['setwhatinstoredata'] = $this->whatinstore_model->getsetwhatinstore();
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/setwhatinstore',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    public function setwhatinstoreproduct()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            if (!isset($_FILES['imagecategory']['name'])) {
                $_FILES['imagecategory']['name'] = null;
            }elseif (empty($_FILES['imagecategory']['name'])) {
                $imagecategory = $this->input->post('imagecategoryname');
            }else{
                $config['upload_path'] = './image/category';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagecategory')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagecategory = null;
                }else{
                    $imagecategory = $this->upload->data('file_name');
                }
            }
            
            
            $datainsert = array(
                'layout_page' => "whatinstoreproduct", 
                'layout_image' => $imagecategory, 
                'layout_h1' => $this->input->post('titleh1eng'), 
                'layout_h2' => $this->input->post('titleh2eng'), 
                'layout_h1_th' => $this->input->post('titleh1thai'), 
                'layout_h2_th' => $this->input->post('titleh2thai'), 
                'layout_texteditor' => $this->input->post('detaileng'), 
                'layout_texteditor_th' => $this->input->post('detailthai'), 
                'layout_meta_page_title' => $this->input->post('metatitle'), 
                'layout_meta_description' => $this->input->post('metadescription'), 
                'layout_meta_keyword' => $this->input->post('metakeyword'), 
                'layout_imagealt' => $this->input->post('imagealt'), 
                'layout_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->updatesetwhatinstoreproduct($datainsert);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."setwhatinstoreproduct");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        $data['setwhatinstoredata'] = $this->whatinstore_model->getsetwhatinstoreproduct();
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/setwhatinstoreproduct',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    public function addribbon()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('ribbon_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            if (!isset($_FILES['imageribbon']['name'])) {
                $_FILES['imageribbon']['name'] = null;
            }elseif (empty($_FILES['imageribbon']['name'])) {
                $imageribbon = $this->input->post('imageribbonname');
            }else{
                $config['upload_path'] = './image/ribbon';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imageribbon')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageribbon = null;
                }else{
                    $imageribbon = $this->upload->data('file_name');
                }
            }
            
            
            $datainsert = array(
                'ribbon_image' => $imageribbon, 
                'ribbon_name' => $this->input->post('ribbon_name'), 
                'ribbon_imagealt' => $this->input->post('ribbon_imagealt'), 
                'ribbon_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->addwhatinstoreribbon($datainsert);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."ribbonlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/addribbon',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    public function editribbon()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('ribbon_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            if (!isset($_FILES['imageribbon']['name'])) {
                $_FILES['imageribbon']['name'] = null;
            }elseif (empty($_FILES['imageribbon']['name'])) {
                $imageribbon = $this->input->post('imageribbonname');
            }else{
                $config['upload_path'] = './image/ribbon';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imageribbon')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageribbon = null;
                }else{
                    $imageribbon = $this->upload->data('file_name');
                }
            }
            
            
            $datainsert = array(
                'ribbon_image' => $imageribbon, 
                'ribbon_name' => $this->input->post('ribbon_name'), 
                'ribbon_imagealt' => $this->input->post('ribbon_imagealt'), 
                'ribbon_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->updateribbon($datainsert,$this->uri->segment(3));
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."ribbonlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        $data["ribbonbyiddata"] = $this->whatinstore_model->getribbonbyid($this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/editribbon',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    
    
    public function ribbonlist()
    {
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/ribbonlist/";
        $config['total_rows'] =   $this->db->count_all("ribbon");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["ribbonlistdata"] = $this->whatinstore_model->getribbonall($config["per_page"], $this->uri->segment(3));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/ribbonlist',$data);
		$this->load->view('adminuser/inc/footer');
    }
    
    public function deleteribbon()
    {
        $rs = $this->whatinstore_model->deleteribbon($this->uri->segment(3));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            $url = base_url()."adminwhatinstore/ribbonlist";
            header("Location: ".$url."");
            
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/ribbonlist";
            header("Location: ".$url."");
            
        }
        
    }
    
    public function deleteribbonbyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
            foreach ($this->input->post() as $key => $value) {
                $index[] = $value;
            }
            $rs = $this->whatinstore_model->deleteribbonbyselect($index);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/ribbonlist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
                $url = base_url()."adminwhatinstore/ribbonlist";
                header("Location: ".$url."");
                
            }
            
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/ribbonlist";
            header("Location: ".$url."");
        }
        
        
        
    }
    
    
    
    public function updatestatusribbon()
    {
        if ($this->input->post('submit')=="Update Status") {
            
            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                    'ribbon_status' => $this->input->post('status'), 
                    'update_date' => date("Y/m/d H:i:s")
                );
                
                $rs = $this->whatinstore_model->updatestatusribbon($this->input->post("checkboxstatus"),$datainsert);
                
                if ($rs==200) {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>SUCCESSFULLY</span>
                    </div>");
                    $url = base_url()."adminwhatinstore/ribbonlist";
                    header("Location: ".$url."");
                } else {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>NO SUCCESS</span>
                    </div>");
                }
                
            }
            
            
        }
        
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/ribbonlist/";
        $config['total_rows'] =   $this->db->count_all("ribbon");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["ribbonlistdata"] = $this->whatinstore_model->getribbonallupdatestatus($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/ribbonlist',$data);
        $this->load->view('adminuser/inc/footer');
        
        
        
        
        
    }
    
    
    
    
    
    public function featurelist()
    {
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/featurelist/";
        $config['total_rows'] =   $this->db->count_all("feature");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["featurelistdata"] = $this->whatinstore_model->getfeatureall($config["per_page"], $this->uri->segment(3));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/whatinstorefeature',$data);
        $this->load->view('adminuser/inc/footer');
        
    }
    
    
    public function addfeature()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('feature_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            
            $datainsert = array(
                'feature_name_th' => $this->input->post('feature_name_th'), 
                'feature_name_en' => $this->input->post('feature_name_en'), 
                'feature_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->addwhatinstorefeature($datainsert);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."featurelist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/addfeature',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    public function deletefeature()
    {
        $rs = $this->whatinstore_model->deletefeature($this->uri->segment(3));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            $url = base_url()."adminwhatinstore/featurelist";
            header("Location: ".$url."");
            
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/featurelist";
            header("Location: ".$url."");
            
        }
        
    }
    
    public function deletefeaturebyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
            foreach ($this->input->post() as $key => $value) {
                $index[] = $value;
            }
            $rs = $this->whatinstore_model->deletefeaturebyselect($index);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/featurelist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
                $url = base_url()."adminwhatinstore/featurelist";
                header("Location: ".$url."");
                
            }
            
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/featurelist";
            header("Location: ".$url."");
        }
        
        
        
    }
    
    
    
    public function updatestatusfeature()
    {
        if ($this->input->post('submit')=="Update Status") {
            
            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                    'feature_status' => $this->input->post('feature_status'), 
                    'update_date' => date("Y/m/d H:i:s")
                );
                
                $rs = $this->whatinstore_model->updatestatusfeature($this->input->post("checkboxstatus"),$datainsert);
                
                if ($rs==200) {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>SUCCESSFULLY</span>
                    </div>");
                    $url = base_url()."adminwhatinstore/featurelist";
                    header("Location: ".$url."");
                } else {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>NO SUCCESS</span>
                    </div>");
                }
                
            }
            
            
        }
        
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/featurelist/";
        $config['total_rows'] =   $this->db->count_all("feature");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["featurelistdata"] = $this->whatinstore_model->getfeatureallupdatestatus($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/whatinstorefeature',$data);
        $this->load->view('adminuser/inc/footer');
        
        
    }
    
    
    public function editfeature()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('feature_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            
            $datainsert = array(
                'feature_name_th' => $this->input->post('feature_name_th'), 
                'feature_name_en' => $this->input->post('feature_name_en'), 
                'feature_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->updatefeature($datainsert,$this->uri->segment(3));
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."featurelist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        $data['featurebyiddata'] = $this->whatinstore_model->getfeaturebyid($this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/editfeature',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    
    // DIMENSION 
    public function dimensionlist()
    {
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/dimensionlist/";
        $config['total_rows'] =   $this->db->count_all("dimension");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["dimensionlistdata"] = $this->whatinstore_model->getdimensionall($config["per_page"], $this->uri->segment(3));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/dimensionlist',$data);
        $this->load->view('adminuser/inc/footer');
        
    }
    
    
    public function adddimension()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('dimension_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            
            $datainsert = array(
                'dimension_name' => $this->input->post('dimension_name'), 
                'dimension_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->addwhatinstoredimension($datainsert);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."dimensionlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/adddimension',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    public function updatestatusdimension()
    {
        if ($this->input->post('submit')=="Update Status") {
            
            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                    'dimension_status' => $this->input->post('dimension_status'), 
                    'update_date' => date("Y/m/d H:i:s")
                );
                
                $rs = $this->whatinstore_model->updatestatusdimension($this->input->post("checkboxstatus"),$datainsert);
                
                if ($rs==200) {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>SUCCESSFULLY</span>
                    </div>");
                    $url = base_url()."adminwhatinstore/dimensionlist";
                    header("Location: ".$url."");
                } else {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>NO SUCCESS</span>
                    </div>");
                }
                
            }
            
            
        }
        
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/dimensionlist/";
        $config['total_rows'] =   $this->db->count_all("dimension");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["dimensionlistdata"] = $this->whatinstore_model->getdimensionallupdatestatus($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/dimensionlist',$data);
        $this->load->view('adminuser/inc/footer');
        
        
    }
    
    public function deletedimension()
    {
        $rs = $this->whatinstore_model->deletedimension($this->uri->segment(3));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            $url = base_url()."adminwhatinstore/dimensionlist";
            header("Location: ".$url."");
            
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/dimensionlist";
            header("Location: ".$url."");
            
        }
        
    }
    
    public function deletedimensionbyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
            foreach ($this->input->post() as $key => $value) {
                $index[] = $value;
            }
            $rs = $this->whatinstore_model->deletedimensionbyselect($index);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/dimensionlist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
                $url = base_url()."adminwhatinstore/dimensionlist";
                header("Location: ".$url."");
                
            }
            
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/dimensionlist";
            header("Location: ".$url."");
        }
        
        
        
    }
    
    
    public function editdimension()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('dimension_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            
            $datainsert = array(
                'dimension_name' => $this->input->post('dimension_name'), 
                'dimension_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->updatedimension($datainsert,$this->uri->segment(3));
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."dimensionlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        $data['dimensionbyiddata'] = $this->whatinstore_model->getdimensionbyid($this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/editdimension',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    
    
    // DIMENSION END
    
    
    //  CoLOUR
    public function colourlist()
    {
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/colourlist/";
        $config['total_rows'] =   $this->db->count_all("colour");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["colourlistdata"] = $this->whatinstore_model->getcolourall($config["per_page"], $this->uri->segment(3));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/colourlist',$data);
        $this->load->view('adminuser/inc/footer');
        
    }
    
    
    public function addcolour()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('colour_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            
            $datainsert = array(
                'colour_name_th' => $this->input->post('colour_name_th'), 
                'colour_name_en' => $this->input->post('colour_name_en'), 
                'colour_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->addwhatinstorecolour($datainsert);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."colourlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/addcolour',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    public function updatestatuscolour()
    {
        if ($this->input->post('submit')=="Update Status") {
            
            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                    'colour_status' => $this->input->post('colour_status'), 
                    'update_date' => date("Y/m/d H:i:s")
                );
                
                $rs = $this->whatinstore_model->updatestatuscolour($this->input->post("checkboxstatus"),$datainsert);
                
                if ($rs==200) {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>SUCCESSFULLY</span>
                    </div>");
                    $url = base_url()."adminwhatinstore/colourlist";
                    header("Location: ".$url."");
                } else {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>NO SUCCESS</span>
                    </div>");
                }
                
            }
            
            
        }
        
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/colourlist/";
        $config['total_rows'] =   $this->db->count_all("colour");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["colourlistdata"] = $this->whatinstore_model->getcolourallupdatestatus($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/colourlist',$data);
        $this->load->view('adminuser/inc/footer');
        
        
    }
    
    public function deletecolour()
    {
        $rs = $this->whatinstore_model->deletecolour($this->uri->segment(3));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            $url = base_url()."adminwhatinstore/colourlist";
            header("Location: ".$url."");
            
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/colourlist";
            header("Location: ".$url."");
            
        }
        
    }
    
    public function deletecolourbyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
            foreach ($this->input->post() as $key => $value) {
                $index[] = $value;
            }
            $rs = $this->whatinstore_model->deletecolourbyselect($index);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/colourlist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
                $url = base_url()."adminwhatinstore/colourlist";
                header("Location: ".$url."");
                
            }
            
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/colourlist";
            header("Location: ".$url."");
        }
        
        
        
    }
    
    
    public function editcolour()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('colour_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            
            $datainsert = array(
                'colour_name_th' => $this->input->post('colour_name_th'), 
                'colour_name_en' => $this->input->post('colour_name_en'), 
                'colour_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->updatecolour($datainsert,$this->uri->segment(3));
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."colourlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        $data['colourbyiddata'] = $this->whatinstore_model->getcolourbyid($this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/editcolour',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    //  CoLOUR END
    
    
    //  Aesthetic
    public function aestheticlist()
    {
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/aestheticlist/";
        $config['total_rows'] =   $this->db->count_all("aesthetic");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["aestheticlistdata"] = $this->whatinstore_model->getaestheticall($config["per_page"], $this->uri->segment(3));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/aestheticlist',$data);
        $this->load->view('adminuser/inc/footer');
        
    }
    
    
    public function addaesthetic()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('aesthetic_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            if (!isset($_FILES['imageaesthetic']['name'])) {
                $_FILES['imageaesthetic']['name'] = null;
            }elseif (empty($_FILES['imageaesthetic']['name'])) {
                $imageaesthetic = $this->input->post('imageaestheticname');
            }else{
                $config['upload_path'] = './image/aesthetic';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imageaesthetic')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageaesthetic = null;
                }else{
                    $imageaesthetic = $this->upload->data('file_name');
                }
            }
            
            
            $datainsert = array(
                'aesthetic_image' => $imageaesthetic, 
                'aesthetic_name_th' => $this->input->post('aesthetic_name_th'), 
                'aesthetic_name_en' => $this->input->post('aesthetic_name_en'), 
                'aesthetic_description_th' => $this->input->post('aesthetic_description_th'), 
                'aesthetic_description_en' => $this->input->post('aesthetic_description_en'), 
                'aesthetic_imagealt' => $this->input->post('aesthetic_imagealt'), 
                'aesthetic_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->addwhatinstoreaesthetic($datainsert);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."aestheticlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/addaesthetic',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    public function updatestatusaesthetic()
    {
        if ($this->input->post('submit')=="Update Status") {
            
            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                    'aesthetic_status' => $this->input->post('aesthetic_status'), 
                    'update_date' => date("Y/m/d H:i:s")
                );
                
                $rs = $this->whatinstore_model->updatestatusaesthetic($this->input->post("checkboxstatus"),$datainsert);
                
                if ($rs==200) {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>SUCCESSFULLY</span>
                    </div>");
                    $url = base_url()."adminwhatinstore/aestheticlist";
                    header("Location: ".$url."");
                } else {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>NO SUCCESS</span>
                    </div>");
                }
                
            }
            
            
        }
        
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/aestheticlist/";
        $config['total_rows'] =   $this->db->count_all("aesthetic");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["aestheticlistdata"] = $this->whatinstore_model->getaestheticallupdatestatus($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/aestheticlist',$data);
        $this->load->view('adminuser/inc/footer');
        
        
    }
    
    public function deleteaesthetic()
    {
        $rs = $this->whatinstore_model->deleteaesthetic($this->uri->segment(3));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            $url = base_url()."adminwhatinstore/aestheticlist";
            header("Location: ".$url."");
            
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/aestheticlist";
            header("Location: ".$url."");
            
        }
        
    }
    
    public function deleteaestheticbyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
            foreach ($this->input->post() as $key => $value) {
                $index[] = $value;
            }
            $rs = $this->whatinstore_model->deleteaestheticbyselect($index);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/aestheticlist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
                $url = base_url()."adminwhatinstore/aestheticlist";
                header("Location: ".$url."");
                
            }
            
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/aestheticlist";
            header("Location: ".$url."");
        }
        
        
        
    }
    
    
    
    public function editaesthetic()
    {
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('aesthetic_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");
            
            // Image Single Upload
            if (!isset($_FILES['imageaesthetic']['name'])) {
                $_FILES['imageaesthetic']['name'] = null;
            }elseif (empty($_FILES['imageaesthetic']['name'])) {
                $imageaesthetic = $this->input->post('imageaestheticname');
            }else{
                $config['upload_path'] = './image/aesthetic';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imageaesthetic')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageaesthetic = null;
                }else{
                    $imageaesthetic = $this->upload->data('file_name');
                }
            }
             // Image Single Upload
            
            
            $datainsert = array(
                'aesthetic_image' => $imageaesthetic, 
                'aesthetic_name_th' => $this->input->post('aesthetic_name_th'), 
                'aesthetic_name_en' => $this->input->post('aesthetic_name_en'), 
                'aesthetic_description_th' => $this->input->post('aesthetic_description_th'), 
                'aesthetic_description_en' => $this->input->post('aesthetic_description_en'), 
                'aesthetic_imagealt' => $this->input->post('aesthetic_imagealt'), 
                'aesthetic_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->whatinstore_model->updateaesthetic($datainsert,$this->uri->segment(3));
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."aestheticlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        $data["aestheticbyiddata"] = $this->whatinstore_model->getaestheticbyid($this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/editaesthetic',$data);
        $this->load->view('adminuser/inc/footer');
    }
    
    
    //  Aesthetic END
    
    
    // What in store Main

    public function productlist()
    {
        $config = array();
        $config["base_url"] = base_url()."adminwhatinstore/productlist/";
        $config['total_rows'] =   $this->db->count_all("product");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 1;
        }
        $data["categorymainalldata"] = $this->whatinstore_model->getcategorymainselectall();
        $data["ribbonalldata"] = $this->whatinstore_model->getribbonselectall();
        $data["productlistdata"] = $this->whatinstore_model->getproductall($config["per_page"], $this->uri->segment(3));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminwhatinstore/productlist',$data);
        $this->load->view('adminuser/inc/footer');
        
    }


    
    public function addproduct()
    {
        if ($this->input->post('save')=="save") {
            if ($this->input->post('product_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");

            // Image Single Upload
            if (!isset($_FILES['imageproductmain']['name'])) {
                $_FILES['imageproductmain']['name'] = null;
            }else{
                $config['upload_path'] = './image/product';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imageproductmain')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageproductmain = null;
                }else{
                    $imageproductmain = $this->upload->data('file_name');
                }
            }
             // Image Single Upload
            
            // MULTIPLE IMAGE
            if (!isset($_FILES['imageproduct']['name'])) {
                $_FILES['imageproduct']['name'] = null;
            }else{
                
                $data = array();
                $filesCount = count($_FILES['imageproduct']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['file']['name']     = $_FILES['imageproduct']['name'][$i];
                    $_FILES['file']['type']     = $_FILES['imageproduct']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['imageproduct']['tmp_name'][$i];
                    $_FILES['file']['error']     = $_FILES['imageproduct']['error'][$i];
                    $_FILES['file']['size']     = $_FILES['imageproduct']['size'][$i];
                    
                    // File upload configuration
                    $uploadPath = './image/product';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    
                    // Load and initialize upload library
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $imageproduct[$i] = $fileData['file_name'];
                    }
                } 
            }
             // MULTIPLE IMAGE

              // Image Single Upload
            if (!isset($_FILES['imagesymbolic']['name'])) {
                $_FILES['imagesymbolic']['name'] = null;
            }else{
                $config['upload_path'] = './image/symbolic';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagesymbolic')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagesymbolic = null;
                }else{
                    $imagesymbolic = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

              // Image Single Upload
            if (!isset($_FILES['imagesimilar']['name'])) {
                $_FILES['imagesimilar']['name'] = null;
            }else{
                $config['upload_path'] = './image/similar';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagesimilar')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagesimilar = null;
                }else{
                    $imagesimilar = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

             // Image Single Upload
            if (!isset($_FILES['technicaldrawing']['name'])) {
                $_FILES['technicaldrawing']['name'] = null;
            }else{
                $config['upload_path'] = './image/technicaldrawing';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('technicaldrawing')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagetechnicaldrawing = null;
                }else{
                    $imagetechnicaldrawing = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

             // Image Single Upload
            if (!isset($_FILES['instructionmanual']['name'])) {
                $_FILES['instructionmanual']['name'] = null;
            }else{
                $config['upload_path'] = './image/instructionmanual';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('instructionmanual')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageinstructionmanual = null;
                }else{
                    $imageinstructionmanual = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

             // Image Single Upload
            if (!isset($_FILES['pdfbulletin']['name'])) {
                $_FILES['pdfbulletin']['name'] = null;
            }else{
                $config['upload_path'] = './image/pdfbulletin';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('pdfbulletin')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagepdfbulletin = null;
                }else{
                    $imagepdfbulletin = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

             // Image Single Upload
            if (!isset($_FILES['3dfilter']['name'])) {
                $_FILES['3dfilter']['name'] = null;
            }else{
                $config['upload_path'] = './image/3dfilter';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('3dfilter')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $image3dfilter = null;
                }else{
                    $image3dfilter = $this->upload->data('file_name');
                }
            }
             // Image Single Upload
            
            
            $datamain = array(
                'product_model' => $this->input->post('product_model'), 
                'category_main' => $this->input->post('categorymain'),
                'category_id' => $this->input->post('categorysub'),
                'aesthetic_id' => $this->input->post('aesthetic_id'),
                'feature_id' => $this->input->post('feature_id'),
                'dimension_id' => $this->input->post('dimension_id'),
                'colour_id' => $this->input->post('colour_id'),
                'ribbon_id' => $this->input->post('ribbon_id'),
                'product_image' => $imageproductmain,
                'product_imagesymbolic' => $imagesymbolic,
                'product_imagesimilar' => $imagesimilar,
                'product_technicaldrawing' => $imagetechnicaldrawing,
                'product_instructionmanual' => $imageinstructionmanual,
                'product_pdfbulletin' => $imagepdfbulletin,
                'product_3dfilter' => $image3dfilter,
                'product_meta_title' => $this->input->post('product_meta_title'),
                'product_meta_description' => $this->input->post('product_meta_description'),
                'product_meta_keyword' => $this->input->post('product_meta_keyword'), 
                'product_price' => $this->input->post('product_price'), 
                'product_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $product_id = $this->whatinstore_model->addproduct($datamain);
            
            $detailthai = array(
                'product_id' => $product_id, 
                'language_id' => 1, 
                'product_description_name' => $this->input->post('product_name_th'), 
                'product_description_short_detail' => $this->input->post('product_short_description_th'), 
                'product_description_detail' => $this->input->post('product_description_th'),
                'product_description_detail2' => $this->input->post('product_description_th2'), 
            );
            
            $detaileng = array(
                'product_id' => $product_id, 
                'language_id' => 2, 
                'product_description_name' => $this->input->post('product_name_en'), 
                'product_description_short_detail' => $this->input->post('product_short_description_en'), 
                'product_description_detail' => $this->input->post('product_description_en'),
                'product_description_detail2' => $this->input->post('product_description_en2'), 
 
            );
            
            $this->whatinstore_model->addproductdescription($detailthai);
            $this->whatinstore_model->addproductdescription($detaileng);
            $rs = $this->whatinstore_model->addproductimage($product_id,$imageproduct);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."productlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        
        $data["categorymainlistdata"] = $this->whatinstore_model->getcategorymainselectall();
        $data["aestheticlistdata"] = $this->whatinstore_model->getaestheticselectall();
        $data["ribbonlistdata"] = $this->whatinstore_model->getribbonselectall();
        $data["colourlistdata"] = $this->whatinstore_model->getcolourselectall();
        $data["dimensionlistdata"] = $this->whatinstore_model->getdimensionselectall();
        $data["featurelistdata"] = $this->whatinstore_model->getfeatureselectall();

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/addproduct',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function getsubcategory()
    {
        if (isset($_POST['category_id'])) {
            $rs = $this->whatinstore_model->getcategorysubselectall($_POST['category_id']);
            echo json_encode($rs);
        }
        
    }


    public function deleteproduct()
    {
        $rs = $this->whatinstore_model->deleteproduct($this->uri->segment(3));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            $url = base_url()."adminwhatinstore/productlist";
            header("Location: ".$url."");
            
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/productlist";
            header("Location: ".$url."");
            
        }
        
    }


    public function deleteproductbyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
            foreach ($this->input->post() as $key => $value) {
                $index[] = $value;
            }
            $rs = $this->whatinstore_model->deleteproductbyselect($index);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/productlist";
                header("Location: ".$url."");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
                $url = base_url()."adminwhatinstore/productlist";
                header("Location: ".$url."");
                
            }
            
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminwhatinstore/productlist";
            header("Location: ".$url."");
        }
        
        
        
    }


    public function updatestatusproduct()
    {
        if ($this->input->post('submit')=="Update Status") {
            
            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                    'product_status' => $this->input->post('product_status'), 
                    'update_date' => date("Y/m/d H:i:s")
                );
                
                $rs = $this->whatinstore_model->updatestatusproduct($this->input->post("checkboxstatus"),$datainsert);
                
                if ($rs==200) {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>SUCCESSFULLY</span>
                    </div>");
                    $url = base_url()."adminwhatinstore/productlist";
                    header("Location: ".$url."");
                } else {
                    $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                    <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                    <span class='font-weight-semibold'>NO SUCCESS</span>
                    </div>");
                }
                
            }
            
            
        }
        
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/productlist',$data);
        $this->load->view('adminuser/inc/footer');
        
        
    }


    public function editproduct()
    {
        if ($this->input->post('save')=="save") {
            if ($this->input->post('product_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
            
            date_default_timezone_set("Asia/Bangkok");

            // Image Single Upload
            if (!isset($_FILES['imageproductmain']['name'])) {
                $_FILES['imageproductmain']['name'] = null;
            }elseif (empty($_FILES['imageproductmain']['name'])) {
                $imageproductmain = $this->input->post('imageproductmainname');
            }else{
                $config['upload_path'] = './image/product';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imageproductmain')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageproductmain = null;
                }else{
                    $imageproductmain = $this->upload->data('file_name');
                }
            }
             // Image Single Upload
            
            // MULTIPLE IMAGE
            if (!isset($_FILES['imageproductmain']['name'])) {
                $_FILES['imageproductmain']['name'] = null;
            }else{ 
                $data = array();
                $filesCount = count($_FILES['imageproduct']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['file']['name']     = $_FILES['imageproduct']['name'][$i];
                    $_FILES['file']['type']     = $_FILES['imageproduct']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['imageproduct']['tmp_name'][$i];
                    $_FILES['file']['error']     = $_FILES['imageproduct']['error'][$i];
                    $_FILES['file']['size']     = $_FILES['imageproduct']['size'][$i];
                    
                    // File upload configuration
                    $uploadPath = './image/product';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    
                    // Load and initialize upload library
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data
                        $fileData = $this->upload->data();
                        $imageproduct[$i] = $fileData['file_name'];
                    }
                } 
            }
             // MULTIPLE IMAGE

              // Image Single Upload
            if (!isset($_FILES['imagesymbolic']['name'])) {
                $_FILES['imagesymbolic']['name'] = null;
            }elseif (empty($_FILES['imagesymbolic']['name'])) {
                $imagesymbolic = $this->input->post('imagesymbolicname');
            }else{
                $config['upload_path'] = './image/symbolic';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagesymbolic')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagesymbolic = null;
                }else{
                    $imagesymbolic = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

              // Image Single Upload
            if (!isset($_FILES['imagesimilar']['name'])) {
                $_FILES['imagesimilar']['name'] = null;
            }elseif (empty($_FILES['imagesimilar']['name'])) {
                $imagesimilar = $this->input->post('imagesimilarname');
            }else{
                $config['upload_path'] = './image/similar';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imagesimilar')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagesimilar = null;
                }else{
                    $imagesimilar = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

             // Image Single Upload
            if (!isset($_FILES['technicaldrawing']['name'])) {
                $_FILES['technicaldrawing']['name'] = null;
            }elseif (empty($_FILES['technicaldrawing']['name'])) {
                $imagetechnicaldrawing = $this->input->post('technicaldrawingname');
            }else{
                $config['upload_path'] = './image/technicaldrawing';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('technicaldrawing')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagetechnicaldrawing = null;
                }else{
                    $imagetechnicaldrawing = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

             // Image Single Upload
            if (!isset($_FILES['instructionmanual']['name'])) {
                $_FILES['instructionmanual']['name'] = null;
            }elseif (empty($_FILES['instructionmanual']['name'])) {
                $imageinstructionmanual = $this->input->post('instructionmanualname');
            }else{
                $config['upload_path'] = './image/instructionmanual';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('instructionmanual')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageinstructionmanual = null;
                }else{
                    $imageinstructionmanual = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

             // Image Single Upload
            if (!isset($_FILES['pdfbulletin']['name'])) {
                $_FILES['pdfbulletin']['name'] = null;
            }elseif (empty($_FILES['pdfbulletin']['name'])) {
                $imagepdfbulletin = $this->input->post('pdfbulletinname');
            }else{
                $config['upload_path'] = './image/pdfbulletin';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('pdfbulletin')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imagepdfbulletin = null;
                }else{
                    $imagepdfbulletin = $this->upload->data('file_name');
                }
            }
             // Image Single Upload

             // Image Single Upload
            if (!isset($_FILES['3dfilter']['name'])) {
                $_FILES['3dfilter']['name'] = null;
            }elseif (empty($_FILES['3dfilter']['name'])) {
                $image3dfilter = $this->input->post('3dfiltername');
            }else{
                $config['upload_path'] = './image/3dfilter';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('3dfilter')) {
                    // Files Upload Success
                    // var_dump($this->upload->data('file_name'));
                } else {
                    // Files Upload Not Success!!
                    $errors = $this->upload->display_errors();
                    echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $image3dfilter = null;
                }else{
                    $image3dfilter = $this->upload->data('file_name');
                }
            }
             // Image Single Upload
            
            
            $datamain = array(
                'product_model' => $this->input->post('product_model'), 
                'category_main' => $this->input->post('categorymain'),
                'category_id' => $this->input->post('categorysub'),
                'aesthetic_id' => $this->input->post('aesthetic_id'),
                'feature_id' => $this->input->post('feature_id'),
                'dimension_id' => $this->input->post('dimension_id'),
                'colour_id' => $this->input->post('colour_id'),
                'ribbon_id' => $this->input->post('ribbon_id'),
                'product_image' => $imageproductmain,
                'product_imagesymbolic' => $imagesymbolic,
                'product_imagesimilar' => $imagesimilar,
                'product_technicaldrawing' => $imagetechnicaldrawing,
                'product_instructionmanual' => $imageinstructionmanual,
                'product_pdfbulletin' => $imagepdfbulletin,
                'product_3dfilter' => $image3dfilter,
                'product_meta_title' => $this->input->post('product_meta_title'),
                'product_meta_description' => $this->input->post('product_meta_description'),
                'product_meta_keyword' => $this->input->post('product_meta_keyword'), 
                'product_price' => $this->input->post('product_price'), 
                'product_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $product_id = $this->uri->segment(3);
            $this->whatinstore_model->updateproduct($datamain,$product_id);
            
            $detailthai = array(
                
                'product_description_name' => $this->input->post('product_name_th'), 
                'product_description_short_detail' => $this->input->post('product_short_description_th'), 
                'product_description_detail' => $this->input->post('product_description_th'),
                'product_description_detail2' => $this->input->post('product_description_th2'), 
            );
            
            $detaileng = array(
                
                'product_description_name' => $this->input->post('product_name_en'),
                'product_description_short_detail' => $this->input->post('product_short_description_en'),  
                'product_description_detail' => $this->input->post('product_description_en'),
                'product_description_detail2' => $this->input->post('product_description_en2'), 
 
            );
            
            $this->whatinstore_model->updateproductdescription($detailthai,$product_id,1);
            $this->whatinstore_model->updateproductdescription($detaileng,$product_id,2);
            $rs = $this->whatinstore_model->updateproductimage($product_id,$imageproduct);
            
            
            
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
                </div>");
                $url = base_url()."adminwhatinstore/";
                header("Location: ".$url."productlist");
                
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
                </div>");
            }
            
        }
        $data["productbyiddatathai"] = $this->whatinstore_model->getproductbyid($this->uri->segment(3),1);
        $data["productbyiddataeng"] = $this->whatinstore_model->getproductbyid($this->uri->segment(3),2);
        $data["productimagebyiddata"] = $this->whatinstore_model->getproductimagebyid($this->uri->segment(3),1);
        $data["categorymainlistdata"] = $this->whatinstore_model->getcategorymainselectall();
        $data["aestheticlistdata"] = $this->whatinstore_model->getaestheticselectall();
        $data["ribbonlistdata"] = $this->whatinstore_model->getribbonselectall();
        $data["colourlistdata"] = $this->whatinstore_model->getcolourselectall();
        $data["dimensionlistdata"] = $this->whatinstore_model->getdimensionselectall();
        $data["featurelistdata"] = $this->whatinstore_model->getfeatureselectall();

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/editproduct',$data);
        $this->load->view('adminuser/inc/footer');
    }


    
    // What in store Main END
    
    
}
