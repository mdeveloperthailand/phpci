<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminhomeslide extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
            parent::__construct();
            if ($this->session->userdata('userdatasession')=="") {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>Please Login</span>
            </div>");
            $url = base_url()."adminlogin/login";
            header("Location: ".$url."");
            }


            $user = $this->session->userdata('userdatasession');
            if (!in_array("homeslide",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
            
            
    }

 //  homeslide
 public function homeslidelist()
 {
     $config = array();
     $config["base_url"] = base_url()."adminhomeslide/homeslidelist/";
     $config['total_rows'] =   $this->db->count_all("homeslide");//here we will count all the data from the table
     $config['per_page'] = 10;//number of data to be shown on single page
     $config["uri_segment"] = 3;
     /* EDIT LINK *  */
     $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
     $config['full_tag_close'] 	= '</ul></nav></div>';
     $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['num_tag_close'] 	= '</span></li>';
     $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
     $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
     $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
     $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['prev_tagl_close'] 	= '</span></li>';
     $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['first_tagl_close'] = '</span></li>';
     $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['last_tagl_close'] 	= '</span></li>';
     $this->pagination->initialize($config);
     if($this->uri->segment(3)){
         $page = ($this->uri->segment(3)) ;
     }
     else{
         $page = 1;
     }
     $data["homeslidelistdata"] = $this->homeslide_model->gethomeslideall($config["per_page"], $this->uri->segment(3));
     $data["links"] = $this->pagination->create_links();//create the link for pagination
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('adminhomeslide/homeslidelist',$data);
     $this->load->view('adminuser/inc/footer');
     
 }
 
 
 public function addhomeslide()
 {
     
     if ($this->input->post('save')=="save") {
         if ($this->input->post('homeslide_status')=="on") {
             $status = 1;
         } else {
             $status = 0;
         }
         $user = $this->session->userdata('userdatasession');
         
         date_default_timezone_set("Asia/Bangkok");
         
         if (!isset($_FILES['imagehomeslide']['name'])) {
             $_FILES['imagehomeslide']['name'] = null;
         }elseif (empty($_FILES['imagehomeslide']['name'])) {
             $imagehomeslide = $this->input->post('imagehomeslidename');
         }else{
             $config['upload_path'] = './image/homeslide';
             $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|mp4';
             $config['max_size'] = '200000000';
             // $config['max_width'] = '1024';
             // $config['max_height'] = '1024';
             $config['remove_spaces'] = TRUE;
             $this->load->library("upload",$config);
             if ($this->upload->do_upload('imagehomeslide')) {
                 // Files Upload Success
                 // var_dump($this->upload->data('file_name'));
             } else {
                 // Files Upload Not Success!!
                 $errors = $this->upload->display_errors();
                 echo $errors;
             } // End else
             if ($this->upload->data('file_name')=="") {
                 $imagehomeslide = null;
             }else{
                 $imagehomeslide = $this->upload->data('file_name');
             }
         }
         
         
         $datainsert = array(
             'homeslide_image' => $imagehomeslide, 
             'homeslide_name' => $this->input->post('name'), 
             'homeslide_imagealt' => $this->input->post('homeslide_imagealt'), 
             'homeslide_link' => $this->input->post('link'), 
             'homeslide_order' => $this->input->post('homeslide_order'), 
             'homeslide_status' => $status, 
             'create_date' => date("Y/m/d H:i:s"), 
             'update_date' => date("Y/m/d H:i:s"), 
         );
         
         $rs = $this->homeslide_model->addhomeslide($datainsert);
         
         
         
         if ($rs) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."adminhomeslide/";
             header("Location: ".$url."homeslidelist");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
         }
         
     }
     
     
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('adminhomeslide/addhomeslide',$data);
     $this->load->view('adminuser/inc/footer');
 }
 
 public function updatestatushomeslide()
 {
     if ($this->input->post('submit')=="Update Status") {
         
         date_default_timezone_set("Asia/Bangkok");
         if ($this->input->post("checkboxstatus")!="") {
             $datainsert = array(
                 'homeslide_status' => $this->input->post('homeslide_status'), 
                 'update_date' => date("Y/m/d H:i:s")
             );
             
             $rs = $this->homeslide_model->updatestatushomeslide($this->input->post("checkboxstatus"),$datainsert);
             
             if ($rs==200) {
                 $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                 <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                 <span class='font-weight-semibold'>SUCCESSFULLY</span>
                 </div>");
                 $url = base_url()."adminhomeslide/homeslidelist";
                 header("Location: ".$url."");
             } else {
                 $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                 <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                 <span class='font-weight-semibold'>NO SUCCESS</span>
                 </div>");
             }
             
         }
         
         
     }elseif ($this->input->post('orderbtn')=="Update Order") {

        date_default_timezone_set("Asia/Bangkok");
        
        // $index = array();
        // foreach ($this->input->post("homeslide_order") as $key => $value) {
        //     $index[] = $key; 
        // }

        $rs = $this->homeslide_model->updatehomeslideorder($this->input->post("order"));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
           
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        }
    
        
                
    }
     
     // ================pagination====================
     $config = array();
     $config["base_url"] = base_url()."adminhomeslide/homeslidelist/";
     $config['total_rows'] =   $this->db->count_all("homeslide");//here we will count all the data from the table
     $config['per_page'] = 10;//number of data to be shown on single page
     $config["uri_segment"] = 3;
     /* EDIT LINK *  */
     $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
     $config['full_tag_close'] 	= '</ul></nav></div>';
     $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['num_tag_close'] 	= '</span></li>';
     $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
     $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
     $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
     $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['prev_tagl_close'] 	= '</span></li>';
     $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['first_tagl_close'] = '</span></li>';
     $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['last_tagl_close'] 	= '</span></li>';
     $this->pagination->initialize($config);
     if($this->uri->segment(3)){
         $page = ($this->uri->segment(3)) ;
     }
     else{
         $page = 1;
     }
     $data["homeslidelistdata"] = $this->homeslide_model->gethomeslideallupdatestatus($config["per_page"], $page);
     $data["links"] = $this->pagination->create_links();//create the link for pagination
     // ================pagination====================
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('adminhomeslide/homeslidelist',$data);
     $this->load->view('adminuser/inc/footer');
     
     
 }
 
 public function deletehomeslide()
 {
     $rs = $this->homeslide_model->deletehomeslide($this->uri->segment(3));
     
     if ($rs==200) {
         $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
         <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
         <span class='font-weight-semibold'>SUCCESSFULLY</span>
         </div>");
         $url = base_url()."adminhomeslide/homeslidelist";
         header("Location: ".$url."");
         
     } else {
         $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
         <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
         <span class='font-weight-semibold'>NO SUCCESS</span>
         </div>");
         $url = base_url()."adminhomeslide/homeslidelist";
         header("Location: ".$url."");
         
     }
     
 }
 
 public function deletehomeslidebyselect()
 {
     // print_r($this->input->post());
     if (!empty($this->input->post())) {
         $index = array(); 
         foreach ($this->input->post() as $key => $value) {
             $index[] = $value;
         }
         $rs = $this->homeslide_model->deletehomeslidebyselect($index);
         
         if ($rs==200) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."adminhomeslide/homeslidelist";
             header("Location: ".$url."");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
             $url = base_url()."adminhomeslide/homeslidelist";
             header("Location: ".$url."");
             
         }
         
     }else{
         $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
         <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
         <span class='font-weight-semibold'>NO SUCCESS</span>
         </div>");
         $url = base_url()."adminhomeslide/homeslidelist";
         header("Location: ".$url."");
     }
     
     
     
 }
 
 
 
 public function edithomeslide()
 {
     
     if ($this->input->post('save')=="save") {
         if ($this->input->post('homeslide_status')=="on") {
             $status = 1;
         } else {
             $status = 0;
         }
         $user = $this->session->userdata('userdatasession');
         
         date_default_timezone_set("Asia/Bangkok");
        //  print_r($_FILES);
         // Image Single Upload
         if (!isset($_FILES['imagehomeslide']['name'])) {
             $_FILES['imagehomeslide']['name'] = null;
         }elseif (empty($_FILES['imagehomeslide']['name'])) {
             $imagehomeslide = $this->input->post('imagehomeslidename');
         }else{
             $config['upload_path'] = './image/homeslide';
             $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|mp4';
             $config['max_size'] = '20000000';
             // $config['max_width'] = '1024';
             // $config['max_height'] = '1024';
             $config['remove_spaces'] = TRUE;
             $this->load->library("upload",$config);
             if ($this->upload->do_upload('imagehomeslide')) {
                 // Files Upload Success
                 // var_dump($this->upload->data('file_name'));
             } else {
                 // Files Upload Not Success!!
                 $errors = $this->upload->display_errors();
                 echo $errors;
             } // End else
             if ($this->upload->data('file_name')=="") {
                 $imagehomeslide = null;
             }else{
                 $imagehomeslide = $this->upload->data('file_name');
             }
         }
          // Image Single Upload
         
         
         $datainsert = array(
            'homeslide_image' => $imagehomeslide, 
             'homeslide_name' => $this->input->post('name'), 
             'homeslide_imagealt' => $this->input->post('homeslide_imagealt'),
             'homeslide_link' => $this->input->post('link'),  
             'homeslide_order' => $this->input->post('homeslide_order'), 
             'homeslide_status' => $status, 
             'update_date' => date("Y/m/d H:i:s"), 
         );
         
         $rs = $this->homeslide_model->updatehomeslide($datainsert,$this->uri->segment(3));
         
         
         
         if ($rs) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."adminhomeslide/";
             header("Location: ".$url."homeslidelist");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
         }
         
     }
     
     $data["homeslidebyiddata"] = $this->homeslide_model->gethomeslidebyid($this->uri->segment(3));
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('adminhomeslide/edithomeslide',$data);
     $this->load->view('adminuser/inc/footer');
 }
 
 
 //  homeslide END

}
