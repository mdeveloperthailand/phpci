<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function searchadminlist()
	{
        $main = 'user';
        $join = array('user_group' => 'user_group_id' );
        $alias = 'user_group.name as name,user.firstname as firstname,user.update_date as update_date';
        $data['adminlistdata'] = $this->search_model->getdatasearch($this->input->post(),$main,$join,$alias);

        $data['admingroupdata'] = $this->admin_model->getadmingroup();
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminuser/adminlist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchadmingrouplist()
	{
        $main = 'user_group';
        $join = array();
        $alias = '';
        $data['admingrouplistdata'] = $this->search_model->getdatasearch($this->input->post(),$main,$join,$alias);

        $data['admingroupdata'] = $this->admin_model->getadmingroup();
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header');
        $this->load->view('adminuser/admingrouplist',$data);
        $this->load->view('adminuser/inc/footer');
    }
   
    public function searchnewslist()
	{
         // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchwhatinstorecategorylist/";
        $config['total_rows'] =   $this->db->count_all("blog");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
        $data["newslistdata"] = $this->search_model->getdatasearchnews($config["per_page"],$page,$this->input->post());
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================


        $data['newscategorylistdata'] = $this->news_model->getnewscategoryall(2);
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminnews/newslist',$data);
        $this->load->view('adminuser/inc/footer');
    }


    public function searchnewscategorylist()
	{
        $main = 'blog_category';
        $join = array('blog_category_description' => 'blog_category_id' );
        $alias = '';
        $data['newscategorylistdata'] = $this->search_model->getdatasearchnewscategory($this->input->post(),$main,$join,$alias);

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminnews/newscategorylist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchwhatinstorecategorylist()
	{
        $main = 'category';
        $join = array('category_description' => 'category_id' );
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchwhatinstorecategorylist/";
        $config['total_rows'] =   $this->db->count_all("category");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
        $data["whatinstoremaincategorylistdata"] = $this->search_model->getdatasearchwhatinstorecategory($config["per_page"],$page,$this->input->post(),$main,$join,$alias);
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/whatinstorecategorylist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchribbonlist()
	{
        $main = 'ribbon';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchribbonlist/";
        $config['total_rows'] =   $this->db->count_all("ribbon");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['ribbonlistdata'] = $this->search_model->getdatasearchribbon($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================
        $data['ribbonlistdata'] = $this->search_model->getdatasearchribbon($this->input->post(),$main,$join,$alias);

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/ribbonlist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchfeaturelist()
	{
        $main = 'feature';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchfeaturelist/";
        $config['total_rows'] =   $this->db->count_all("feature");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['featurelistdata'] = $this->search_model->getdatasearchfeature($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/whatinstorefeature',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchdimensionlist()
	{
        $main = 'dimension';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchdimensionlist/";
        $config['total_rows'] =   $this->db->count_all("dimension");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['dimensionlistdata'] = $this->search_model->getdatasearchdimension($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/dimensionlist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchcolourlist()
	{
        $main = 'colour';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchcolourlist/";
        $config['total_rows'] =   $this->db->count_all("colour");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['colourlistdata'] = $this->search_model->getdatasearchcolour($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/colourlist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchaestheticlist()
	{
        $main = 'aesthetic';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchaestheticlist/";
        $config['total_rows'] =   $this->db->count_all("aesthetic");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['aestheticlistdata'] = $this->search_model->getdatasearchaesthetic($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/aestheticlist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchproductlist()
	{
        $main = 'product';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchproductlist/";
        $config['total_rows'] =   $this->db->count_all("product");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data["categorymainalldata"] = $this->whatinstore_model->getcategorymainselectall();
        $data["ribbonalldata"] = $this->whatinstore_model->getribbonselectall();
            $data['productlistdata'] = $this->search_model->getdatasearchproduct($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminwhatinstore/productlist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchpersmegtivelist()
	{
        $main = 'persmegtive';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchpersmegtivelist/";
        $config['total_rows'] =   $this->db->count_all("persmegtive");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['persmegtivelistdata'] = $this->search_model->getdatasearchpersmegtive($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminpersmegtive/persmegtivelist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchreviewlist()
	{
        $main = 'review';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchreviewlist/";
        $config['total_rows'] =   $this->db->count_all("review");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['reviewlistdata'] = $this->search_model->getdatasearchreview($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminreview/reviewlist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchstorelocatorlist()
	{
        $main = 'storelocator';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchstorelocatorlist/";
        $config['total_rows'] =   $this->db->count_all("storelocator");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['storelocatorlistdata'] = $this->search_model->getdatasearchstorelocator($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminstorelocator/storelocatorlist',$data);
        $this->load->view('adminuser/inc/footer');
    }


    public function searchfeatureproductlist()
	{
        $main = 'feature_product';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchfeaturelist/";
        $config['total_rows'] =   $this->db->count_all("feature_product");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['featurelistdata'] = $this->search_model->getdatasearchfeatureproduct($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminfeature/featurelist',$data);
        $this->load->view('adminuser/inc/footer');
    }

    public function searchhomeslidelist()
	{
        $main = 'homeslide';
        $join = array();
        $alias = '';
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."search/searchhomeslidelist/";
        $config['total_rows'] =   $this->db->count_all("homeslide");//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
            }
            else{
            $page = 1;
            }
            $data['homeslidelistdata'] = $this->search_model->getdatasearchhomeslide($this->input->post(),$main,$join,$alias);
            $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminhomeslide/homeslidelist',$data);
        $this->load->view('adminuser/inc/footer');
    }



}
