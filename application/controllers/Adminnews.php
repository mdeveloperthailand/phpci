<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminnews extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
            parent::__construct();
            if ($this->session->userdata('userdatasession')=="") {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>Please Login</span>
            </div>");
            $url = base_url()."adminlogin/login";
            header("Location: ".$url."");
            }

            $user = $this->session->userdata('userdatasession');
            if (!in_array("news",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
            
    }
    public function newslist()
    {
        // ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."adminnews/newslist";
        $config['total_rows'] =   $this->db->count_all('blog');//here we will count all the data from the table
        $config['per_page'] = 10;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 0;
        }
        $data['newslistdata'] = $this->news_model->getnewsalladmin($config["per_page"],$page,$this->session->userdata('lang'));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
        // ================pagination====================


        $data['newscategorylistdata'] = $this->news_model->getnewscategoryall(2);
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminnews/newslist',$data);
		$this->load->view('adminuser/inc/footer');
    }

    public function newscategorylist()
    {
        $data['newscategorylistdata'] = $this->news_model->getnewscategoryall(2);
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminnews/newscategorylist',$data);
		$this->load->view('adminuser/inc/footer');
    }


    public function updatestatus()
    {
        if ($this->input->post('submit')=="Update Status") {

            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                'blog_status' => $this->input->post('status'), 
                'update_date' => date("Y/m/d H:i:s")
            );

            $rs = $this->news_model->updatestatusnews($this->input->post("checkboxstatus"),$datainsert);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
        
            }
            
                    
        }elseif ($this->input->post('delete')=="delete") {

            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>delete</span>
        </div>");
        $url = base_url()."admin/adminlist";
        header("Location: ".$url."");
        }elseif ($this->input->post('orderbtn')=="Update Order") {

            date_default_timezone_set("Asia/Bangkok");
            
            $index = array();
            foreach ($this->input->post("order") as $key => $value) {
                $index[] = $key; 
            }

            $rs = $this->news_model->updatenewsorder($this->input->post("order"),$index);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
        
            
                    
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>Error</span>
        </div>");
        $url = base_url()."adminnews/newslist";
        header("Location: ".$url."");
        }

        $data['newslistdata'] = $this->news_model->getnewsall(2);
        $data['newscategorylistdata'] = $this->news_model->getnewscategoryall(2);
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminnews/newslist',$data);
        $this->load->view('adminuser/inc/footer');



    }

    public function deletenews()
    {
        $rs = $this->news_model->deletenews($this->uri->segment(3));
            
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
        $url = base_url()."adminnews/newslist";
        header("Location: ".$url."");

        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        $url = base_url()."adminnews/newslist";
        header("Location: ".$url."");

        }

    }

    public function deletenewsbyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
           foreach ($this->input->post() as $key => $value) {
              $index[] = $value;
           }
           $rs = $this->news_model->deletenewsbyselect($index);
                
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
        $url = base_url()."adminnews/newslist";
        header("Location: ".$url."");

        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        $url = base_url()."adminnews/newslist";
        header("Location: ".$url."");

        }

        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminnews/newslist";
            header("Location: ".$url."");
        }
        
       

    }

    public function deletenewscategory()
    {
        $rs = $this->news_model->deletenewscategory($this->uri->segment(3));
            
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
        $url = base_url()."adminnews/newscategorylist";
        header("Location: ".$url."");

        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        $url = base_url()."adminnews/newscategorylist";
        header("Location: ".$url."");

        }

    }

    public function deletenewscategorybyselect()
    {
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
           foreach ($this->input->post() as $key => $value) {
              $index[] = $value;
           }
           $rs = $this->news_model->deletenewscategorybyselect($index);
                
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
        $url = base_url()."adminnews/newscategorylist";
        header("Location: ".$url."");

        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        $url = base_url()."adminnews/newscategorylist";
        header("Location: ".$url."");

        }

        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."adminnews/newscategorylist";
            header("Location: ".$url."");
        }
        
       

    }

    
    public function clearsession($name)
    {
        $this->session->unset_userdata($name);

    }


    public function addnews()
	{

        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');

            date_default_timezone_set("Asia/Bangkok");

            if (!isset($_FILES['imageblog']['name'])) {
                $_FILES['imageblog']['name'] = null;
            }else{
                $config['upload_path'] = './image/blog';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imageblog')) {
                // Files Upload Success
                // var_dump($this->upload->data('file_name'));
                } else {
                // Files Upload Not Success!!
                $errors = $this->upload->display_errors();
                echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageblog = null;
                }else{
                    $imageblog = $this->upload->data('file_name');
                }
            }

        
            $datainsert = array(
                'user_id' => $user['user_id'], 
                'blog_type_id' => $this->input->post('blog_type_id'), 
                'blog_category_id' => $this->input->post('blog_category_id'), 
                'blog_meta_title' => $this->input->post('blog_meta_title'), 
                'blog_meta_description' => $this->input->post('blog_meta_description'), 
                'blog_meta_keyword' => $this->input->post('blog_meta_keyword'), 
                'blog_order' => $this->input->post('blog_order'), 
                'blog_image' => $imageblog, 
                'blog_imagealt' => $this->input->post('imagealt'), 
                'blog_status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );

            $blog_id = $this->news_model->addnews($datainsert);


            $datathai = array(
                'blog_id' => $blog_id, 
                'language_id' => 1, 
                'blog_description_subject' => $this->input->post('titlethai'), 
                'blog_description_detail' => $this->input->post('detailthai'), 
            );
            $dataeng = array(
                'blog_id' => $blog_id, 
                'language_id' => 2, 
                'blog_description_subject' => $this->input->post('titleeng'), 
                'blog_description_detail' => $this->input->post('detaileng'), 
            );
            $rs = $this->news_model->addnewsdescription($datathai,$dataeng);
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
            
            
        
        }
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['newstypedata'] = $this->news_model->getblogtypeall(2);
        $data['newscategorydata'] = $this->news_model->getnewscategoryall(2);
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminnews/addnews',$data);
		$this->load->view('adminuser/inc/footer');
    }

    public function addnewscategory()
	{

        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('blog_category_status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }

            date_default_timezone_set("Asia/Bangkok");

        
            $datainsert = array(
                'blog_category_meta_title' => $this->input->post('blog_category_meta_title'), 
                'blog_category_meta_description' => $this->input->post('blog_category_meta_description'), 
                'blog_category_meta_keyword' => $this->input->post('blog_category_meta_keyword'), 
                'blog_category_status' => $status, 
                'blog_category_create_date' => date("Y/m/d H:i:s"), 
                'blog_category_update_date' => date("Y/m/d H:i:s"), 
            );

            $blog_category_id = $this->news_model->addnewscategory($datainsert);


            $datathai = array(
                'blog_category_id' => $blog_category_id, 
                'language_id' => 1, 
                'blog_category_name' => $this->input->post('namethai'), 
            );
            $dataeng = array(
                'blog_category_id' => $blog_category_id, 
                'language_id' => 2, 
                'blog_category_name' => $this->input->post('nameeng'), 
            );
            $rs = $this->news_model->addnewscategorydescription($datathai,$dataeng);
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
            
            
        
        }
        
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminnews/addnewscategory',$data);
		$this->load->view('adminuser/inc/footer');
    }

    public function editnewscategory()
	{

        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }

            date_default_timezone_set("Asia/Bangkok");

        
            $datainsert = array(
                'blog_category_meta_title' => $this->input->post('blog_category_meta_title'), 
                'blog_category_meta_description' => $this->input->post('blog_category_meta_description'), 
                'blog_category_meta_keyword' => $this->input->post('blog_category_meta_keyword'), 
                'blog_category_status' => $status, 
                'blog_category_create_date' => date("Y/m/d H:i:s"), 
                'blog_category_update_date' => date("Y/m/d H:i:s"), 
            );

            $blog_category_id = $this->news_model->updatenewscategory($datainsert,$this->uri->segment(3));


            $datathai = array(
                'blog_category_id' => $this->uri->segment(3), 
                'language_id' => 1, 
                'blog_category_name' => $this->input->post('namethai'), 
            );
            $dataeng = array(
                'blog_category_id' => $this->uri->segment(3), 
                'language_id' => 2, 
                'blog_category_name' => $this->input->post('nameeng'), 
            );
            $rs = $this->news_model->updatenewscategorydescription($datathai,$dataeng,$this->uri->segment(3));
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
            
            
        
        }

        $data['newscategorybyiddatathai'] = $this->news_model->getnewscategorybyid(1,$this->uri->segment(3));
        $data['newscategorybyiddataeng'] = $this->news_model->getnewscategorybyid(2,$this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminnews/editnewscategory',$data);
		$this->load->view('adminuser/inc/footer');
    }

    public function editnews()
	{

        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            $user = $this->session->userdata('userdatasession');
    
            date_default_timezone_set("Asia/Bangkok");

            if (!isset($_FILES['imageblog']['name'])) {
                $_FILES['imageblog']['name'] = null;
            }elseif (empty($_FILES['imageblog']['name'])) {
                $imageblog = $this->input->post('imageblogname');
            }else{
                $config['upload_path'] = './image/blog';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
                $config['max_size'] = '2000000';
                // $config['max_width'] = '1024';
                // $config['max_height'] = '1024';
                $config['remove_spaces'] = TRUE;
                $config['encrypt_name'] = TRUE;
                // $new_name = time().$_FILES["userfiles"]['name'];
                // $config['file_name'] = $new_name;
                $this->load->library("upload",$config);
                if ($this->upload->do_upload('imageblog')) {
                // Files Upload Success
                // var_dump($this->upload->data('file_name'));
                } else {
                // Files Upload Not Success!!
                $errors = $this->upload->display_errors();
                echo $errors;
                } // End else
                if ($this->upload->data('file_name')=="") {
                    $imageblog = null;
                }else{
                    $imageblog = $this->upload->data('file_name');
                }
            }

        
            $dataupdate = array(
                'blog_type_id' => $this->input->post('blog_type_id'), 
                'blog_category_id' => $this->input->post('blog_category_id'), 
                'blog_meta_title' => $this->input->post('blog_meta_title'), 
                'blog_meta_description' => $this->input->post('blog_meta_description'), 
                'blog_meta_keyword' => $this->input->post('blog_meta_keyword'), 
                'blog_order' => $this->input->post('blog_order'), 
                'blog_image' => $imageblog, 
                'blog_imagealt' => $this->input->post('imagealt'), 
                'blog_status' => $status, 
                'update_date' => date("Y/m/d H:i:s"), 
            );

            $this->news_model->updatenews($dataupdate,$this->uri->segment(3));


            $datathai = array(
                'blog_id' => $this->uri->segment(3),
                'language_id' => 1, 
                'blog_description_subject' => $this->input->post('titlethai'), 
                'blog_description_detail' => $this->input->post('detailthai'), 
            );
            $dataeng = array(
                'blog_id' => $this->uri->segment(3), 
                'language_id' => 2, 
                'blog_description_subject' => $this->input->post('titleeng'), 
                'blog_description_detail' => $this->input->post('detaileng'), 
            );
            $rs = $this->news_model->updatenewsdescription($datathai,$dataeng,$this->uri->segment(3));
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
            
            
        
        }
        $data['newsbyiddatathai'] = $this->news_model->getnewsbyid(1,$this->uri->segment(3));
        $data['newsbyiddataeng'] = $this->news_model->getnewsbyid(2,$this->uri->segment(3));
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['newstypedata'] = $this->news_model->getblogtypeall(2);
        $data['newscategorydata'] = $this->news_model->getnewscategoryall(2);
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminnews/editnews',$data);
		$this->load->view('adminuser/inc/footer');
    }









    public function updatestatuscategory()
    {
        if ($this->input->post('submit')=="Update Status") {

            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                'blog_category_status' => $this->input->post('blog_category_status'), 
                'blog_category_update_date' => date("Y/m/d H:i:s")
            );

            $rs = $this->news_model->updatestatusnewscategory($this->input->post("checkboxstatus"),$datainsert);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
        
            }
            
                    
        }elseif ($this->input->post('delete')=="delete") {

            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>delete</span>
        </div>");
        $url = base_url()."adminnews/newscategorylist";
        header("Location: ".$url."");
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>Error</span>
        </div>");
        $url = base_url()."adminnews/newscategorylist";
        header("Location: ".$url."");
        }

        $data['newscategorylistdata'] = $this->news_model->getnewscategoryall(2);
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminnews/newscategorylist',$data);
        $this->load->view('adminuser/inc/footer');



    }












}
