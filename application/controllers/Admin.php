<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
            parent::__construct();
            if ($this->session->userdata('userdatasession')=="") {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>Please Login</span>
            </div>");
            $url = base_url()."adminlogin/login";
            header("Location: ".$url."");
            }

            
    }

	public function index()
	{
        $data['userdatasession'] = $this->session->userdata('userdatasession');
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminuser/home',$data);
		$this->load->view('adminuser/inc/footer');
    }
    public function adminlist()
	{
        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['adminlistdata'] = $this->admin_model->getadminall();
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminuser/adminlist',$data);
		$this->load->view('adminuser/inc/footer');
    }
    public function admingrouplist()
	{

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['admingrouplistdata'] = $this->admin_model->getadmingroup();
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminuser/admingrouplist',$data);
		$this->load->view('adminuser/inc/footer');
    }

    
    public function addadmingroup()
	{

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
        if ($this->input->post('save')=="save") {
            date_default_timezone_set("Asia/Bangkok");
            
            $keep = array();
            foreach ($this->input->post() as $key => $value) {
                if ($key!="save") {
                    $keep[] = $key;
                }
            } 
            $datainsert = array(
                'name' => $this->input->post('groupname'), 
                'permission' => serialize($keep),
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );

            $rs = $this->admin_model->addadmingroup($datainsert);
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
            
            
        
        }

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['admingrouplistdata'] = $this->admin_model->getadmingroup();
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminuser/addadmingroup',$data);
		$this->load->view('adminuser/inc/footer');
    }


    public function editadmingroup()
	{

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
        if ($this->input->post('save')=="save") {
            date_default_timezone_set("Asia/Bangkok");
            
            $keep = array();
            foreach ($this->input->post() as $key => $value) {
                if ($key!="save") {
                    $keep[] = $key;
                }
            } 
            $datainsert = array(
                'name' => $this->input->post('groupname'), 
                'permission' => serialize($keep),
                'update_date' => date("Y/m/d H:i:s"), 
            );

            $rs = $this->admin_model->updateadmingroup($this->input->post('user_group_id'),$datainsert);
            if ($rs==200) {

                $userdatasession = array(
                    'user_id' => $rs['user_id'],
                    'username' => $rs['username'],
                    'firstname' => $rs['firstname'],
                    'lastname' => $rs['lastname'],
                    'permission' => $keep,
                    'email' => $rs['email'],
                    'tel' => $rs['tel'],
                    'lineid' => $rs['lineid'],
                    'create_date' => $rs['create_date'],
                    'update_date' => $rs['update_date'],
                    'logged_in' => TRUE
                );
               
                $this->session->set_userdata('userdatasession',$userdatasession);

                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
            
            
        
        }

        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['admingroupdata'] = $this->admin_model->getadmingroupbyid($this->uri->segment(3));
        $data['permission'] = unserialize($data['admingroupdata']['permission']);
        
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminuser/editadmingroup',$data);
		$this->load->view('adminuser/inc/footer');
    }


    public function addadmin()
	{

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
        
        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            date_default_timezone_set("Asia/Bangkok");
            $datainsert = array(
                'firstname' => $this->input->post('firstname'), 
                'lastname' => $this->input->post('lastname'), 
                'user_group_id' => $this->input->post('user_group_id'), 
                'username' => $this->input->post('username'), 
                'password' => md5($this->input->post('password')), 
                'email' => $this->input->post('email'), 
                'tel' => $this->input->post('tel'), 
                'lineid' => $this->input->post('lineid'), 
                'status' => $status, 
                'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            $rs = $this->admin_model->addadmin($datainsert);
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
            
            
        
        }
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['admingroupdata'] = $this->admin_model->getadmingroup();
		$this->load->view('adminuser/inc/header',$data);
		$this->load->view('adminuser/addadmin',$data);
		$this->load->view('adminuser/inc/footer');
    }
    
    public function updatestatus()
    {

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }

        if ($this->input->post('submit')=="Update Status") {

            date_default_timezone_set("Asia/Bangkok");
            if ($this->input->post("checkboxstatus")!="") {
                $datainsert = array(
                'status' => $this->input->post('status'), 
                'update_date' => date("Y/m/d H:i:s")
            );

            $rs = $this->admin_model->updatestatusadmin($this->input->post("checkboxstatus"),$datainsert);
            
            if ($rs==200) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            }
        
            }
            
                    
        }elseif ($this->input->post('delete')=="delete") {

            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>delete</span>
        </div>");
        $url = base_url()."admin/adminlist";
        header("Location: ".$url."");
        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>Error</span>
        </div>");
        $url = base_url()."admin/adminlist";
        header("Location: ".$url."");
        }
        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['adminlistdata'] = $this->admin_model->getadminall();
        $data['admingroupdata'] = $this->admin_model->getadmingroup();
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminuser/adminlist',$data);
        $this->load->view('adminuser/inc/footer');



    }

    public function editadmin()
    {


        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }

        if ($this->input->post('save')=="save") {
            if ($this->input->post('status')=="on") {
                $status = 1;
            } else {
                $status = 0;
            }
            date_default_timezone_set("Asia/Bangkok");
            $datainsert = array(
                'firstname' => $this->input->post('firstname'), 
                'lastname' => $this->input->post('lastname'), 
                'user_group_id' => $this->input->post('user_group_id'), 
                'username' => $this->input->post('username'), 
                // 'password' => md5($this->input->post('password')), 
                'email' => $this->input->post('email'), 
                'tel' => $this->input->post('tel'), 
                'lineid' => $this->input->post('lineid'), 
                'status' => $status, 
                // 'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            
            $rs = $this->admin_model->updateadmin($this->input->post('user_id'),$datainsert);
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            redirect('admin/editadmin/'.$this->input->post('user_id'),'refresh');
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            redirect('admin/editadmin/'.$this->input->post('user_id'),'refresh');
            }
            
            
        
        }



        $data['userdatasession'] = $this->session->userdata('userdatasession');
        $data['adminbyiddata'] = $this->admin_model->getadminbyid($this->uri->segment(3));
        $data['admingroupdata'] = $this->admin_model->getadmingroup();
        $this->load->view('adminuser/inc/header',$data);
        $this->load->view('adminuser/editadmin',$data);
        $this->load->view('adminuser/inc/footer');

    }


    public function updatepasswordadmin()
    {

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }

        if ($this->input->post('save')=="save") {
          
            date_default_timezone_set("Asia/Bangkok");
            $datainsert = array(
                'password' => md5($this->input->post('password')), 
                // 'create_date' => date("Y/m/d H:i:s"), 
                'update_date' => date("Y/m/d H:i:s"), 
            );
            $rs = $this->admin_model->updatepasswordadmin($this->input->post('user_id'),$datainsert);
            if ($rs) {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>SUCCESSFULLY</span>
            </div>");
            redirect('admin/editadmin/'.$this->input->post('user_id'),'refresh');
               
            } else {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            redirect('admin/editadmin/'.$this->input->post('user_id'),'refresh');
            }
            $data['userdatasession'] = $this->session->userdata('userdatasession');
            $data['adminbyiddata'] = $this->admin_model->getadminbyid($this->uri->segment(3));
            $data['admingroupdata'] = $this->admin_model->getadmingroup();
            $this->load->view('adminuser/inc/header',$data);
            $this->load->view('adminuser/editadmin',$data);
            $this->load->view('adminuser/inc/footer');

        
        }


    }

    public function deleteadmin()
    {

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }

        $rs = $this->admin_model->deleteadmin($this->uri->segment(3));
            
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
        $url = base_url()."admin/adminlist";
        header("Location: ".$url."");

        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        $url = base_url()."admin/adminlist";
        header("Location: ".$url."");

        }

    }

    public function deleteadmingroup()
    {

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
        $rs = $this->admin_model->deleteadmingroup($this->uri->segment(3));
            
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
        $url = base_url()."admin/admingrouplist";
        header("Location: ".$url."");

        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        $url = base_url()."admin/admingrouplist";
        header("Location: ".$url."");

        }

    }

    public function deleteadminbyselect()
    {

        $user = $this->session->userdata('userdatasession');
            if (!in_array("adminweb",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
        // print_r($this->input->post());
        if (!empty($this->input->post())) {
            $index = array(); 
           foreach ($this->input->post() as $key => $value) {
              $index[] = $value;
           }
           $rs = $this->admin_model->deleteadminbyselect($index);
                
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
        $url = base_url()."admin/adminlist";
        header("Location: ".$url."");

        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        $url = base_url()."admin/adminlist";
        header("Location: ".$url."");

        }

        }else{
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
            </div>");
            $url = base_url()."admin/adminlist";
            header("Location: ".$url."");
        }
        
       

    }
    

    public function clearsession($name)
    {
        $this->session->unset_userdata($name);

    }




}
