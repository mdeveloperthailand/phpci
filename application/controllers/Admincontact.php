<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admincontact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
            parent::__construct();
            if ($this->session->userdata('userdatasession')=="") {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>Please Login</span>
            </div>");
            $url = base_url()."adminlogin/login";
            header("Location: ".$url."");
            }
            
            
            
            
    }
 
 
 public function editcontact()
 {

    $user = $this->session->userdata('userdatasession');
            if (!in_array("contactus",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
    $user = $this->session->userdata('userdatasession');
    if (!in_array("contactus",$user['permission']))
    {
        $url = base_url()."admin";
        header("Location: ".$url."");
    }
     
     if ($this->input->post('save')=="save") {
         if ($this->input->post('contact_status')=="on") {
             $status = 1;
         } else {
             $status = 0;
         }
         $user = $this->session->userdata('userdatasession');
         
         date_default_timezone_set("Asia/Bangkok");
         
         // Image Single Upload
         if (!isset($_FILES['imagecontact']['name'])) {
             $_FILES['imagecontact']['name'] = null;
         }elseif (empty($_FILES['imagecontact']['name'])) {
             $imagecontact = $this->input->post('imagecontactname');
         }else{
             $config['upload_path'] = './image';
             $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
             $config['max_size'] = '2000000';
             // $config['max_width'] = '1024';
             // $config['max_height'] = '1024';
             $config['remove_spaces'] = TRUE;
             $this->load->library("upload",$config);
             if ($this->upload->do_upload('imagecontact')) {
                 // Files Upload Success
                 // var_dump($this->upload->data('file_name'));
             } else {
                 // Files Upload Not Success!!
                 $errors = $this->upload->display_errors();
                 echo $errors;
             } // End else
             if ($this->upload->data('file_name')=="") {
                 $imagecontact = null;
             }else{
                 $imagecontact = $this->upload->data('file_name');
             }
         }
          // Image Single Upload
         
         
         $datainsert = array(
             'contact_image' => $imagecontact, 
             'contact_name_th' => $this->input->post('namethai'), 
             'contact_name_en' => $this->input->post('nameeng'), 
             'contact_address_th' => $this->input->post('addressthai'), 
             'contact_address_en' => $this->input->post('addresseng'), 
             'contact_tel' => $this->input->post('tel'), 
             'contact_time' => $this->input->post('contact_time'), 
             'contact_facebook' => $this->input->post('contact_facebook'), 
             'contact_ig' => $this->input->post('contact_ig'), 
             'contact_line' => $this->input->post('contact_line'), 
             'update_date' => date("Y/m/d H:i:s"), 
         );
         
         $rs = $this->contact_model->updatecontact($datainsert,$this->uri->segment(3));
         
         
         
         if ($rs) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."admincontact/";
             header("Location: ".$url."editcontact");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
         }
         
     }
     
     $data["contactbyiddata"] = $this->contact_model->getcontactbyid(1);
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('admincontact/editcontact',$data);
     $this->load->view('adminuser/inc/footer');
 }

 public function editpageabout()
 {

    $user = $this->session->userdata('userdatasession');
            if (!in_array("pagecontent",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
     
     if ($this->input->post('save')=="save") {
         if ($this->input->post('about_status')=="on") {
             $status = 1;
         } else {
             $status = 0;
         }
         $user = $this->session->userdata('userdatasession');
         
         date_default_timezone_set("Asia/Bangkok");
         
         // Image Single Upload
         if (!isset($_FILES['imagesetpage']['name'])) {
             $_FILES['imagesetpage']['name'] = null;
         }elseif (empty($_FILES['imagesetpage']['name'])) {
             $imagesetpage = $this->input->post('imagesetpagename');
         }else{
             $config['upload_path'] = './image';
             $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
             $config['max_size'] = '2000000';
             // $config['max_width'] = '1024';
             // $config['max_height'] = '1024';
             $config['remove_spaces'] = TRUE;
             $this->load->library("upload",$config);
             if ($this->upload->do_upload('imagesetpage')) {
                 // Files Upload Success
                 // var_dump($this->upload->data('file_name'));
             } else {
                 // Files Upload Not Success!!
                 $errors = $this->upload->display_errors();
                 echo $errors;
             } // End else
             if ($this->upload->data('file_name')=="") {
                 $imagesetpage = null;
             }else{
                 $imagesetpage = $this->upload->data('file_name');
             }
         }
          // Image Single Upload
         
         
         $datainsert = array(
             'setpage_image' => $imagesetpage, 
             'setpage_imagealt' => $this->input->post('setpage_imagealt'), 
             'setpage_h1_th' => $this->input->post('setpage_h1_th'), 
             'setpage_h1_en' => $this->input->post('setpage_h1_en'), 
             'setpage_h2_th' => $this->input->post('setpage_h2_th'), 
             'setpage_h2_en' => $this->input->post('setpage_h2_en'), 
             'setpage_short_th' => $this->input->post('setpage_short_th'), 
             'setpage_short_en' => $this->input->post('setpage_short_en'), 
             'setpage_detail_th' => $this->input->post('setpage_detail_th'), 
             'setpage_detail_en' => $this->input->post('setpage_detail_en'), 
             'setpage_meta_title' => $this->input->post('setpage_meta_title'), 
             'setpage_meta_description' => $this->input->post('setpage_meta_description'), 
             'setpage_meta_keyword' => $this->input->post('setpage_meta_keyword'), 
             'create_date' => date("Y/m/d H:i:s"), 
             'update_date' => date("Y/m/d H:i:s"), 
         );
         
         $rs = $this->contact_model->updatesetpage($datainsert,1);
         
         
         
         if ($rs) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."admincontact/";
             header("Location: ".$url."editpageabout");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
         }
         
     }
     
     $data["setpagebyiddata"] = $this->contact_model->getsetpagebyid(1);
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('admincontact/editpageabout',$data);
     $this->load->view('adminuser/inc/footer');
 }


 public function editpageexclusive()
 {

    $user = $this->session->userdata('userdatasession');
            if (!in_array("pagecontent",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
     
     if ($this->input->post('save')=="save") {
         if ($this->input->post('exclusive_status')=="on") {
             $status = 1;
         } else {
             $status = 0;
         }
         $user = $this->session->userdata('userdatasession');
         
         date_default_timezone_set("Asia/Bangkok");
         
         // Image Single Upload
         if (!isset($_FILES['imagesetpage']['name'])) {
             $_FILES['imagesetpage']['name'] = null;
         }elseif (empty($_FILES['imagesetpage']['name'])) {
             $imagesetpage = $this->input->post('imagesetpagename');
         }else{
             $config['upload_path'] = './image';
             $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
             $config['max_size'] = '2000000';
             // $config['max_width'] = '1024';
             // $config['max_height'] = '1024';
             $config['remove_spaces'] = TRUE;
             $this->load->library("upload",$config);
             if ($this->upload->do_upload('imagesetpage')) {
                 // Files Upload Success
                 // var_dump($this->upload->data('file_name'));
             } else {
                 // Files Upload Not Success!!
                 $errors = $this->upload->display_errors();
                 echo $errors;
             } // End else
             if ($this->upload->data('file_name')=="") {
                 $imagesetpage = null;
             }else{
                 $imagesetpage = $this->upload->data('file_name');
             }
         }
          // Image Single Upload
         
         
         $datainsert = array(
             'setpage_image' => $imagesetpage, 
             'setpage_imagealt' => $this->input->post('setpage_imagealt'), 
             'setpage_h1_th' => $this->input->post('setpage_h1_th'), 
             'setpage_h1_en' => $this->input->post('setpage_h1_en'), 
             'setpage_h2_th' => $this->input->post('setpage_h2_th'), 
             'setpage_h2_en' => $this->input->post('setpage_h2_en'), 
             'setpage_short_th' => $this->input->post('setpage_short_th'), 
             'setpage_short_en' => $this->input->post('setpage_short_en'), 
             'setpage_detail_th' => $this->input->post('setpage_detail_th'), 
             'setpage_detail_en' => $this->input->post('setpage_detail_en'), 
             'setpage_meta_title' => $this->input->post('setpage_meta_title'), 
             'setpage_meta_description' => $this->input->post('setpage_meta_description'), 
             'setpage_meta_keyword' => $this->input->post('setpage_meta_keyword'), 
             'update_date' => date("Y/m/d H:i:s"), 
         );
         
         $rs = $this->contact_model->updatesetpage($datainsert,2);
         
         
         
         if ($rs) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."admincontact/";
             header("Location: ".$url."editpageexclusive");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
         }
         
     }
     
     $data["setpagebyiddata"] = $this->contact_model->getsetpagebyid(2);
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('admincontact/editpageexclusive',$data);
     $this->load->view('adminuser/inc/footer');
 }

 public function editpagestory()
 {
    $user = $this->session->userdata('userdatasession');
    if (!in_array("story",$user['permission']))
    {
        $url = base_url()."admin";
        header("Location: ".$url."");
    }
     if ($this->input->post('save')=="save") {
         if ($this->input->post('story_status')=="on") {
             $status = 1;
         } else {
             $status = 0;
         }
         $user = $this->session->userdata('userdatasession');
         
         date_default_timezone_set("Asia/Bangkok");
         
         // Image Single Upload
         if (!isset($_FILES['imagesetpage']['name'])) {
             $_FILES['imagesetpage']['name'] = null;
         }elseif (empty($_FILES['imagesetpage']['name'])) {
             $imagesetpage = $this->input->post('imagesetpagename');
         }else{
             $config['upload_path'] = './image';
             $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
             $config['max_size'] = '2000000';
             // $config['max_width'] = '1024';
             // $config['max_height'] = '1024';
             $config['remove_spaces'] = TRUE;
             $this->load->library("upload",$config);
             if ($this->upload->do_upload('imagesetpage')) {
                 // Files Upload Success
                 // var_dump($this->upload->data('file_name'));
             } else {
                 // Files Upload Not Success!!
                 $errors = $this->upload->display_errors();
                 echo $errors;
             } // End else
             if ($this->upload->data('file_name')=="") {
                 $imagesetpage = null;
             }else{
                 $imagesetpage = $this->upload->data('file_name');
             }
         }
          // Image Single Upload

         
         
         $datainsert = array(
             'setpage_image' => $imagesetpage, 
             'setpage_imagealt' => $this->input->post('setpage_imagealt'), 
             'setpage_h1_th' => $this->input->post('setpage_h1_th'), 
             'setpage_h1_en' => $this->input->post('setpage_h1_en'), 
             'setpage_h2_th' => $this->input->post('setpage_h2_th'), 
             'setpage_h2_en' => $this->input->post('setpage_h2_en'), 
             'setpage_short_th' => $this->input->post('setpage_short_th'), 
             'setpage_short_en' => $this->input->post('setpage_short_en'), 
             'setpage_detail_th' => $this->input->post('setpage_detail_th'), 
             'setpage_detail_en' => $this->input->post('setpage_detail_en'), 
             'setpage_meta_title' => $this->input->post('setpage_meta_title'), 
             'setpage_meta_description' => $this->input->post('setpage_meta_description'), 
             'setpage_meta_keyword' => $this->input->post('setpage_meta_keyword'), 
             'update_date' => date("Y/m/d H:i:s"), 
         );
         
         $rs = $this->contact_model->updatesetpage($datainsert,3);
         
         
         
         if ($rs) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."admincontact/";
             header("Location: ".$url."editpagestory");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
         }
         
     }
     
     $data["setpagebyiddata"] = $this->contact_model->getsetpagebyid(3);
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('admincontact/editpagestory',$data);
     $this->load->view('adminuser/inc/footer');
 }
 
 
 

}
