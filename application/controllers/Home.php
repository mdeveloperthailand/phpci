<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
            parent::__construct();
            if ($this->session->userdata('lang')=="") {
				$this->session->set_userdata('lang', 1);
				$this->lang->load("message","english"); // โหลดภาษาอังกฤษมาแสดง

            }
             $this->data['logo'] = logob();
    }
	public function index()
	{
		($this->session->userdata('lang')==1)?$this->lang->load("message","thai"):$this->lang->load("message","english");
		$data['homeslidedata'] = $this->home_model->gethomeslide($this->session->userdata('lang'));
		$data['featureproductdata'] = $this->home_model->getfeatureproductall($this->session->userdata('lang'));
		$data['contactdata'] = $this->home_model->getsmegcontact($this->session->userdata('lang'));
		$data['persmegtivedata'] = $this->home_model->getpersmegtive($this->session->userdata('lang'));
		$data['smegstorydata'] = $this->home_model->getsmeg(3,$this->session->userdata('lang'));
		$data['smegreviewdata'] = $this->home_model->getsmegreview($this->session->userdata('lang'));
		$data['newsdata'] = $this->home_model->getnewslimitthree($this->session->userdata('lang'));
		$this->load->view('inc/header',$this->data);
		$this->load->view('home',$data);
		$this->load->view('inc/footer',$data);
	}

	public function landing()
	{
		$this->load->view('landing');
	}

	public function changelang()
	{
		if ($this->session->userdata('lang')==1) {
			$this->session->set_userdata('lang', 2);
		}else{
			$this->session->set_userdata('lang', 1);
		}
		echo "<script>window.history.back();</script>";
	}


	public function about()
	{
		($this->session->userdata('lang')==1)?$this->lang->load("message","thai"):$this->lang->load("message","english");
		$data['contactdata'] = $this->home_model->getsmegcontact($this->session->userdata('lang'));
		$data['aboutdata'] = $this->home_model->getsetpage(1,$this->session->userdata('lang'));
		$this->load->view('inc/header',$this->data);
		$this->load->view('aboutsmeg',$data);
		$this->load->view('inc/footer',$data);
	}

	public function exclusive()
	{
		($this->session->userdata('lang')==1)?$this->lang->load("message","thai"):$this->lang->load("message","english");
		$data['contactdata'] = $this->home_model->getsmegcontact($this->session->userdata('lang'));
		$data['exclusivedata'] = $this->home_model->getsetpage(2,$this->session->userdata('lang'));
		$this->load->view('inc/header',$this->data);
		$this->load->view('exclusivesmeg',$data);
		$this->load->view('inc/footer',$data);
	}

	public function story()
	{
		($this->session->userdata('lang')==1)?$this->lang->load("message","thai"):$this->lang->load("message","english");
		$data['contactdata'] = $this->home_model->getsmegcontact($this->session->userdata('lang'));
		$data['storydata'] = $this->home_model->getsetpage(3,$this->session->userdata('lang'));
		$this->load->view('inc/header',$this->data);
		$this->load->view('storysmeg',$data);
		$this->load->view('inc/footer',$data);
	}

	public function collectdata()
	{
		$rs = $this->home_model->addstayconnect($_POST['email']);
		if ($rs==200) {

			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'mdeveloperthailand@gmail.com',
				'smtp_pass' => 'DEWdew@@1533',
				'mailtype'  => 'html', 
				'charset'   => 'utf-8'
			);
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");

			$this->email->from('c.panuwong@gmail.com', 'TESTER');
			$this->email->to('c.panuwong@gmail.com'); 
	
			$this->email->subject('Email Test');
			$this->email->message('Testing the email class.');  
	
			$this->email->send();
				
		}
		echo $rs;
	}


	public function uploadsummernote()
	{
		if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $name = md5(rand(100, 200));
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];
                $destination = $_SERVER['DOCUMENT_ROOT']."/dev/image/" . $filename; //change this directory
                $location = $_FILES["file"]["tmp_name"];
                move_uploaded_file($location, $destination);
                echo 'http://smeg.co.th/dev/image/' . $filename;//change this URL
            }
            else
            {
              echo  $message = 'Ooops!  Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
        }
	}
	

}
