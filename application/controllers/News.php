<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."news/index";
        $config['total_rows'] =   $this->news_model->count_blogall();//here we will count all the data from the table
        $config['per_page'] = 5;//number of data to be shown on single page
        $config["uri_segment"] = 3;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center" width="100%"><nav style="margin-left:40%"><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(3)){
            $page = ($this->uri->segment(3)) ;
        }
        else{
            $page = 0;
        }
		$data['newsbycategoryiddata'] = $this->news_model->getnewsallpag($config["per_page"],$page,$this->session->userdata('lang'));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
		// ================pagination====================
        ($this->session->userdata('lang')==1)?$this->lang->load("message","thai"):$this->lang->load("message","english");
		$data['contactdata'] = $this->home_model->getsmegcontact($this->session->userdata('lang'));
		$data['categorydata'] = $this->news_model->getnewscategoryall($this->session->userdata('lang'));
        $data['logo'] = logob();
		$this->load->view('inc/header',$data);
		$this->load->view('news',$data);
		$this->load->view('inc/footer',$data);
	}

	public function newscategory()
	{
		// ================pagination====================
        $config = array();
        $config["base_url"] = base_url()."news/newscategory/".$this->uri->segment(3)."/";
        $config['total_rows'] =   $this->news_model->count_blog($this->uri->segment(3));//here we will count all the data from the table
        $config['per_page'] = 5;//number of data to be shown on single page
        $config["uri_segment"] = 4;
        /* EDIT LINK *  */
        $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close'] 	= '</ul></nav></div>';
        $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] 	= '</span></li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] 	= '</span></li>';
        $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        if($this->uri->segment(4)){
            $page = ($this->uri->segment(4)) ;
        }
        else{
            $page = 0;
        }
		$data['newsbycategoryiddata'] = $this->news_model->getnewsbycategoryid($config["per_page"],$page,$this->uri->segment(3),$this->session->userdata('lang'));
        $data["links"] = $this->pagination->create_links();//create the link for pagination
		// ================pagination====================
		($this->session->userdata('lang')==1)?$this->lang->load("message","thai"):$this->lang->load("message","english");
		$data['contactdata'] = $this->home_model->getsmegcontact($this->session->userdata('lang'));
		$data['categorydata'] = $this->news_model->getnewscategoryall($this->session->userdata('lang'));
        $data['logo'] = logob();
		$this->load->view('inc/header',$data);
		$this->load->view('news',$data);
		$this->load->view('inc/footer',$data);
	}

	public function newsdetail()
	{
		$data['contactdata'] = $this->home_model->getsmegcontact($this->session->userdata('lang'));
		$data['newsbyblogiddata'] = $this->news_model->getnewsbyblogid($this->uri->segment(3),$this->session->userdata('lang'));
        ($this->session->userdata('lang')==1)?$this->lang->load("message","thai"):$this->lang->load("message","english");
        $data['logo'] = logob();
		$this->load->view('inc/header',$data);
		$this->load->view('newsdetail',$data);
		$this->load->view('inc/footer',$data);
	}
}
