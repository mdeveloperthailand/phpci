<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminpersmegtive extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
            parent::__construct();
            if ($this->session->userdata('userdatasession')=="") {
                $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                <span class='font-weight-semibold'>Please Login</span>
            </div>");
            $url = base_url()."adminlogin/login";
            header("Location: ".$url."");
            }

            $user = $this->session->userdata('userdatasession');
            if (!in_array("persmegtive",$user['permission']))
            {
                $url = base_url()."admin";
                header("Location: ".$url."");
            }
            
            
    }

 //  persmegtive
 public function persmegtivelist()
 {
     $config = array();
     $config["base_url"] = base_url()."adminpersmegtive/persmegtivelist/";
     $config['total_rows'] =   $this->db->count_all("persmegtive");//here we will count all the data from the table
     $config['per_page'] = 10;//number of data to be shown on single page
     $config["uri_segment"] = 3;
     /* EDIT LINK *  */
     $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
     $config['full_tag_close'] 	= '</ul></nav></div>';
     $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['num_tag_close'] 	= '</span></li>';
     $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
     $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
     $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
     $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['prev_tagl_close'] 	= '</span></li>';
     $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['first_tagl_close'] = '</span></li>';
     $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['last_tagl_close'] 	= '</span></li>';
     $this->pagination->initialize($config);
     if($this->uri->segment(3)){
         $page = ($this->uri->segment(3)) ;
     }
     else{
         $page = 1;
     }
     $data["persmegtivelistdata"] = $this->persmegtive_model->getpersmegtiveall($config["per_page"], $this->uri->segment(3));
     $data["links"] = $this->pagination->create_links();//create the link for pagination
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('adminpersmegtive/persmegtivelist',$data);
     $this->load->view('adminuser/inc/footer');
     
 }
 
 
 public function addpersmegtive()
 {
     
     if ($this->input->post('save')=="save") {
         if ($this->input->post('persmegtive_status')=="on") {
             $status = 1;
         } else {
             $status = 0;
         }
         $user = $this->session->userdata('userdatasession');
         
         date_default_timezone_set("Asia/Bangkok");
         
         if (!isset($_FILES['imagepersmegtive']['name'])) {
             $_FILES['imagepersmegtive']['name'] = null;
         }elseif (empty($_FILES['imagepersmegtive']['name'])) {
             $imagepersmegtive = $this->input->post('imagepersmegtivename');
         }else{
             $config['upload_path'] = './image/persmegtive';
             $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
             $config['max_size'] = '2000000';
             // $config['max_width'] = '1024';
             // $config['max_height'] = '1024';
             $config['remove_spaces'] = TRUE;
             $this->load->library("upload",$config);
             if ($this->upload->do_upload('imagepersmegtive')) {
                 // Files Upload Success
                 // var_dump($this->upload->data('file_name'));
             } else {
                 // Files Upload Not Success!!
                 $errors = $this->upload->display_errors();
                 echo $errors;
             } // End else
             if ($this->upload->data('file_name')=="") {
                 $imagepersmegtive = null;
             }else{
                 $imagepersmegtive = $this->upload->data('file_name');
             }
         }
         
         
         $datainsert = array(
             'persmegtive_image' => $imagepersmegtive, 
             'persmegtive_name_th' => $this->input->post('namethai'), 
             'persmegtive_name_en' => $this->input->post('nameeng'), 
             'persmegtive_detail_th' => $this->input->post('detailthai'), 
             'persmegtive_detail_en' => $this->input->post('detaileng'), 
             'persmegtive_imagealt' => $this->input->post('persmegtive_imagealt'), 
             'persmegtive_meta_title' => $this->input->post('persmegtive_meta_title'), 
             'persmegtive_meta_description' => $this->input->post('persmegtive_meta_description'), 
             'persmegtive_meta_keyword' => $this->input->post('persmegtive_meta_keyword'), 
             'persmegtive_order' => $this->input->post('persmegtive_order'), 
             'persmegtive_status' => $status, 
             'create_date' => date("Y/m/d H:i:s"), 
             'update_date' => date("Y/m/d H:i:s"), 
         );
         
         $rs = $this->persmegtive_model->addpersmegtive($datainsert);
         
         
         
         if ($rs) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."adminpersmegtive/";
             header("Location: ".$url."persmegtivelist");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
         }
         
     }
     
     
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('adminpersmegtive/addpersmegtive',$data);
     $this->load->view('adminuser/inc/footer');
 }
 
 public function updatestatuspersmegtive()
 {
     if ($this->input->post('submit')=="Update Status") {
         
         date_default_timezone_set("Asia/Bangkok");
         if ($this->input->post("checkboxstatus")!="") {
             $datainsert = array(
                 'persmegtive_status' => $this->input->post('persmegtive_status'), 
                 'update_date' => date("Y/m/d H:i:s")
             );
             
             $rs = $this->persmegtive_model->updatestatuspersmegtive($this->input->post("checkboxstatus"),$datainsert);
             
             if ($rs==200) {
                 $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                 <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                 <span class='font-weight-semibold'>SUCCESSFULLY</span>
                 </div>");
                 $url = base_url()."adminpersmegtive/persmegtivelist";
                 header("Location: ".$url."");
             } else {
                 $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
                 <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
                 <span class='font-weight-semibold'>NO SUCCESS</span>
                 </div>");
             }
             
         }
         
         
     }elseif ($this->input->post('orderbtn')=="Update Order") {

        date_default_timezone_set("Asia/Bangkok");
        
        // $index = array();
        // foreach ($this->input->post("persmegtive_order") as $key => $value) {
        //     $index[] = $key; 
        // }

        $rs = $this->persmegtive_model->updatepersmegtiveorder($this->input->post("order"));
        
        if ($rs==200) {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>SUCCESSFULLY</span>
        </div>");
           
        } else {
            $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
            <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
            <span class='font-weight-semibold'>NO SUCCESS</span>
        </div>");
        }
    
        
                
    }
     
     // ================pagination====================
     $config = array();
     $config["base_url"] = base_url()."adminpersmegtive/persmegtivelist/";
     $config['total_rows'] =   $this->db->count_all("persmegtive");//here we will count all the data from the table
     $config['per_page'] = 10;//number of data to be shown on single page
     $config["uri_segment"] = 3;
     /* EDIT LINK *  */
     $config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
     $config['full_tag_close'] 	= '</ul></nav></div>';
     $config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['num_tag_close'] 	= '</span></li>';
     $config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
     $config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
     $config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
     $config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['prev_tagl_close'] 	= '</span></li>';
     $config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['first_tagl_close'] = '</span></li>';
     $config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
     $config['last_tagl_close'] 	= '</span></li>';
     $this->pagination->initialize($config);
     if($this->uri->segment(3)){
         $page = ($this->uri->segment(3)) ;
     }
     else{
         $page = 1;
     }
     $data["persmegtivelistdata"] = $this->persmegtive_model->getpersmegtiveallupdatestatus($config["per_page"], $page);
     $data["links"] = $this->pagination->create_links();//create the link for pagination
     // ================pagination====================
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('adminpersmegtive/persmegtivelist',$data);
     $this->load->view('adminuser/inc/footer');
     
     
 }
 
 public function deletepersmegtive()
 {
     $rs = $this->persmegtive_model->deletepersmegtive($this->uri->segment(3));
     
     if ($rs==200) {
         $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
         <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
         <span class='font-weight-semibold'>SUCCESSFULLY</span>
         </div>");
         $url = base_url()."adminpersmegtive/persmegtivelist";
         header("Location: ".$url."");
         
     } else {
         $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
         <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
         <span class='font-weight-semibold'>NO SUCCESS</span>
         </div>");
         $url = base_url()."adminpersmegtive/persmegtivelist";
         header("Location: ".$url."");
         
     }
     
 }
 
 public function deletepersmegtivebyselect()
 {
     // print_r($this->input->post());
     if (!empty($this->input->post())) {
         $index = array(); 
         foreach ($this->input->post() as $key => $value) {
             $index[] = $value;
         }
         $rs = $this->persmegtive_model->deletepersmegtivebyselect($index);
         
         if ($rs==200) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."adminpersmegtive/persmegtivelist";
             header("Location: ".$url."");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
             $url = base_url()."adminpersmegtive/persmegtivelist";
             header("Location: ".$url."");
             
         }
         
     }else{
         $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
         <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
         <span class='font-weight-semibold'>NO SUCCESS</span>
         </div>");
         $url = base_url()."adminpersmegtive/persmegtivelist";
         header("Location: ".$url."");
     }
     
     
     
 }
 
 
 
 public function editpersmegtive()
 {
     
     if ($this->input->post('save')=="save") {
         if ($this->input->post('persmegtive_status')=="on") {
             $status = 1;
         } else {
             $status = 0;
         }
         $user = $this->session->userdata('userdatasession');
         
         date_default_timezone_set("Asia/Bangkok");
         
         // Image Single Upload
         if (!isset($_FILES['imagepersmegtive']['name'])) {
             $_FILES['imagepersmegtive']['name'] = null;
         }elseif (empty($_FILES['imagepersmegtive']['name'])) {
             $imagepersmegtive = $this->input->post('imagepersmegtivename');
         }else{
             $config['upload_path'] = './image/persmegtive';
             $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
             $config['max_size'] = '2000000';
             // $config['max_width'] = '1024';
             // $config['max_height'] = '1024';
             $config['remove_spaces'] = TRUE;
             $this->load->library("upload",$config);
             if ($this->upload->do_upload('imagepersmegtive')) {
                 // Files Upload Success
                 // var_dump($this->upload->data('file_name'));
             } else {
                 // Files Upload Not Success!!
                 $errors = $this->upload->display_errors();
                 echo $errors;
             } // End else
             if ($this->upload->data('file_name')=="") {
                 $imagepersmegtive = null;
             }else{
                 $imagepersmegtive = $this->upload->data('file_name');
             }
         }
          // Image Single Upload
         
         
         $datainsert = array(
            'persmegtive_image' => $imagepersmegtive, 
            'persmegtive_name_th' => $this->input->post('namethai'), 
            'persmegtive_name_en' => $this->input->post('nameeng'), 
            'persmegtive_detail_th' => $this->input->post('detailthai'), 
            'persmegtive_detail_en' => $this->input->post('detaileng'), 
            'persmegtive_imagealt' => $this->input->post('persmegtive_imagealt'), 
            'persmegtive_meta_title' => $this->input->post('persmegtive_meta_title'), 
            'persmegtive_meta_description' => $this->input->post('persmegtive_meta_description'), 
            'persmegtive_meta_keyword' => $this->input->post('persmegtive_meta_keyword'), 
            'persmegtive_order' => $this->input->post('persmegtive_order'), 
            'persmegtive_status' => $status, 
             'update_date' => date("Y/m/d H:i:s"), 
         );
         
         $rs = $this->persmegtive_model->updatepersmegtive($datainsert,$this->uri->segment(3));
         
         
         
         if ($rs) {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>SUCCESSFULLY</span>
             </div>");
             $url = base_url()."adminpersmegtive/";
             header("Location: ".$url."persmegtivelist");
             
         } else {
             $this->session->set_flashdata('message',"<div class='alert bg-primary text-white alert-styled-left alert-dismissible'>
             <button type='button' class='close' data-dismiss='alert'><span>×</span></button>
             <span class='font-weight-semibold'>NO SUCCESS</span>
             </div>");
         }
         
     }
     
     $data["persmegtivebyiddata"] = $this->persmegtive_model->getpersmegtivebyid($this->uri->segment(3));
     $data['userdatasession'] = $this->session->userdata('userdatasession');
     $this->load->view('adminuser/inc/header',$data);
     $this->load->view('adminpersmegtive/editpersmegtive',$data);
     $this->load->view('adminuser/inc/footer');
 }
 
 
 //  persmegtive END

}
