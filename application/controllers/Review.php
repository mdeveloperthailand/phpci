<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
            parent::__construct();
            if ($this->session->userdata('lang')=="") {
				$this->session->set_userdata('lang', 1);
            }
    }
	public function reviewdetail()
	{
		$data['contactdata'] = $this->home_model->getsmegcontact($this->session->userdata('lang'));
		$data['smegreviewdata'] = $this->review_model->getreviewdetailbyid($this->uri->segment(3),$this->session->userdata('lang'));
		($this->session->userdata('lang')==1)?$this->lang->load("message","thai"):$this->lang->load("message","english");
		$data['logo'] = logob();
		$this->load->view('inc/header',$data);
		$this->load->view('reviewdetail',$data);
		$this->load->view('inc/footer',$data);
	}

	

}
