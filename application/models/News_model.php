<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function count_blogall()
	{   
       $this->db->where('blog.blog_status', 1);
        $query = $this->db->get('blog');
		return $query->num_rows();
    }
	public function getnewsall($lang)
	{   
        $this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
        return $query->result_array();
    }

    public function getblogtypeall($lang)
	{   
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $query = $this->db->get('blog_type');
        return $query->result_array();
    }

    public function getnewscategoryall($lang)
	{   
		// $this->db->where('blog_category.blog_category_status', 1);
        $this->db->where('blog_category_description.language_id', $lang);
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $query = $this->db->get('blog_category');
        return $query->result_array();
	}
	public function count_blog($blog_category_id)
	{
		$this->db->where('blog.blog_category_id', $blog_category_id);
        $query = $this->db->get('blog');
		return $query->num_rows();
	}

	
	public function getnewsbycategoryid($limit,$start,$blog_category_id,$lang)
	{
		$this->db->limit($limit, $start);
		$this->db->order_by('blog.blog_order','ASC');
		$this->db->where('blog.blog_status', 1);
		$this->db->where('blog.blog_category_id', $blog_category_id);
		$this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
		return $query->result_array();
		
	}

	public function getnewsallpag($limit,$start,$lang)
	{
		$this->db->limit($limit, $start);
		$this->db->order_by('blog.blog_order','ASC');
		$this->db->where('blog.blog_status', 1);
		$this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
		return $query->result_array();
		
	}

	public function getnewsalladmin($limit,$start,$lang)
	{
		$this->db->limit($limit, $start);
		$this->db->order_by('blog.blog_order','ASC');
		$this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
		return $query->result_array();
		
	}

	public function getnewsbyblogid($blog_id,$lang)
	{
		$this->db->where('blog.blog_id', $blog_id);
		$this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
		return $query->row_array();
		
	}
    
    public function updatestatusnews($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('blog_id', $index);
		$this->db->update('blog', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
	
	public function updatestatusnewscategory($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('blog_category_id', $index);
		$this->db->update('blog_category', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function updatenewsorder($data,$index)
    {
       
        $this->db->trans_start();
        foreach ($data as $key => $value) {
        	if ($value==""||$value==0) {
        		$this->db->set('blog_order', 99);
	            $this->db->where('blog_id', $key);
	            $this->db->update('blog');
        	}else{
            $this->db->set('blog_order', $value);
            $this->db->where('blog_id', $key);
            $this->db->update('blog');
        	}
        }
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
    }
    
    public function deletenews($index)
    {
		$this->db->trans_start();
		$this->db->where('blog_id', $index);
        $this->db->delete('blog');
        $this->db->where('blog_id', $index);
		$this->db->delete('blog_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }
    
    public function deletenewsbyselect($index)
    {
		$this->db->trans_start();
		$this->db->where_in('blog_id', $index);
        $this->db->delete('blog');
        $this->db->where_in('blog_id', $index);
		$this->db->delete('blog_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
	
	public function deletenewscategory($index)
    {
		$this->db->trans_start();
		$this->db->where('blog_category_id', $index);
        $this->db->delete('blog_category');
        $this->db->where('blog_category_id', $index);
		$this->db->delete('blog_category_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }
    
    public function deletenewscategorybyselect($index)
    {
		$this->db->trans_start();
		$this->db->where_in('blog_category_id', $index);
        $this->db->delete('blog_category');
        $this->db->where_in('blog_category_id', $index);
		$this->db->delete('blog_category_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }


    public function addnews($data)
	{
        $query = $this->db->insert('blog',$data);
      	$id = $this->db->insert_id();
		return $id;
	}
	
	public function addnewscategory($data)
	{
        $query = $this->db->insert('blog_category',$data);
      	$id = $this->db->insert_id();
		return $id;
    }
    
    public function updatenewsdescription($datathai,$dataeng,$index)
	{
        $this->db->trans_start();
			$this->db->where('blog_id', $index);
			$this->db->where('language_id', 1);
			$this->db->update('blog_description',$datathai);
			
			$this->db->where('blog_id', $index);
			$this->db->where('language_id', 2);
            $this->db->update('blog_description',$dataeng);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
	}

	public function updatenewscategorydescription($datathai,$dataeng,$index)
	{
        $this->db->trans_start();
			$this->db->where('blog_category_id', $index);
			$this->db->where('language_id', 1);
			$this->db->update('blog_category_description',$datathai);
			
			$this->db->where('blog_category_id', $index);
			$this->db->where('language_id', 2);
            $this->db->update('blog_category_description',$dataeng);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
	}

	public function updatenews($data,$index)
	{
        $this->db->trans_start();
            $this->db->where('blog_id', $index);
            $this->db->update('blog',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
	}
	public function updatenewscategory($data,$index)
	{
        $this->db->trans_start();
            $this->db->where('blog_category_id', $index);
            $this->db->update('blog_category',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
    }
    
    public function addnewsdescription($datathai,$dataeng)
	{
        $query = $this->db->insert('blog_description',$datathai);
        $query = $this->db->insert('blog_description',$dataeng);
        return ($this->db->affected_rows() != 1) ? false : true;
	}
	public function addnewscategorydescription($datathai,$dataeng)
	{
        $query = $this->db->insert('blog_category_description',$datathai);
        $query = $this->db->insert('blog_category_description',$dataeng);
        return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getnewsbyid($lang,$blog_id)
	{
		$this->db->where('blog.blog_id', $blog_id);
		$this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
        return $query->row_array();
	}

	public function getnewscategorybyid($lang,$blog_id)
	{
		
		$this->db->where('blog_category_description.language_id', $lang);
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $query = $this->db->get('blog_category');
        return $query->row_array();
	}
	
    







}
