
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeslide_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

   // homeslide
   public function gethomeslideall()
   {

       $query = $this->db->get('homeslide');
       return $query->result_array();

   }

   public function addhomeslide($datainsert)
   {
       $query = $this->db->insert('homeslide',$datainsert);
       if($this->db->affected_rows() == 1){
           return TRUE;
       }else{
           return FALSE;
       }
   }


   public function deletehomeslide($index)
   {
       $this->db->trans_start();
       $this->db->where('homeslide_id', $index);
       $this->db->delete('homeslide');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function deletehomeslidebyselect($index)
   {
       $this->db->trans_start();
       $this->db->where_in('homeslide_id', $index);
       $this->db->delete('homeslide');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }


   public function updatestatushomeslide($index,$dataupdate)
   {
       $this->db->trans_start();
       $this->db->where_in('homeslide_id', $index);
       $this->db->update('homeslide', $dataupdate);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function gethomeslideallupdatestatus($limit,$start)
   {

       $query = $this->db->get('homeslide');

       if($query->num_rows()>0)
       {
           return $query->result_array();
       }
       else 
       {
           return false;
       }

       
   }

   public function gethomeslidebyid($index)
   {

       $this->db->where('homeslide_id', $index);
       $query = $this->db->get('homeslide');
       return $query->row_array();
       
   }

   public function updatehomeslide($data,$index)
   {
       $this->db->trans_start();
       $this->db->where('homeslide_id', $index);
       $this->db->update('homeslide',$data);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   public function updatehomeslideorder($data)
   {
      
       $this->db->trans_start();
       foreach ($data as $key => $value) {
           $this->db->set('homeslide_order', $value);
           $this->db->where('homeslide_id', $key);
           $this->db->update('homeslide');
       }
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   // homeslide END

	 







}
