
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persmegtive_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

   // persmegtive
   public function getpersmegtiveall()
   {

       $query = $this->db->get('persmegtive');
       return $query->result_array();

   }

   public function addpersmegtive($datainsert)
   {
       $query = $this->db->insert('persmegtive',$datainsert);
       if($this->db->affected_rows() == 1){
           return TRUE;
       }else{
           return FALSE;
       }
   }


   public function deletepersmegtive($index)
   {
       $this->db->trans_start();
       $this->db->where('persmegtive_id', $index);
       $this->db->delete('persmegtive');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function deletepersmegtivebyselect($index)
   {
       $this->db->trans_start();
       $this->db->where_in('persmegtive_id', $index);
       $this->db->delete('persmegtive');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }


   public function updatestatuspersmegtive($index,$dataupdate)
   {
       $this->db->trans_start();
       $this->db->where_in('persmegtive_id', $index);
       $this->db->update('persmegtive', $dataupdate);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function getpersmegtiveallupdatestatus($limit,$start)
   {

       $query = $this->db->get('persmegtive');

       if($query->num_rows()>0)
       {
           return $query->result_array();
       }
       else 
       {
           return false;
       }

       
   }

   public function getpersmegtivebyid($index)
   {

       $this->db->where('persmegtive_id', $index);
       $query = $this->db->get('persmegtive');
       return $query->row_array();
       
   }

   public function updatepersmegtive($data,$index)
   {
       $this->db->trans_start();
       $this->db->where('persmegtive_id', $index);
       $this->db->update('persmegtive',$data);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   public function updatepersmegtiveorder($data)
   {
      
       $this->db->trans_start();
       foreach ($data as $key => $value) {
           $this->db->set('persmegtive_order', $value);
           $this->db->where('persmegtive_id', $key);
           $this->db->update('persmegtive');
       }
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   // persmegtive END

	 
   public function getpersmegtivedetailbyid($index,$lang)
   {   

       if ($lang==1) {
           $this->db->select("persmegtive_id,persmegtive_image,persmegtive_imagealt,persmegtive_name_th as persmegtive_name,persmegtive_detail_th as persmegtive_detail,persmegtive_order,persmegtive_meta_title,persmegtive_meta_description,persmegtive_meta_keyword");
           $this->db->from('persmegtive');
           $this->db->where('persmegtive_status', 1);
           $this->db->where('persmegtive_id', $index);

       }else{
           $this->db->select("persmegtive_id,persmegtive_image,persmegtive_imagealt,persmegtive_name_en as persmegtive_name,persmegtive_detail_en as persmegtive_detail,persmegtive_order,persmegtive_meta_title,persmegtive_meta_description,persmegtive_meta_keyword");
           $this->db->from('persmegtive');
           $this->db->where('persmegtive_status', 1);
           $this->db->where('persmegtive_id', $index);
       }

       $query = $this->db->get();
       return $query->row_array();
   }






}
