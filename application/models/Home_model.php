<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    

    public function getnewslimitthree($lang)
	{   
        $this->db->limit(3);
        $this->db->order_by('blog.blog_order','ASC');
        $this->db->where('blog.blog_status', 1);
        $this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
        return $query->result_array();
    }

    public function getsmegreview($lang)
	{   

        if ($lang==1) {
            $this->db->select("review_id,review_image,review_imagealt,review_name_th as review_name,review_short_th as review_short,review_detail_th as review_detail,review_order,review_meta_title,review_meta_description,review_meta_keyword");
            $this->db->from('review');
            $this->db->where('review_status', 1);
        }else{
            $this->db->select("review_id,review_image,review_imagealt,review_name_en as review_name,review_short_en as review_short,review_detail_en as review_detail,review_order,review_meta_title,review_meta_description,review_meta_keyword");
            $this->db->from('review');
            $this->db->where('review_status', 1);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getsmeg($index,$lang)
	{   

        if ($lang==1) {
            $this->db->select("setpage_id,setpage_image,setpage_imagealt,setpage_h1_th as setpage_h1,setpage_h2_th as setpage_h2,setpage_short_th as setpage_short,setpage_detail_th as setpage_detail,setpage_meta_title,setpage_meta_description,setpage_meta_keyword");
            $this->db->from('setpage');
            $this->db->where('setpage_id', $index);
        }else{
            $this->db->select("setpage_id,setpage_image,setpage_imagealt,setpage_h1_en as setpage_h1,setpage_h2_en as setpage_h2,setpage_short_en as setpage_short,setpage_detail_en as setpage_detail,setpage_meta_title,setpage_meta_description,setpage_meta_keyword");
            $this->db->from('setpage');
            $this->db->where('setpage_id', $index);

        }

        $query = $this->db->get();
        return $query->row_array();
	}
	
	public function getfeatureproductall($lang)
	{   

        if ($lang==1) {
            $this->db->select("feature_id,feature_image,feature_imagealt,feature_h1_th as feature_h1,feature_h2_th as feature_h2,feature_h3_th as feature_h3,feature_link,feature_meta_title,feature_meta_description,feature_meta_keyword");
            $this->db->from('feature_product');
        }else{
            $this->db->select("feature_id,feature_image,feature_imagealt,feature_h1_en as feature_h1,feature_h2_en as feature_h2,feature_h3_en as feature_h3,feature_link,,feature_meta_title,feature_meta_description,feature_meta_keyword");
            $this->db->from('feature_product');

        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getpersmegtive($lang)
	{   

        if ($lang==1) {
            $this->db->select("persmegtive_id,persmegtive_image,persmegtive_imagealt,persmegtive_name_th as persmegtive_name,persmegtive_detail_th as persmegtive_detail,persmegtive_order,persmegtive_meta_title,persmegtive_meta_description,persmegtive_meta_keyword");
            $this->db->from('persmegtive');
            $this->db->where('persmegtive_status', 1);
            $this->db->order_by('persmegtive_order', 'ASC');
            $this->db->limit(6);

        }else{
            $this->db->select("persmegtive_id,persmegtive_image,persmegtive_imagealt,persmegtive_name_en as persmegtive_name,persmegtive_detail_en as persmegtive_detail,persmegtive_order,persmegtive_meta_title,persmegtive_meta_description,persmegtive_meta_keyword");
            $this->db->from('persmegtive');
            $this->db->where('persmegtive_status', 1);
            $this->db->order_by('persmegtive_order', 'ASC');
            $this->db->limit(6);
        }

        $query = $this->db->get();
        return $query->result_array();
    }
    public function getsmegcontact($lang)
	{   

        if ($lang==1) {
            $this->db->select("contact_id,contact_image,contact_name_th as contact_name,contact_address_th as contact_address,contact_tel,contact_time,contact_facebook,contact_ig,contact_line");
            $this->db->from('contact');
        }else{
            $this->db->select("contact_id,contact_image,contact_name_en as contact_name,contact_address_en as contact_address,contact_tel,contact_time,contact_facebook,contact_ig,contact_line");
            $this->db->from('contact');
        }

        $query = $this->db->get();
        return $query->row_array();
    }











	public function getnewsall($lang)
	{   
        $this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
        return $query->result_array();
    }

    public function getblogtypeall($lang)
	{   
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $query = $this->db->get('blog_type');
        return $query->result_array();
    }

    public function getnewscategoryall($lang)
	{   
		// $this->db->where('blog_category.blog_category_status', 1);
        $this->db->where('blog_category_description.language_id', $lang);
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $query = $this->db->get('blog_category');
        return $query->result_array();
    }
    
    public function updatestatusnews($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('blog_id', $index);
		$this->db->update('blog', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
	
	public function updatestatusnewscategory($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('blog_category_id', $index);
		$this->db->update('blog_category', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function updatenewsorder($data,$index)
    {
       
        $this->db->trans_start();
        foreach ($data as $key => $value) {
            $this->db->set('blog_order', $value);
            $this->db->where('blog_id', $key);
            $this->db->update('blog');
        }
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
    }
    
    public function deletenews($index)
    {
		$this->db->trans_start();
		$this->db->where('blog_id', $index);
        $this->db->delete('blog');
        $this->db->where('blog_id', $index);
		$this->db->delete('blog_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }
    
    public function deletenewsbyselect($index)
    {
		$this->db->trans_start();
		$this->db->where_in('blog_id', $index);
        $this->db->delete('blog');
        $this->db->where_in('blog_id', $index);
		$this->db->delete('blog_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
	
	public function deletenewscategory($index)
    {
		$this->db->trans_start();
		$this->db->where('blog_category_id', $index);
        $this->db->delete('blog_category');
        $this->db->where('blog_category_id', $index);
		$this->db->delete('blog_category_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }
    
    public function deletenewscategorybyselect($index)
    {
		$this->db->trans_start();
		$this->db->where_in('blog_category_id', $index);
        $this->db->delete('blog_category');
        $this->db->where_in('blog_category_id', $index);
		$this->db->delete('blog_category_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }


    public function addnews($data)
	{
        $query = $this->db->insert('blog',$data);
      	$id = $this->db->insert_id();
		return $id;
	}
	
	public function addnewscategory($data)
	{
        $query = $this->db->insert('blog_category',$data);
      	$id = $this->db->insert_id();
		return $id;
    }
    
    public function updatenewsdescription($datathai,$dataeng,$index)
	{
        $this->db->trans_start();
			$this->db->where('blog_id', $index);
			$this->db->where('language_id', 1);
			$this->db->update('blog_description',$datathai);
			
			$this->db->where('blog_id', $index);
			$this->db->where('language_id', 2);
            $this->db->update('blog_description',$dataeng);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
	}

	public function updatenewscategorydescription($datathai,$dataeng,$index)
	{
        $this->db->trans_start();
			$this->db->where('blog_category_id', $index);
			$this->db->where('language_id', 1);
			$this->db->update('blog_category_description',$datathai);
			
			$this->db->where('blog_category_id', $index);
			$this->db->where('language_id', 2);
            $this->db->update('blog_category_description',$dataeng);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
	}

	public function updatenews($data,$index)
	{
        $this->db->trans_start();
            $this->db->where('blog_id', $index);
            $this->db->update('blog',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
	}
	public function updatenewscategory($data,$index)
	{
        $this->db->trans_start();
            $this->db->where('blog_category_id', $index);
            $this->db->update('blog_category',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
    }
    
    public function addnewsdescription($datathai,$dataeng)
	{
        $query = $this->db->insert('blog_description',$datathai);
        $query = $this->db->insert('blog_description',$dataeng);
        return ($this->db->affected_rows() != 1) ? false : true;
	}
	public function addnewscategorydescription($datathai,$dataeng)
	{
        $query = $this->db->insert('blog_category_description',$datathai);
        $query = $this->db->insert('blog_category_description',$dataeng);
        return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getnewsbyid($lang,$blog_id)
	{
		$this->db->where('blog.blog_id', $blog_id);
		$this->db->where('blog_category_description.language_id', $lang);
        $this->db->where('blog_description.language_id', $lang);
        $this->db->where('blog_type_description.language_id', $lang);
        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');
        $query = $this->db->get('blog');
        return $query->row_array();
	}

	public function getnewscategorybyid($lang,$blog_id)
	{
		
		$this->db->where('blog_category_description.language_id', $lang);
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $query = $this->db->get('blog_category');
        return $query->row_array();
	}

	public function gethomeslide($lang)
	{   
    
        $this->db->order_by('homeslide_order', "ASC");
        $this->db->where('homeslide_status', 1);
        $query = $this->db->get('homeslide');
        return $query->result_array();
	}
	
	
    
	public function getsetpage($index,$lang)
	{
		if ($lang==1) {
            $this->db->select("setpage_id,setpage_image,setpage_imagealt,setpage_h1_th as setpage_h1,setpage_h2_th as setpage_h2,setpage_short_th as setpage_short,setpage_detail_th as setpage_detail,setpage_meta_title,setpage_meta_description,setpage_meta_keyword");
            $this->db->from('setpage');
            $this->db->where('setpage_id', $index);
        }else{
            $this->db->select("setpage_id,setpage_image,setpage_imagealt,setpage_h1_en as setpage_h1,setpage_h2_en as setpage_h2,setpage_short_en as setpage_short,setpage_detail_en as setpage_detail,setpage_meta_title,setpage_meta_description,setpage_meta_keyword");
            $this->db->from('setpage');
            $this->db->where('setpage_id', $index);

        }
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function addstayconnect($email)
    {
        $this->db->where('stayconnect_email', $email);
        $query = $this->db->get('stayconnect');
        if ($query->num_rows()>=1) {
            return 400;
        }else {
            $data = array(
            'stayconnect_email' => $email,
            'update_date' => date("Y/m/d H:i:s")
            );
            $query = $this->db->insert('stayconnect',$data);
            return 200;
        }
    }






}
