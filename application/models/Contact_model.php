
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

    public function getdatasearchproduct($data,$main,$join,$alias)
	{
       
        $this->db->join('product_description', 'product.product_id = product_description.product_id', 'left');
        $this->db->join('category', 'product.category_id = category.category_id', 'left');
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $this->db->join('ribbon', 'product.ribbon_id = ribbon.ribbon_id', 'left');
        $this->db->where("DATE(product.update_date) >=",date('Y-m-d',strtotime($data['start'])));
        $this->db->where("DATE(product.update_date) <=",date('Y-m-d',strtotime($data['end'])));
        $this->db->where('product.product_status', $data['product_status']);
        $this->db->where('product.category_main', $data['category_id']);
        $this->db->where('ribbon.ribbon_id', $data['ribbon']);
        $this->db->where('category_description.language_id', 2);
        $this->db->where('product_description.language_id', 2);
        $this->db->like('product_description_name', $data['product_name_en']);
        $this->db->like('product_model', $data['product_model']);
        $query = $this->db->get('product');
        return $query->result_array();
    }

    public function getcontactbyid($index)
    {

        $this->db->where('contact_id', $index);
        $query = $this->db->get('contact');
        return $query->row_array();
        
    }

    public function updatecontact($data,$index)
    {
        $this->db->trans_start();
        $this->db->where('contact_id', $index);
        $this->db->update('contact',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }


    public function getsetpagebyid($index)
    {

        $this->db->where('setpage_id', $index);
        $query = $this->db->get('setpage');
        return $query->row_array();
        
    }
    public function updatesetpage($data,$index)
    {
        $this->db->trans_start();
        $this->db->where('setpage_id', $index);
        $this->db->update('setpage',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }

    
    
	 







}
