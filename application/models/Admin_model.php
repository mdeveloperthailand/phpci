<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function checkdatalogin($username,$password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', md5($password));
		$this->db->where('user.status', 1);
		$this->db->join('user_group', "user.user_group_id = user_group.user_group_id","left");
        $query = $this->db->get('user');
        return $query->row_array();
	}
	public function getadmingroup()
	{
        $query = $this->db->query('select user_group_id,name,update_date from user_group');
        return $query->result_array();
	}
	public function getadmingroupbyid($id)
	{
        $this->db->where('user_group_id', $id);
        $query = $this->db->get('user_group');
        return $query->row_array();
    }
    public function addadmin($data)
	{
        $query = $this->db->insert('user',$data);
        return ($this->db->affected_rows() != 1) ? false : true;
	}
	public function updateadmin($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where('user_id', $index);
		$this->db->update('user', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
	public function addadmingroup($data)
	{
        $query = $this->db->insert('user_group',$data);
        return ($this->db->affected_rows() != 1) ? false : true;
	}
	public function updateadmingroup($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where('user_group_id', $index);
		$this->db->update('user_group', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
    public function getadminall()
    {
        $this->db->join('user_group', 'user.user_group_id = user_group.user_group_id', 'left');
        $query = $this->db->get('user');
        return $query->result_array();
	}

	public function getadminbyid($id)
    {
		$this->db->where('user_id', $id);
        $query = $this->db->get('user');
        return $query->row_array();
	}
	
	public function updatestatusadmin($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('user_id', $index);
		$this->db->update('user', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
	
	public function updatepasswordadmin($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where('user_id', $index);
		$this->db->update('user', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
	
	public function deleteadmin($index)
    {
		$this->db->trans_start();
		$this->db->where('user_id', $index);
		$this->db->delete('user');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}

	public function deleteadmingroup($index)
    {
		$this->db->trans_start();
		$this->db->where('user_group_id', $index);
		$this->db->delete('user_group');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

	}
	
	public function deleteadminbyselect($index)
    {
		$this->db->trans_start();
		$this->db->where_in('user_id', $index);
		$this->db->delete('user');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }
    







}
