
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storelocator_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

   // storelocator
   public function getstorelocatorall()
   {

       $query = $this->db->get('storelocator');
       return $query->result_array();

   }

   public function addstorelocator($datainsert)
   {
       $query = $this->db->insert('storelocator',$datainsert);
       if($this->db->affected_rows() == 1){
           return TRUE;
       }else{
           return FALSE;
       }
   }


   public function deletestorelocator($index)
   {
       $this->db->trans_start();
       $this->db->where('storelocator_id', $index);
       $this->db->delete('storelocator');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function deletestorelocatorbyselect($index)
   {
       $this->db->trans_start();
       $this->db->where_in('storelocator_id', $index);
       $this->db->delete('storelocator');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }


   public function updatestatusstorelocator($index,$dataupdate)
   {
       $this->db->trans_start();
       $this->db->where_in('storelocator_id', $index);
       $this->db->update('storelocator', $dataupdate);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function getstorelocatorallupdatestatus($limit,$start)
   {

       $query = $this->db->get('storelocator');

       if($query->num_rows()>0)
       {
           return $query->result_array();
       }
       else 
       {
           return false;
       }

       
   }

   public function getstorelocatorbyid($index)
   {

       $this->db->where('storelocator_id', $index);
       $query = $this->db->get('storelocator');
       return $query->row_array();
       
   }

   public function updatestorelocator($data,$index)
   {
       $this->db->trans_start();
       $this->db->where('storelocator_id', $index);
       $this->db->update('storelocator',$data);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   public function updatestorelocatororder($data)
   {
      
       $this->db->trans_start();
       foreach ($data as $key => $value) {
           $this->db->set('storelocator_order', $value);
           $this->db->where('storelocator_id', $key);
           $this->db->update('storelocator');
       }
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   // storelocator END

	 

   public function getstorelocatorallwithlang($limit,$start,$lang)
   {

     if ($lang==1) {
        $this->db->select("storelocator_id,storelocator_image,storelocator_imagealt,storelocator_name_th as storelocator_name,storelocator_address_th as storelocator_address,storelocator_order,storelocator_tel,storelocator_time,storelocator_lat,storelocator_long,storelocator_meta_title,storelocator_meta_description,storelocator_meta_keyword");
        $this->db->from('storelocator');
        $this->db->where('storelocator_status', 1);
        $this->db->order_by('storelocator_order', "ASC");
        $this->db->limit($limit, $start);

    }else{
        $this->db->select("storelocator_id,storelocator_image,storelocator_imagealt,storelocator_name_en as storelocator_name,storelocator_address_en as storelocator_address,storelocator_order,storelocator_tel,storelocator_time,storelocator_lat,storelocator_long,storelocator_meta_title,storelocator_meta_description,storelocator_meta_keyword");
        $this->db->from('storelocator');
        $this->db->where('storelocator_status', 1);
        $this->db->order_by('storelocator_order', "ASC");
        $this->db->limit($limit, $start);

    }
    $query = $this->db->get();
    return $query->result_array();
       
   }





}
