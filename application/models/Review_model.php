
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

   // review
   public function getreviewall()
   {

       $query = $this->db->get('review');
       return $query->result_array();

   }

   public function addreview($datainsert)
   {
       $query = $this->db->insert('review',$datainsert);
       if($this->db->affected_rows() == 1){
           return TRUE;
       }else{
           return FALSE;
       }
   }


   public function deletereview($index)
   {
       $this->db->trans_start();
       $this->db->where('review_id', $index);
       $this->db->delete('review');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function deletereviewbyselect($index)
   {
       $this->db->trans_start();
       $this->db->where_in('review_id', $index);
       $this->db->delete('review');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }


   public function updatestatusreview($index,$dataupdate)
   {
       $this->db->trans_start();
       $this->db->where_in('review_id', $index);
       $this->db->update('review', $dataupdate);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function getreviewallupdatestatus($limit,$start)
   {

       $query = $this->db->get('review');

       if($query->num_rows()>0)
       {
           return $query->result_array();
       }
       else 
       {
           return false;
       }

       
   }

   public function getreviewbyid($index)
   {

       $this->db->where('review_id', $index);
       $query = $this->db->get('review');
       return $query->row_array();
       
   }

   public function getreviewdetailbyid($index,$lang)
   {

     if ($lang==1) {
        $this->db->select("review_id,review_image,review_imagealt,review_name_th as review_name,review_short_th as review_short,review_detail_th as review_detail,review_order,review_meta_title,review_meta_description,review_meta_keyword");
        $this->db->from('review');
        $this->db->where('review_status', 1);
        $this->db->where('review_id', $index);

    }else{
        $this->db->select("review_id,review_image,review_imagealt,review_name_en as review_name,review_short_en as review_short,review_detail_en as review_detail,review_order,review_meta_title,review_meta_description,review_meta_keyword");
        $this->db->from('review');
        $this->db->where('review_status', 1);
        $this->db->where('review_id', $index);

    }
    $query = $this->db->get();
    return $query->row_array();
       
   }

   

   public function updatereview($data,$index)
   {
       $this->db->trans_start();
       $this->db->where('review_id', $index);
       $this->db->update('review',$data);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   public function updaterevieworder($data)
   {
      
       $this->db->trans_start();
       foreach ($data as $key => $value) {
           $this->db->set('review_order', $value);
           $this->db->where('review_id', $key);
           $this->db->update('review');
       }
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   // review END

	 







}
