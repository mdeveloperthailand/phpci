
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feature_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	

   // feature
   public function getfeatureall()
   {

       $query = $this->db->get('feature_product');
       return $query->result_array();

   }

   public function addfeature($datainsert)
   {
       $query = $this->db->insert('feature_product',$datainsert);
       if($this->db->affected_rows() == 1){
           return TRUE;
       }else{
           return FALSE;
       }
   }


   public function deletefeature($index)
   {
       $this->db->trans_start();
       $this->db->where('feature_id', $index);
       $this->db->delete('feature_product');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function deletefeaturebyselect($index)
   {
       $this->db->trans_start();
       $this->db->where_in('feature_id', $index);
       $this->db->delete('feature_product');
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }


   public function updatestatusfeature($index,$dataupdate)
   {
       $this->db->trans_start();
       $this->db->where_in('feature_id', $index);
       $this->db->update('feature_product', $dataupdate);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }

   }

   public function getfeatureallupdatestatus($limit,$start)
   {

       $query = $this->db->get('feature_product');

       if($query->num_rows()>0)
       {
           return $query->result_array();
       }
       else 
       {
           return false;
       }

       
   }

   public function getfeaturebyid($index)
   {

       $this->db->where('feature_id', $index);
       $query = $this->db->get('feature_product');
       return $query->row_array();
       
   }

   public function updatefeature($data,$index)
   {
       $this->db->trans_start();
       $this->db->where('feature_id', $index);
       $this->db->update('feature_product',$data);
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   public function updatefeatureorder($data)
   {
      
       $this->db->trans_start();
       foreach ($data as $key => $value) {
           $this->db->set('feature_order', $value);
           $this->db->where('feature_id', $key);
           $this->db->update('feature_product');
       }
       $this->db->trans_complete();

       if ($this->db->trans_status() === FALSE)
       {
           return 400;
       }else{
           return 200;
       }
   }

   // feature END

	 







}
