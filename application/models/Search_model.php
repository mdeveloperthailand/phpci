<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function getdatasearch($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }

    public function getdatasearchnewscategory($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(blog_category.blog_category_update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(blog_category.blog_category_update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }
       $this->db->where('blog_category_description.language_id',2);
       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }
    
    public function getdatasearchnews($limit,$start,$data)
	{
        // $this->db->limit($limit, $start);
        $this->db->like('blog_description_subject', $data['blog_description_subject']);
        $this->db->where('blog_category_description.language_id',2);
        $this->db->where('blog_description.language_id',2);
        $this->db->where('blog_type_description.language_id',2);
        $this->db->where("DATE(blog.update_date) >=",date('Y-m-d',strtotime($data['start'])));
        $this->db->where("DATE(blog.update_date) <=",date('Y-m-d',strtotime($data['end'])));
        $this->db->where("blog_status",$data['blog_status']);


        $this->db->join('blog_type', 'blog_type.blog_type_id = blog.blog_type_id', 'inner');
        $this->db->join('blog_type_description', 'blog_type.blog_type_id = blog_type_description.blog_type_id', 'inner');
        $this->db->join('blog_category', 'blog_category.blog_category_id = blog.blog_category_id', 'inner');
        $this->db->join('blog_category_description', 'blog_category.blog_category_id = blog_category_description.blog_category_id', 'inner');
        $this->db->join('blog_description', 'blog_description.blog_id = blog.blog_id', 'inner');

       $query = $this->db->get('blog');
       return $query->result_array();
    }


    public function getdatasearchwhatinstorecategory($limit,$start,$data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(category.update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(category.update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="category_status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }
       $this->db->where('category_description.language_id',2);
       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }
        // $this->db->limit($limit, $start);

       $query = $this->db->get();
       return $query->result_array();
    }

    public function getdatasearchribbon($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }


    public function getdatasearchfeature($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }

    public function getdatasearchdimension($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }

    public function getdatasearchcolour($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }

    public function getdatasearchaesthetic($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }

    public function getdatasearchproduct($data,$main,$join,$alias)
	{
       
        $this->db->join('product_description', 'product.product_id = product_description.product_id', 'left');
        $this->db->join('category', 'product.category_id = category.category_id', 'left');
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $this->db->join('ribbon', 'product.ribbon_id = ribbon.ribbon_id', 'left');
        $this->db->where("DATE(product.update_date) >=",date('Y-m-d',strtotime($data['start'])));
        $this->db->where("DATE(product.update_date) <=",date('Y-m-d',strtotime($data['end'])));
        $this->db->where('product.product_status', $data['product_status']);
        $this->db->where('product.category_main', $data['category_id']);
        $this->db->where('ribbon.ribbon_id', $data['ribbon']);
        $this->db->where('category_description.language_id', 2);
        $this->db->where('product_description.language_id', 2);
        $this->db->like('product_description_name', $data['product_name_en']);
        $this->db->like('product_model', $data['product_model']);
        $query = $this->db->get('product');
        return $query->result_array();
    }

    public function getdatasearchpersmegtive($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }

    public function getdatasearchreview($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }

    public function getdatasearchstorelocator($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }


    public function getdatasearchfeatureproduct($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }
    

    public function getdatasearchhomeslide($data,$main,$join,$alias)
	{

       $this->db->select("*,$alias");
       $this->db->from($main);
       foreach ($data as $key => $value) {
           if ($key=="start") {
            $this->db->where("DATE(".$main.".update_date) >=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="end") {
            $this->db->where("DATE(".$main.".update_date) <=",date('Y-m-d',strtotime($value)));
           }elseif ($key=="status") {
            $this->db->where($key,$value);
           }elseif($key!="search"){
           $this->db->like($key, $value);
           }
       }

       foreach ($join as $key => $value) {
        $this->db->join($key, " ".$main.".".$value."  = ".$key.".".$value."  ", 'left');
       }

       $query = $this->db->get();
       return $query->result_array();
    }
	 







}
