<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Whatinstore_model extends CI_model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
    public function getwhatinstoremaincategoryall($limit, $start)
	{   
        $this->db->limit($limit, $start);
        $this->db->where('category_description.language_id', 2);
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $query = $this->db->get('category');

        $data = $query->result_array();
        $newarr = array();
        $i = 0;
        foreach ($data as $key => $value) {
           

            if ($value['parent_id']==0) {
               
                $newarr[$i]['category_id'] = $value['category_id'];
                $newarr[$i]['category_description_name'] = $value['category_description_name'];
                $newarr[$i]['category_status'] = $value['category_status'];
                $newarr[$i]['update_date'] = $value['update_date'];
                $ii = 0;

                foreach ($data as $key1 => $value1) {
                
                    if ($value1['parent_id']==$value['category_id']) {
                    $newarr[$i]['sub'][$ii]['category_id'] = $value1['category_id'];
                    $newarr[$i]['sub'][$ii]['category_description_name'] = $value1['category_description_name'];
                    $newarr[$i]['sub'][$ii]['category_status'] = $value1['category_status'];
                    $newarr[$i]['sub'][$ii]['update_date'] = $value1['update_date'];
                    $ii++;
                    }

                }

            }

            $i++;
           
        }


        return $newarr;
        // if($query->num_rows()>0)
        // {
        //     return $query->result_array();
        // }
        // else 
        // {
        //     return false;
        // }
    }

    public function getwhatinstoremaincategoryallupdatestatus($limit, $start)
	{   
        // $this->db->limit($limit, $start);
        $this->db->where('category_description.language_id', 2);
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $query = $this->db->get('category');

        $data = $query->result_array();
        $newarr = array();
        foreach ($data as $key => $value) {
            $i = 0;
            $newarr[$i]['category_id'] = $value['category_id'];
            $newarr[$i]['category_description_name'] = $value['category_description_name'];
            $newarr[$i]['category_status'] = $value['category_status'];
            $newarr[$i]['update_date'] = $value['update_date'];
            $ii = 0;
            foreach ($data as $key1 => $value1) {
               
                if ($value1['parent_id']==$value['category_id']) {
                $newarr[$i]['sub'][$ii]['category_id'] = $value1['category_id'];
                $newarr[$i]['sub'][$ii]['category_description_name'] = $value1['category_description_name'];
                $newarr[$i]['sub'][$ii]['category_status'] = $value1['category_status'];
                $newarr[$i]['sub'][$ii]['update_date'] = $value1['update_date'];
                $ii++;
                }
            }
            $i++;
           
        }


        return $newarr;
        // if($query->num_rows()>0)
        // {
        //     return $query->result_array();
        // }
        // else 
        // {
        //     return false;
        // }
    }

     public function getcategorymainproductall($lang)
    {
        $this->db->where('category.parent_id', 0);
        $this->db->where('category_description.language_id', $lang);
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $query = $this->db->get('category');
        return $query->result_array();
    }

    public function getmaincategorydatabyid($index)
    {
        $this->db->where('category.category_id', $index);
        $this->db->where('category_description.language_id', 2);
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $query = $this->db->get('category');
        return $query->row_array();
    }

    public function getproductdatabycategoryid($limit,$start,$index,$lang)
    {
        $this->db->limit($limit, $start);
        $this->db->where('product.category_main', $index);
        $this->db->where('category_description.language_id', $lang);
        $this->db->where('product_description.language_id', $lang);
        $this->db->join('product_description', 'product.product_id = product_description.product_id', 'left');
        $this->db->join('category', 'product.category_id = category.category_id', 'left');
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $this->db->join('ribbon', 'product.ribbon_id = ribbon.ribbon_id', 'left');
        $query = $this->db->get('product');
        return $query->result_array();
    }

    public function getlayoutwhatinstore($index,$lang)
    {
        if ($lang==1) {
            $this->db->select("layout_id,layout_image,layout_imagealt,layout_h1_th as layout_h1,layout_h2_th as layout_h2,layout_texteditor_th as layout_texteditor,layout_meta_page_title,layout_meta_description,layout_meta_keyword");
            $this->db->from('layout');
            $this->db->where('layout_page', $index);
        }else{
            $this->db->select("layout_id,layout_image,layout_imagealt,layout_h1 as layout_h1,layout_h2 as layout_h2,layout_texteditor as layout_texteditor,layout_meta_page_title,layout_meta_description,layout_meta_keyword");
            $this->db->from('layout');
            $this->db->where('layout_page', $index);

        }

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getcategorydatabyidthai($index)
    {
        $this->db->where('category.category_id', $index);
        $this->db->where('category_description.language_id', 1);
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $query = $this->db->get('category');
        return $query->row_array();
    }

    public function getcategorydatabyideng($index)
    {
        $this->db->where('category.category_id', $index);
        $this->db->where('category_description.language_id', 2);
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $query = $this->db->get('category');
        return $query->row_array();
    }

    public function updatestatuscategory($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('category_id', $index);
		$this->db->update('category', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function addwhatinstorecategorymain($data)
	{
        $query = $this->db->insert('category',$data);
      	$id = $this->db->insert_id();
		return $id;
    }

    public function addwhatinstorecategorydescriptionmain($datathai,$dataeng)
	{
        $query = $this->db->insert('category_description',$datathai);
        $query = $this->db->insert('category_description',$dataeng);
        return ($this->db->affected_rows() != 1) ? false : true;
	}
   
    public function deletewhatinstorecategory($index)
    {
		$this->db->trans_start();
		$this->db->where('category_id', $index);
        $this->db->delete('category');
        $this->db->where('category_id', $index);
		$this->db->delete('category_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function deletewhatinstorecategorybyselect($index)
    {
		$this->db->trans_start();
		$this->db->where_in('category_id', $index);
        $this->db->delete('category');
        $this->db->where_in('category_id', $index);
		$this->db->delete('category_description');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }


    public function updatewhatinstorecategory($data,$index)
	{
        $this->db->trans_start();
            $this->db->where('category_id', $index);
            $this->db->update('category',$data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
    }

    public function updatewhatinstorecategorydescription($datathai,$dataeng,$index)
	{
        $this->db->trans_start();
			$this->db->where('category_id', $index);
			$this->db->where('language_id', 1);
			$this->db->update('category_description',$datathai);
			
			$this->db->where('category_id', $index);
			$this->db->where('language_id', 2);
            $this->db->update('category_description',$dataeng);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
    }
    

    public function getsetwhatinstore()
    {
        $this->db->where('layout_page', 'whatinstore');
        $query = $this->db->get('layout');
        return $query->row_array();
    }

    public function updatesetwhatinstore($data)
    {
        $this->db->trans_start();
        $this->db->where('layout_page', 'whatinstore');
        $this->db->update('layout',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }

    public function getsetwhatinstoreproduct()
    {
        $this->db->where('layout_page', 'whatinstoreproduct');
        $query = $this->db->get('layout');
        return $query->row_array();
    }

    public function updatesetwhatinstoreproduct($data)
    {
        $this->db->trans_start();
        $this->db->where('layout_page', 'whatinstoreproduct');
        $this->db->update('layout',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }


    public function addwhatinstoreribbon($datainsert)
    {
        $query = $this->db->insert('ribbon',$datainsert);
        if($this->db->affected_rows() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function getribbonall()
    {
        $query = $this->db->get('ribbon');
        return $query->result_array();
    }
    public function getribbonbyid($index)
    {
        $this->db->where('ribbon_id', $index);
        $query = $this->db->get('ribbon');
        return $query->row_array();
    }

    public function updateribbon($data,$index)
    {
        $this->db->trans_start();
        $this->db->where('ribbon_id', $index);
        $this->db->update('ribbon',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }

    public function deleteribbon($index)
    {
		$this->db->trans_start();
		$this->db->where('ribbon_id', $index);
        $this->db->delete('ribbon');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function deleteribbonbyselect($index)
    {
		$this->db->trans_start();
        $this->db->where_in('ribbon_id', $index);
		$this->db->delete('ribbon');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function updatestatusribbon($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('ribbon_id', $index);
		$this->db->update('ribbon', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function getribbonallupdatestatus($limit,$start)
    {

        $query = $this->db->get('ribbon');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else 
        {
            return false;
        }

        
    }


    public function getfeatureall()
    {

        $query = $this->db->get('feature');
        return $query->result_array();

    }

    public function addwhatinstorefeature($datainsert)
    {
        $query = $this->db->insert('feature',$datainsert);
        if($this->db->affected_rows() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }


    public function deletefeature($index)
    {
		$this->db->trans_start();
		$this->db->where('feature_id', $index);
        $this->db->delete('feature');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function deletefeaturebyselect($index)
    {
		$this->db->trans_start();
        $this->db->where_in('feature_id', $index);
		$this->db->delete('feature');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }


    public function updatestatusfeature($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('feature_id', $index);
		$this->db->update('feature', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function getfeatureallupdatestatus($limit,$start)
    {

        $query = $this->db->get('feature');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else 
        {
            return false;
        }

        
    }

    public function getfeaturebyid($index)
    {

        $this->db->where('feature_id', $index);
        $query = $this->db->get('feature');
        return $query->row_array();
        
    }

    public function updatefeature($data,$index)
    {
        $this->db->trans_start();
        $this->db->where('feature_id', $index);
        $this->db->update('feature',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }


    // DIMENSION
    public function getdimensionall()
    {

        $query = $this->db->get('dimension');
        return $query->result_array();

    }

    public function addwhatinstoredimension($datainsert)
    {
        $query = $this->db->insert('dimension',$datainsert);
        if($this->db->affected_rows() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }


    public function deletedimension($index)
    {
		$this->db->trans_start();
		$this->db->where('dimension_id', $index);
        $this->db->delete('dimension');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function deletedimensionbyselect($index)
    {
		$this->db->trans_start();
        $this->db->where_in('dimension_id', $index);
		$this->db->delete('dimension');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }


    public function updatestatusdimension($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('dimension_id', $index);
		$this->db->update('dimension', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function getdimensionallupdatestatus($limit,$start)
    {

        $query = $this->db->get('dimension');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else 
        {
            return false;
        }

        
    }

    public function getdimensionbyid($index)
    {

        $this->db->where('dimension_id', $index);
        $query = $this->db->get('dimension');
        return $query->row_array();
        
    }

    public function updatedimension($data,$index)
    {
        $this->db->trans_start();
        $this->db->where('dimension_id', $index);
        $this->db->update('dimension',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }
    // DIMENSION END


    // COLOUR
    public function getcolourall()
    {

        $query = $this->db->get('colour');
        return $query->result_array();

    }

    public function addwhatinstorecolour($datainsert)
    {
        $query = $this->db->insert('colour',$datainsert);
        if($this->db->affected_rows() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }


    public function deletecolour($index)
    {
		$this->db->trans_start();
		$this->db->where('colour_id', $index);
        $this->db->delete('colour');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function deletecolourbyselect($index)
    {
		$this->db->trans_start();
        $this->db->where_in('colour_id', $index);
		$this->db->delete('colour');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }


    public function updatestatuscolour($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('colour_id', $index);
		$this->db->update('colour', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function getcolourallupdatestatus($limit,$start)
    {

        $query = $this->db->get('colour');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else 
        {
            return false;
        }

        
    }

    public function getcolourbyid($index)
    {

        $this->db->where('colour_id', $index);
        $query = $this->db->get('colour');
        return $query->row_array();
        
    }

    public function updatecolour($data,$index)
    {
        $this->db->trans_start();
        $this->db->where('colour_id', $index);
        $this->db->update('colour',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }
    // COLOUR END


    // aesthetic
    public function getaestheticall()
    {

        $query = $this->db->get('aesthetic');
        return $query->result_array();

    }

    public function addwhatinstoreaesthetic($datainsert)
    {
        $query = $this->db->insert('aesthetic',$datainsert);
        if($this->db->affected_rows() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }


    public function deleteaesthetic($index)
    {
		$this->db->trans_start();
		$this->db->where('aesthetic_id', $index);
        $this->db->delete('aesthetic');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function deleteaestheticbyselect($index)
    {
		$this->db->trans_start();
        $this->db->where_in('aesthetic_id', $index);
		$this->db->delete('aesthetic');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }


    public function updatestatusaesthetic($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('aesthetic_id', $index);
		$this->db->update('aesthetic', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function getaestheticallupdatestatus($limit,$start)
    {

        $query = $this->db->get('aesthetic');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else 
        {
            return false;
        }

        
    }

    public function getaestheticbyid($index)
    {

        $this->db->where('aesthetic_id', $index);
        $query = $this->db->get('aesthetic');
        return $query->row_array();
        
    }

    public function updateaesthetic($data,$index)
    {
        $this->db->trans_start();
        $this->db->where('aesthetic_id', $index);
        $this->db->update('aesthetic',$data);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            return 400;
        }else{
            return 200;
        }
    }
    // aesthetic END

    // product MAIN

    public function addproduct($data)
    {
        
           $this->db->insert('product',$data);
           $id = $this->db->insert_id();
           return $id;

    }

    public function addproductimage($product_id,$imageproduct)
    {
        foreach ($imageproduct as $key => $value) {
           $this->db->set('product_id', $product_id);
           $this->db->set('image', $value);
           $this->db->insert('product_image');
        }
        if($this->db->affected_rows() >= 1){
            return TRUE;
        }else{
            return FALSE;
        }

    }

    public function addproductdescription($detail)
    {
        $this->db->insert('product_description',$detail);
        if($this->db->affected_rows() >= 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function updateproduct($data,$index)
    {
        $this->db->trans_start();
		$this->db->where('product_id', $index);
		$this->db->update('product', $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function updateproductimage($product_id,$imageproduct)
    {
        if ($imageproduct!=NULL) {
            $this->db->where('product_id', $product_id);
            $this->db->delete('product_image');
            foreach ($imageproduct as $key => $value) {
                $this->db->set('product_id', $product_id);
                $this->db->set('image', $value);
                $this->db->insert('product_image');
             }
             if($this->db->affected_rows() >= 1){
                 return TRUE;
             }else{
                 return FALSE;
             }
        }else{
            return TRUE;
        }
        

    }

    public function updateproductdescription($detail,$index,$lang)
    {
        $this->db->trans_start();
        $this->db->where('language_id', $lang);
		$this->db->where('product_id', $index);
		$this->db->update('product_description', $detail);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}
    }

    public function getcolourselectall()
    {

        $query = $this->db->get('colour');
        return $query->result_array();

    }
    public function getfeatureselectall()
    {

        $query = $this->db->get('feature');
        return $query->result_array();

    }
    public function getaestheticselectall()
    {

        $query = $this->db->get('aesthetic');
        return $query->result_array();

    }
    public function getribbonselectall()
    {

        $query = $this->db->get('ribbon');
        return $query->result_array();

    }
    public function getcategorymainselectall()
    {
        $this->db->where('category.parent_id',0);
        $this->db->where('category_description.language_id', 2);
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $query = $this->db->get('category');
        return $query->result_array();

    }

    public function getcategorysubselectall($index)
    {
        $this->db->where('category.parent_id',$index);
        $this->db->where('category_description.language_id', 2);
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $query = $this->db->get('category');
        return $query->result_array();

    }
    public function getdimensionselectall()
    {

        $query = $this->db->get('dimension');
        return $query->result_array();

    }

    public function getproductall()
    {
        $this->db->where('category_description.language_id', 2);
        $this->db->where('product_description.language_id', 2);
        $this->db->join('product_description', 'product.product_id = product_description.product_id', 'left');
        $this->db->join('category', 'product.category_id = category.category_id', 'left');
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $this->db->join('ribbon', 'product.ribbon_id = ribbon.ribbon_id', 'left');
        $query = $this->db->get('product');
        return $query->result_array();

    }

    public function deleteproduct($index)
    {
		$this->db->trans_start();
		$this->db->where('product_id', $index);
        $this->db->delete('product');
        $this->db->where('product_id', $index);
        $this->db->delete('product_description');
        $this->db->where('product_id', $index);
        $this->db->delete('product_image');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function deleteproductbyselect($index)
    {
		$this->db->trans_start();
        $this->db->where_in('product_id', $index);
        $this->db->delete('product');
        $this->db->where_in('product_id', $index);
        $this->db->delete('product_description');
        $this->db->where_in('product_id', $index);
        $this->db->delete('product_image');
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function updatestatusproduct($index,$dataupdate)
    {
		$this->db->trans_start();
		$this->db->where_in('product_id', $index);
		$this->db->update('product', $dataupdate);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 400;
		}else{
			return 200;
		}

    }

    public function getproductallupdatestatus($limit,$start)
    {

        $query = $this->db->get('product');

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else 
        {
            return false;
        }

        
    }

    public function getproductbyid($product_id,$lang)
    {
        $this->db->where('product.product_id', $product_id);
        $this->db->where('category_description.language_id', $lang);
        $this->db->where('product_description.language_id', $lang);
        $this->db->join('product_description', 'product.product_id = product_description.product_id', 'left');
        $this->db->join('category', 'product.category_id = category.category_id', 'left');
        $this->db->join('category_description', 'category.category_id = category_description.category_id', 'left');
        $this->db->join('ribbon', 'product.ribbon_id = ribbon.ribbon_id', 'left');
        $this->db->join('dimension', 'product.dimension_id = dimension.dimension_id', 'left');
        $this->db->join('aesthetic', 'product.aesthetic_id = aesthetic.aesthetic_id', 'left');
        $this->db->join('feature', 'product.feature_id = feature.feature_id', 'left');
        $this->db->join('colour', 'product.colour_id = colour.colour_id', 'left');

        $query = $this->db->get('product');
        return $query->row_array();

    }

    public function getproductimagebyid($product_id)
    {
        $this->db->where('product_id', $product_id);
        $query = $this->db->get('product_image');
        return $query->result_array();

    }


    // product MAIN





}
